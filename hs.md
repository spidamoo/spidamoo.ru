
# backend

## install
- Dancer2
- DBD::SQLite
- Math::Counting
- Crypt::PBKDF2
- Math::Random::Secure
- XML::Simple

this can be done e.g. like this `sudo cpanm Dancer2 DBD::SQLite Math::Counting Crypt::PBKDF2 Math::Random::Secure XML::Simple`

### prepare SQLite DB
`perl bin/create_tables.pl`

## run
`plackup bin/app.psgi`

# frontend

## install
- NodeJs

## first run
```
cd hsf
npm i
npm run gen
npm run gen:images
npm run server
npm start
```
## later runs
```
npm run server
npm start
```

## npm tasks description
|name      |description                         |
|----------|------------------------------------|
|start     |builds js with --watch              |
|build     |builds js once with compression     |
|server    |runs `plackup bin/app.psgi`         |
|gen       |generates game.const.ts file by perl|
|gen:images|packs images to text file           |

