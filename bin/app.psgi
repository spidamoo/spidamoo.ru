#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "/var/www/spidamoo/data/perl5/lib/perl5/"; # ... fixme

use spidamoo;
spidamoo->to_app;
