use strict;
use warnings;

use XML::Simple qw(:strict);
use Data::Dumper;

my $home_dir = '../rns_client/manual-bin/';

my ($map_name) = @ARGV;

die "give me existing map name" unless $map_name && -f "res/$map_name.tmx";

my $in_xml = XMLin("res/$map_name.tmx", KeyAttr => {}, ForceArray => ['tileset', 'tile', 'layer', 'property', 'object']);
my $out_xml = {
    tiles      => {
        width => $in_xml->{width},
        height => $in_xml->{height},
        tile => [],
    },
    animations => {
        animation => [],
        size      => 1,
    },
    monster_spawns => {
        spawn => []
    },
    effect_spawns => {
        spawn => []
    },
    zones => {
        zone => []
    },
};
my $tiles;
my $tile_types_firstgid;
# my $anim_id = 0;
my @tileset_shifts;
for my $tileset(@{ $in_xml->{tileset} }) {
    if ($tileset->{name} eq 'tile_types') {
        $tile_types_firstgid = $tileset->{firstgid};
        next;
    }
    my $texture = $tileset->{image}{source};
    $texture =~ s/^$home_dir(.+)/$1/;
    my $margin  = $tileset->{margin} // 0;
    my $spacing = $tileset->{spacing} // 0;
    for my $tile(@{ $tileset->{tile} }) {
        my $col = $tile->{id} % $tileset->{columns};
        my $row = int($tile->{id} / $tileset->{columns});
        my $x   = $margin + $col * ($tileset->{tilewidth}  + $spacing);
        my $y   = $margin + $row * ($tileset->{tileheight} + $spacing);
        my $id  = $tileset->{firstgid} + $tile->{id};
        push @{ $out_xml->{animations}{animation} }, {
            id => $id,
            texture => $texture,
            x => $x,
            y => $y,
            w => $tileset->{tilewidth},
            h => $tileset->{tileheight},
            anchor_x => "0",
            anchor_y => "0",
            nframes => "1",
            fps => "0"
        };
        $out_xml->{animations}{size} = $id + 1 if $id >= $out_xml->{animations}{size};
        # $anim_id++;
    }
}

for my $layer(@{ $in_xml->{layer} }, @{ $in_xml->{objectgroup} }) {
    my $type;
    for my $property(@{ $layer->{properties}{property} }) {
        if ($property->{name} eq 'type') {
            $type = $property->{value};
        }
    }
    if ($type eq 'tile_visual') {
        my $y = 0;
        for my $line( split("\n", $layer->{data}{content}) ) {
            next unless $line;
            my $x = 0;
            for my $row( split(",", $line) ) {
                $tiles->[$y][$x]{animation} = $row;
                $x++;
            }
            $y++;
        }
    }
    if ($type eq 'tile_type') {
        my $y = 0;
        for my $line( split("\n", $layer->{data}{content}) ) {
            next unless $line;
            my $x = 0;
            for my $row( split(",", $line) ) {
                $tiles->[$y][$x]{type} = $row ? $row - $tile_types_firstgid + 1 : 0;
                $x++;
            }
            $y++;
        }
    }
    elsif ($type eq 'monster_spawns' || $type eq 'effect_spawns' || $type eq 'zones') {
        my $node_name = $type;
        for my $object(@{ $layer->{object} }) {
            my %properties;
            for my $property(@{ $object->{properties}{property} }) {
                $properties{ $property->{name} } = $property->{value};
            }
            push @{ $out_xml->{$node_name}{spawn} }, {
                type => $object->{type},
                x => $object->{x},
                y => $object->{y},
                width => $object->{width},
                height => $object->{height},
                %properties,
            }
        }
    }
    elsif ($type eq 'images') {
        for my $object(@{ $layer->{object} }) {
            my %properties;
            for my $property(@{ $object->{properties}{property} }) {
                $properties{ $property->{name} } = $property->{value};
            }
            my $x = $object->{x} + $object->{width}  / 2;
            my $y = $object->{y} + $object->{height} / 2;
            my $tile_x = int($x / $in_xml->{tilewidth});
            my $tile_y = int($y / $in_xml->{tileheight});
            my $shift_x = $x - $tile_x * $in_xml->{tilewidth};
            my $shift_y = $y - $tile_y * $in_xml->{tileheight};
            push @{ $tiles->[$tile_y][$tile_x]{object} }, {
                shift_x => $shift_x,
                shift_y => $shift_y,
                animation => $object->{gid},
            }
        }
    }
}

for my $y(0 .. $#{$tiles}) {
    for my $x(0 .. $#{$tiles->[$y]}) {
        my $tile = $tiles->[$y][$x];
        push @{ $out_xml->{tiles}{tile} }, {
            x => $x, y => $y,
            %$tile,
            height => 0,
        };
    }
}


if (-f "res/$map_name.xml") {
    my $now = time();
    system "cp res/$map_name.xml res/$map_name.bak_$now.xml";
}

open my $out_file, '>', "res/$map_name.xml";
print $out_file XMLout($out_xml, KeyAttr => {}, RootName => 'map');
