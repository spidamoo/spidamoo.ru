#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "/var/www/spidamoo/data/perl5/lib/perl5/"; # ... fixme

use HexStrat;


my $hs = new HexStrat(
	data_path => 'data/hex_strat/',
);
$hs->_create_tables(general => 1);
# $hs->_create_tables();
# $hs->_create_map();
