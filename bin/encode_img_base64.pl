use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use MIME::Base64 qw(encode_base64 encode_base64url);

print "{\n";
my $first = 1;
walk("$FindBin::Bin/../data/hex_strat", 'img');
print "\n}\n";

sub walk {
    my ($root, $dir) = @_;
    $dir //= '';
    opendir(my $dh, "$root/$dir");

    while ( my $file = readdir($dh) ) {
        next if $file eq '.';
        next if $file eq '..';

        if (-d "$root/$dir/$file") {
            walk($root, "$dir/$file");
        }
        else {
            next unless substr($file, -4) eq '.png';
            if ($first) {
                $first = 0;
            }
            else {
                print ",\n";
            }
            print "\"$dir/$file\":";
            open(my $fh, "$root/$dir/$file");
            local($/) = undef;
            print '"', encode_base64(<$fh>, ''), '"';
        }
    }
}

