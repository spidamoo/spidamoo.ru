use strict;
no strict 'refs';
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use HexStrat;
use JSON::MaybeXS;

my %settings = map {$_ => 1} @ARGV;

my $json = JSON::MaybeXS->new(pretty => $settings{pretty} // 0);

my %consts;
for my $const(sort keys %constant::declared) {
    if ( substr($const, 0, 10) eq 'HexStrat::' ) {
        $consts{substr($const, 10)} = &$const();
    }
}
print "export const HS = " . $json->encode(\%consts);

