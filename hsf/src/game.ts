import './game.less';
// import 'createjs/builds/1.0.0/createjs.min';
import 'easeljs/lib/easeljs.min';

import Container = createjs.Container;
import Shape = createjs.Shape;
import {forEachObject, getImage, isDefined, preload_images} from './utils';
import Bitmap = createjs.Bitmap;
import Stage = createjs.Stage;
import Text = createjs.Text;
import {ApiService} from './services/api.service';
import {Map} from './modules/map';
import {
    IArmy, IBuilding, ICampaign, ICreateJsEvent, IGameInfo, IResource, ITask, IUnit,
    IUser
} from './types/game.types';
import {Minimap} from './modules/minimap';
import {HS} from './game.const';
import {Player} from './entities/player';
import {UI} from './ui';

// Rxjs imports
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/dom/ajax';
import {EndTurnButton} from './modules/endTurnButton';
import {LastTurnLog} from './modules/lastTurnLog';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

export const HARVEST_COUNT = 15;

export class Game {
    screen_width: number;
    screen_height: number;

    global_task: number;
    global_tasks_queue: ITask[];
    unit_queues: ITask[][];
    now: number;
    roads_graph = new Graph();
    players_settlements: number[][];
    stored_resources_$ = new BehaviorSubject<{[name: string]: {count: number, mod: number}}>({});
    claimed_resources_$ = new BehaviorSubject<{[name: string]: {count: number, mod: number}}>({});
    unclaimed_resources_map: Object;
    stored_resources_map: Object;
    being_harversted_resources: {
        [name: string]: IResource;
        wood: IResource,
        gold: IResource,
        food: IResource,
    };
    armies_on_map: {
        armies: {[name: string]: IArmy};
        x: number;
        y: number;
    }[];
    armies_positions: {
        [name: string]: number[];
    };
    screen_width_hex: number;
    screen_height_hex: number;
    selected_hex: number[];
    hovered_hex: number[];
    selected_army: IArmy;
    selected_unit: IUnit;
    global_task_current_container: Container;
    capital: IBuilding;
    players: {[name: string]: Player};
    user: IUser;
    campaign: ICampaign;
    building_tasks: number[][] = [];
    turn: string;
    the_winner: string;
    colors: string[];

    hint_container: Container;
    hint_bg: Shape;
    hint_text: Text;

    global_task_data: any;

    map = new Map();
    stage: Stage;
    minimap_stage: Stage;
    minimap = new Minimap();
    end_turn_button = new EndTurnButton();
    last_turn_log = new LastTurnLog();
    player_checkboxes: {[name: string]: Text} = {};
    canvas: HTMLCanvasElement;
    minimap_canvas: HTMLCanvasElement;
    turn_text: Text;
    info_timeout: number;
    grid_icon_bitmap;
    spreadsheet_icon_bitmap;


    ui;

    direction_shifts = [
        [ this.map.hex_half_w,        0],
        [ this.map.hex_half_w * 0.5,  this.map.hex_half_w * Math.sqrt(3) * 0.5],
        [-this.map.hex_half_w * 0.5,  this.map.hex_half_w * Math.sqrt(3) * 0.5],
        [-this.map.hex_half_w,        0],
        [-this.map.hex_half_w * 0.5, -this.map.hex_half_w * Math.sqrt(3) * 0.5],
        [ this.map.hex_half_w * 0.5, -this.map.hex_half_w * Math.sqrt(3) * 0.5],
    ];

    overlay = new Shape();

    get_game_info(callback: (data: IGameInfo) => void) {
        ApiService.get<IGameInfo>('get_game_info', {})
            .subscribe(callback);
    }

    init_game_info(callback: () => void) {
        this.get_game_info(data => {
           this.user = data.user;
           this.players = {};

           // this.campaign = data.campaign;
           data.players.forEach(player => {
               this.players[player.user_id] = new Player(player);
           });

           if (callback) {
               callback();
           }
        });
    }

    init(canvas_id: string, minimap_canvas_id: string, campaign_id: string) {
        this.ui = new UI();
        this.canvas = document.getElementById(canvas_id) as HTMLCanvasElement;
        this.minimap_canvas = document.getElementById(minimap_canvas_id) as HTMLCanvasElement;

        Observable.fromEvent(window, 'resize')
            .debounceTime(1000)
            .subscribe(() => this.resize());

        this.stage = new createjs.Stage(canvas_id);
        this.minimap_stage = new createjs.Stage(minimap_canvas_id);

        this.campaign = {
            short_id: campaign_id
        };
        this.init_game_info(() => {
            this.init_game_data();
        });

        this.ui.init();
    }

    resize() {
        this.screen_width  = window.innerWidth;
        this.screen_height = window.innerHeight;
        this.canvas.setAttribute('width', this.screen_width.toString());
        this.canvas.setAttribute( 'height', this.screen_height.toString());
        this.minimap_canvas.style.setProperty('width', '200px');
        this.minimap_canvas.style.setProperty( 'height', '200px');
        this.stageUpdate();
        this.minimap.updateStream.next();
    }

    init_game_data() {
        this.colors = Object.keys(this.players).map(key => this.players[key].color).concat(['gray']);

        this.resize();
        this.screen_width_hex  = 1 + Math.ceil( this.screen_width / this.map.hex_w );
        this.screen_height_hex =  Math.ceil( this.screen_height / (this.map.hex_half_h * 1.5) );
        this.screen_height_hex += this.screen_height_hex % 2; // округляем вверх до четного
        console.log('init_game_data', this.screen_width_hex, this.screen_height_hex);
        this.stage.enableMouseOver(30);
        createjs.Touch.enable(this.stage);

        this.stage.addChild(this.map.container);

        this.minimap.init(this.minimap_stage);
        this.map.init();
        this.end_turn_button.init();
        this.last_turn_log.init();

        this.turn_text = new createjs.Text('', '14px Arial', 'black');
        this.stage.addChild(this.turn_text);
        this.turn_text.x = this.screen_width  - 70;
        this.turn_text.y = this.screen_height - 70;

        let i = 0;
        let checkbox_text;

        for (const user_id in this.players) {
            checkbox_text = new createjs.Text("☐", 'bold 18px Arial', this.players[user_id].color);
            this.stage.addChild(checkbox_text);
            checkbox_text.x = this.screen_width - 72 + i * 12;
            checkbox_text.y = this.screen_height - 22;
            this.player_checkboxes[+user_id] = checkbox_text;

            i++;
        }

        this.grid_icon_bitmap = new createjs.Bitmap(getImage('img/grid.png'));
        this.stage.addChild(this.grid_icon_bitmap);
        this.grid_icon_bitmap.x = this.screen_width  - 70;
        this.grid_icon_bitmap.y = this.screen_height - 230;
        this.grid_icon_bitmap.on('click', () => {
            this.toggle_grid();
        });
        this.grid_icon_bitmap.on('mouseover', () => {
            this.draw_hint('Вкл./выкл. сетку', this.screen_width - 115, this.screen_height - 245);
        });
        this.grid_icon_bitmap.on('mouseout', () => {
            this.draw_hint();
        });

        this.spreadsheet_icon_bitmap = new createjs.Bitmap(getImage('img/spreadsheet.png'));
        this.stage.addChild(this.spreadsheet_icon_bitmap);
        this.spreadsheet_icon_bitmap.x = this.screen_width  - 70;
        this.spreadsheet_icon_bitmap.y = this.screen_height - 310;
        this.spreadsheet_icon_bitmap.on('click', () => {
            this.show_stats();
        });
        this.spreadsheet_icon_bitmap.on('mouseover', () => {
            this.draw_hint('Статистика', this.screen_width - 80, this.screen_height - 325);
        });
        this.spreadsheet_icon_bitmap.on('mouseout', () => {
            this.draw_hint();
        });

        const surrender_icon_bitmap = new createjs.Bitmap(getImage('img/surrender.png'));
        this.stage.addChild(surrender_icon_bitmap);
        surrender_icon_bitmap.x = this.screen_width  - 70;
        surrender_icon_bitmap.y = this.screen_height - 390;
        surrender_icon_bitmap.on('click', () => {
            this.surrender();
        });
        surrender_icon_bitmap.on('mouseover', () => {
            this.draw_hint('Сдаться', this.screen_width - 70, this.screen_height - 405);
        });
        surrender_icon_bitmap.on('mouseout', () => {
            this.draw_hint();
        });

        building_ui_shape = new createjs.Shape();
        this.stage.addChild(building_ui_shape);
        building_ui_shape.y = 0;
        building_ui_shape.x = 0;
        building_ui_shape.graphics.f('lightgrey').s('black').r(5, 5, 400, 42);

        let j = 0;

        forEachObject(HS.AVAILABLE_TASKS['building']['0'], (active, task_type) => {
            this.stage.addChild(
                this.add_task_icon(0, parseInt(task_type), 10 + j * 34, 10)
            );
            j++;
        });

        this.global_task_current_container = new createjs.Container();
        this.map.container.addChild(this.global_task_current_container);
        global_queue_container = new createjs.Container();
        this.map.container.addChild(global_queue_container);
        global_queue_road_shape = new createjs.Shape();
        this.map.container.addChild(global_queue_road_shape);

        this.hint_container = new createjs.Container();
        this.stage.addChild(this.hint_container);
        this.hint_bg = new createjs.Shape();
        this.hint_container.addChild(this.hint_bg);
        this.hint_text = new createjs.Text('', '14px Arial', 'black');
        this.hint_container.addChild(this.hint_text);

        this.stage.addChild(this.overlay);
        this.overlay.graphics.f('rgba(255, 255, 255, 0.5)').rect(0, 0, this.screen_width, this.screen_height);
        this.overlay.visible = false;

        document.onkeydown = event => this.key_pressed(event);

        // createjs.Ticker.setFPS(50);
        // createjs.Ticker.addEventListener("tick", function(event) {

        this.refresh_game_info();
    }

    reset_global_task() {
        this.global_task = undefined;
        this.global_task_data = false;
        this.global_task_current_container.removeAllChildren();
    }

    determine_direction(x1: number, y1: number, x2: number, y2: number): number {
        const xd = x2 - x1;
        const yd = y2 - y1;
        const dirs = directions[y1 % 2];

        for (let i = 0; i < 6; i++) {
            if (xd === dirs[i][0] && yd === dirs[i][1]) {
                return i;
            }
        }
    }

    draw_hint(text?: string, x?: number, y?: number) {
        if (!text) {
            this.hint_container.visible = false;
            this.stageUpdate();

            return;
        }

        this.hint_container.visible = true;
        this.hint_text.text = text;
        this.hint_container.x = x;
        this.hint_container.y = y;

        const bounds = this.hint_text.getBounds();

        this.hint_bg.graphics.c().f('white').r(bounds.x, bounds.y, bounds.width, bounds.height);
        this.stageUpdate();
    }
    
    stageUpdate() {
        this.stage.update();
    }

    add_unit_task_queue_icon(unit_id: number, task: ITask, x: number, y: number) {
        return this.add_icon(task_images[task.task_type], x, y, task_names[task.task_type] + ': ' + task.task_data, () => {
            this.remove_unit_task(unit_id, task.id);
        });
    }

    add_icon(img: string, x: number, y: number, hint: string, action: Callback) {
        const bitmap = new createjs.Bitmap(getImage(img));

        bitmap.x = x;
        bitmap.y = y;
        bitmap.on('mouseover', () => {
            const point = bitmap.localToGlobal(0, 0);

            this.draw_hint(hint, point.x, point.y + 33);
        });
        bitmap.on('mouseout', () => {
            this.draw_hint();
        });

        if (action !== undefined) {
            bitmap.on('click', action);
        }

        return bitmap;
    }

    add_unit_task(unit_id: number, task_type: number, task_data?: number, callback?: (data: ITask[]) => void) {
        ApiService.get<ITask[]>('add_unit_task', {unit_id, task_type, task_data})
            .subscribe(data => {
                this.unit_queues[unit_id] = data;
                this.map.update_unit_ui(this.selected_unit);

                if (callback) {
                    callback(data);
                }
            });
    }

    remove_unit_task(unit_id: number, id: number) {
        ApiService.get<ITask[]>('remove_unit_task', {id})
            .subscribe(data => {
                this.unit_queues[unit_id] = data;
                this.map.update_unit_ui(this.selected_unit);
            });
    }

    remove_army_tasks(army_id: number) {
        ApiService.get<ITask[]>('remove_army_tasks', {army_id})
            .subscribe(data => {
                this.map.update_army_ui(army_id, data);
            });
    }

    add_task_icon(building_id: number, task_type: number, x: number, y: number) {
        return this.add_icon(task_images[task_type], x, y, task_names[task_type], () => {
            this.reset_global_task();

            switch (task_types[task_type]) {
                case TASK_TYPE_NORMAL:
                    this.add_building_task(building_id, task_type);
                    break;
                // case TASK_TYPE_TARGETED:
                //     show_target_icons(true, function(target) {return target.type == 'building' || target.type == 'unit'});
                //     target_callback = function(target) {
                //         var data = target.type == 'building' ? 'b:' + target.building.id : 'u:' + target.unit.id;
                //         add_building_task(building_id, task_type, data);
                //         show_target_icons(false);
                //         target_callback = undefined;
                //     }
                //     break;
                case TASK_TYPE_GLOBAL_TARGETED:

                    const paths_to_settlements = this.roads_graph.shortestPath(
                        this.selected_hex[0] + ';' + this.selected_hex[1],
                        this.players_settlements.map(s => {
                            return s[0] + ';' + s[1]
                        })
                    );

                    if (Object.keys(paths_to_settlements).length > 0) {
                        for (let settlement in paths_to_settlements) {
                            const [s_x, s_y] = settlement.split(';');
                            const [s_hx, s_hy] = this.map.hex_to_position(+s_x, +s_y);
                            this.global_task_current_container.addChild(
                                this.map.add_target_settlement_button([s_x, s_y], s_hx, s_hy)
                            );
                        }
                        this.map.target_callback = target_settlement_pos => {
                            this.add_building_task(building_id, task_type, target_settlement_pos[0] + ';' + target_settlement_pos[1]);
                            this.map.target_callback = undefined;
                        }
                    }

                    this.stageUpdate();
                    break;
                case TASK_TYPE_TRANSPORT_RESOURCES:
                    this.reset_global_task();
                    this.global_task_data = {};
                    this.players_settlements.forEach(settlement => {
                        const s_x = settlement[0];
                        const s_y = settlement[1];

                        const [s_hx, s_hy] = this.map.hex_to_position(s_x, s_y);

                        this.global_task_current_container.addChild(
                            this.map.add_army_button('img/arrow_down_big.png', s_hx - 18, s_hy, 'Отсюда', event => {
                                const x = event.currentTarget.x + 16;
                                const y = event.currentTarget.y + 16;

                                event.stopImmediatePropagation();

                                this.global_task_data.from = settlement;

                                this.global_task_current_container.removeAllChildren();

                                let offset = -32;

                                resource_map.forEach(res => {
                                    this.global_task_data[res] = 0;
                                    const res_text = new createjs.Text(this.global_task_data[res], '14px Arial', 'black');
                                    res_text.x = x + offset;
                                    res_text.y = y + 2;
                                    res_text.textAlign = 'center';
                                    res_text.textBaseline = 'middle';

                                    const res_callback = (event: ICreateJsEvent) => {
                                        event.stopImmediatePropagation();
                                        let increase = 1;
                                        if (event.nativeEvent.shiftKey) {
                                            increase = 10;
                                        }
                                        if (event.nativeEvent.altKey) {
                                            increase *= -1;
                                        }

                                        this.global_task_data[res] += increase;
                                        res_text.text = this.global_task_data[res];
                                        this.stageUpdate();
                                    };

                                    res_text.on('pressup', res_callback);
                                    this.global_task_current_container.addChild(
                                        this.map.add_army_button(
                                            'img/' + res + '.png', x + offset, y, '',
                                            res_callback
                                        )
                                    );
                                    this.global_task_current_container.addChild(res_text);

                                    offset += 32;
                                });

                                this.global_task_current_container.addChild(
                                    this.map.add_army_button(
                                        'img/ok2.png', x + 70, y, 'Отправить',
                                        event => {
                                            event.stopImmediatePropagation();

                                            this.global_task_current_container.removeAllChildren();

                                            this.players_settlements.forEach(to_settlement => {
                                                if (to_settlement === settlement) {
                                                    return;
                                                }

                                                const s_x = to_settlement[0];
                                                const s_y = to_settlement[1];

                                                const [s_hx, s_hy] = this.map.hex_to_position(s_x, s_y);

                                                this.global_task_current_container.addChild(
                                                    this.map.add_army_button('img/arrow_down_big.png', s_hx - 18, s_hy, 'Сюда', event => {
                                                        event.stopImmediatePropagation();

                                                        this.global_task_data.to = to_settlement;

                                                        this.add_building_task(0, task_type, this.global_task_data);
                                                        this.reset_global_task();
                                                        this.stageUpdate();
                                                    })
                                                );
                                            });

                                            this.stageUpdate();
                                        }
                                    )
                                );

                                this.stageUpdate();
                            })
                        );
                    });

                    this.map.tile_callback = () => {
                        this.reset_global_task();

                        return false;
                    };

                    this.stageUpdate();
                    break;
                case TASK_TYPE_HIRE:
                    this.map.target_callback = target => {
                        this.add_building_task(building_id, task_type, target.id);
                        this.map.show_target_icons(false);
                        this.map.target_callback = undefined;
                    };
                    this.map.show_target_icons(true, t => t.type === 'army');
                    break;
            }
        });
    }

    add_task_queue_icon(building_id: number, task: ITask, x: number, y: number) {
        let hint = task_names[task.task_type];

        if (task.task_data) {
            hint += ': ' + task.task_data + ' [' + task.ap_cost + ']';
        }

        return this.add_icon(task_images[task.task_type], x, y, hint, () => {
            this.remove_building_task(building_id, task.id);
        });
    }

    // TODO: объединить с building
    add_unit_task_icon(unit: IUnit, task_type: number, x: number, y: number) {
        return this.add_icon(task_images[task_type], x, y, task_names[task_type], () => {
            switch (task_types[task_type]) {
                case TASK_TYPE_NORMAL:
                    this.add_unit_task(unit.id, task_type);
                    break;
                // case TASK_TYPE_TARGETED:
                //     show_target_icons(true, function(target) {return target.type == 'building' || target.type == 'unit'});
                //     target_callback = function(target) {
                //         var data = target.type == 'building' ? 'b:' + target.building.id : 'u:' + target.unit.id;
                //         add_unit_task(unit.id, task_type, data);
                //         show_target_icons(false);
                //         target_callback = undefined;
                //     }
                //     break;
                // case TASK_TYPE_ROAD:
                //     update_unit_ui(false);
                //     select_army(unit.army_id, unit.id);
                //     break;
            }
        });
    }

    // TODO: move to map.ts
    calculate_resources_stats() {
        const claimed_resources = resources_map_helper({count: 0, mod: 0});
        const stored_resources = resources_map_helper({count: 0, mod: 0});
        this.being_harversted_resources = { wood: {total: 0}, gold: {total: 0}, food: {total: 0} };

        this.unclaimed_resources_map    = resources_map_helper({});
        this.stored_resources_map       = {};

        this.map.fields.forEach((row, y) => {
            row.forEach((field, x) => {
                let node = x + ';' + y;
                let is_players_settlement = false;
                let is_other_settlement = false;

                const resources_claimed: {[name: string]: number} = resources_map_helper(0);

                if (field.buildings) {
                    const resources_available: {[name: string]: number} = {...field.raw};

                    field.raw_mod = {};

                    forEachObject(field.buildings, building => {
                        if (building.user_id === this.user.id) {
                            is_players_settlement = true;
                        }
                        else {
                            is_other_settlement = true;
                        }
                        const target = building.building_data ? building.building_data : x + ';' + y;
                        resource_map.forEach(res => {
                            if (building.building_type === resource_harvesters[res]) {
                                resources_claimed[res] = is_players_settlement ? 1 : 2;

                                if (is_players_settlement) {
                                    // count owned resources


                                    if (
                                        !building.tasks || building.tasks.length === 0
                                        || building.tasks[0].task_type !== HS.TASK_TYPE_DIRECT_RESOURCE
                                    ) {
                                        const being_harvested = Math.min(resources_available[res], HARVEST_COUNT);

                                        field.raw_mod[res] = being_harvested;
                                        this.being_harversted_resources[res].total += being_harvested;

                                        if (!isDefined(this.being_harversted_resources[res][target])) {
                                            this.being_harversted_resources[res][target] = 0;
                                        }

                                        this.being_harversted_resources[res][target] += being_harvested;
                                        const [target_x, target_y] = target.split(';');

                                        if (!this.map.fields[target_y][target_x].stored_mod) {
                                            this.map.fields[target_y][target_x].stored_mod = {};
                                        }

                                        this.map.fields[target_y][target_x].stored_mod[res] =
                                            (this.map.fields[target_y][target_x].stored_mod[res] || 0) + being_harvested;

                                        resources_available[res] -= being_harvested;
                                    }
                                }
                            }
                        });
                    });
                }
                resource_map.forEach(res => {
                    if (is_players_settlement && field.stored) {
                        stored_resources[res].count += field.stored[res];
                        stored_resources[res].mod += field.stored_mod && field.stored_mod[res] || 0;
                        if (field.stored[res]) {
                            if (!this.stored_resources_map[node]) {
                                this.stored_resources_map[node] = resources_map_helper(0);
                            }
                            this.stored_resources_map[node][res] = field.stored[res];
                        }
                    }
                    if (field.raw && field.raw[res]) {
                        if (resources_claimed[res] == 1) {
                            claimed_resources[res].count += field.raw[res];
                            claimed_resources[res].mod += field.raw_mod && field.raw_mod[res] || 0;
                        }
                        else if (resources_claimed[res] == 0) {
                            this.unclaimed_resources_map[res][node] = field.raw[res];
                        }
                    }
                });
            });
        });

        this.stored_resources_$.next(stored_resources);
        this.claimed_resources_$.next(claimed_resources);

        console.log('being_harversted_resources', this.being_harversted_resources);
    }

    add_building_task(building_id: number, task_type: number, task_data?: any) {
        if (typeof task_data === 'object') {
            task_data = JSON.stringify(task_data);
        }

        ApiService.post<ITask[]>('add_building_task', {building_id, task_type, task_data})
            .subscribe(data => {
                if (building_id) {
                    this.map.fields[ this.selected_hex[1] ][  this.selected_hex[0] ].buildings[building_id.toString()].tasks = data;
                    this.calculate_resources_stats();
                    this.map.update_tile_ui(this.map.fields[ this.selected_hex[1] ][  this.selected_hex[0] ]); // TODO: обновлять только задания?
                }
                else {
                    this.global_tasks_queue = data;
                    this.update_global_queue_ui();
                }
            });
    }

    remove_building_task(building_id: number, id: number) {
        ApiService.get<ITask[]>('remove_building_task', {id})
            .subscribe(data => {
                if (building_id) {
                    this.map.fields[ this.selected_hex[1] ][  this.selected_hex[0] ].buildings[building_id].tasks = data;
                    this.calculate_resources_stats();
                    this.map.update_tile_ui(this.map.fields[ this.selected_hex[1] ][  this.selected_hex[0] ]); // TODO: обновлять только задания?
                }
                else {
                    this.global_tasks_queue = data;
                    this.update_global_queue_ui();
                }
            });
    }

    update_global_queue_ui() {
        global_queue_container.removeAllChildren();
        global_queue_road_shape.graphics.clear();
        global_queue_road_shape.graphics.f(null).ss(5, 'round');
        console.log('update_global_queue_ui', this.global_tasks_queue);

        // const settlement_buildings_num: {
        //     [name: string]: number;
        // } = {};

        this.ui.resetBuildOverlays();

        this.global_tasks_queue.forEach(task => {
            const task_data: any = JSON.parse(task.task_data);


            switch (task.task_type) {
                case HS.TASK_TYPE_BUILD_HOUSE:
                case HS.TASK_TYPE_BUILD_SAWMILL:
                case HS.TASK_TYPE_BUILD_MINE:
                case HS.TASK_TYPE_BUILD_FARM:
                case HS.TASK_TYPE_BUILD_BARRACKS:
                case HS.TASK_TYPE_BUILD_SHOOTING_RANGE:
                case HS.TASK_TYPE_BUILD_STADIUM:
                    this.ui.addBuildOverlay(task.task_type, task_data);
                    break;
                case HS.TASK_TYPE_TRANSPORT_RESOURCE:
                    let [hx, hy] = this.map.hex_to_position(task_data.from[0], task_data.from[1]);
                    global_queue_road_shape.graphics.s('rgba(255,0,0,0.5)').mt(hx, hy);
                    [hx, hy] = this.map.hex_to_position(task_data.to[0], task_data.to[1]);
                    global_queue_road_shape.graphics.lt(hx, hy);

                    const cancel_string = 'Отменить караван: ' + task_data.wood + '/' + task_data.gold + '/' + task_data.food + ' [' + task.ap_cost + ']';
                    global_queue_container.addChild(
                        this.map.add_army_button('img/cancel_big.png', hx + 18, hy, cancel_string, event => {
                            this.remove_building_task(0, task.id);
                            event.stopImmediatePropagation();
                        })
                    );
                    break;
            }
        });

        this.stageUpdate();
    }

    refresh_game_info() {
        clearTimeout(this.info_timeout);
        this.get_game_info(data => {

            data.players.forEach((player) => {
                this.players[player.user_id].update(player);
            });

            const number_of_undefeated = Object.keys(this.players)
                .filter(id => this.players[id].defeated).length;

            if (Object.keys(this.players).length > 1 && this.the_winner === undefined && number_of_undefeated === 1) {
                this.the_winner = Object.keys(this.players)
                    .find(id => !!this.players[id].defeated);

                if (this.the_winner === this.user.id) {
                    alert("Вы одержали победу!");
                }
                else {
                    alert("Игрок " + this.the_winner + " одержал победу!");
                }
            }

            console.log('turn', this.turn, data.campaign.turn);
            this.turn_text.text = data.campaign.turn;
            if (this.turn !== data.campaign.turn) {
                this.map.reload();
            }
            this.turn = data.campaign.turn;
            this.stageUpdate();
            this.ui.update();

            this.info_timeout = setTimeout(() => this.refresh_game_info(), 10000);
        });
    }

    get_player_log() {
        ApiService.get<any[][]>('get_player_log', {turn: parseInt(this.turn) - 1})
            .subscribe(data => {
                let log = '';
                data.forEach(logData => {
                    let line: string;
                    switch (logData[0]) {
                        case HS.LOG_TYPE_ARMY_MOVE:
                            line =
                                'Армия ' + logData[1] + ' двигается из ' + logData[2] + ' в ' + logData[3]
                                + ' за ' + logData[4];
                            break;
                        case HS.LOG_TYPE_ARMY_INTERCEPT:
                            line =
                                'Армия ' + logData[1] + ' преследует армию ' + logData[2];
                            break;
                        case HS.LOG_TYPE_ARMY_FIGHT:
                            console.log(logData);
                            line =
                                'Армии ' + logData[1].join(', ') + ' сражаются в ' + logData[2]
                                + ' с силами игроков ' + logData[3].join(', ');
                            line += "\nВаши силы: " + logData[4].join(', ');
                            line += "\nСилы врагов:";
                            logData[5].forEach((troops: string[]) => {
                                line += ' ' + troops.join(', ');
                            });
                            break;
                        case HS.LOG_TYPE_ARMY_FORMATION:
                            line =
                                'Армия ' + logData[1] + ' перестроилась в '
                                + (logData[2] ? 'боевой' : 'походный') + ' порядок';
                            break;
                        case HS.LOG_TYPE_HIRE_UNIT:
                            line =
                                'В здании ' + logData[1] + ' был нанят отряд ' + unit_names[ logData[2] ]
                                + ', отправленный в ' + (logData[4] ? 'новую' : '') + ' армию ' + logData[5];
                            break;
                        case HS.LOG_TYPE_BUILD_BUILDING:
                            line =
                                building_names[ logData[2] ] + ' построено в ' + logData[1];
                            break;
                        case HS.LOG_TYPE_HARVEST_RESOURCE:
                            line =
                                resource_names[ logData[1] ] + ' собрано в здании '
                                + logData[3] + '. На складе ' + logData[4] + ', в тайле осталось ' + logData[5];
                            break;
                        case HS.LOG_TYPE_LOAD_RESOURCE:
                            line =
                                resource_names[ logData[1] ] + ' загружено в ' + (logData[2] === 'building' ? 'здание ' : 'отряд ')
                                + logData[3] + ' из ' + (logData[4] === 'building' ? 'здания ' : 'отряда ') + logData[5]
                                + '. В запасе ' + logData[6] + ', у источника осталось ' + logData[7];
                            break;
                        case HS.LOG_TYPE_UNLOAD_RESOURCE:
                            line =
                                resource_names[ logData[1] ] + ' выгружено из ' + (logData[2] === 'building' ? 'здания ' : 'отряда ')
                                + logData[3] + ' в ' + (logData[4] === 'building' ? 'здание ' : 'отряд ') + logData[5]
                                + '. В запасе осталось ' + logData[6] + ', у цели ' + logData[7];
                            break;
                        case HS.LOG_TYPE_FAIL:
                            if (logData[3] === 'global') {
                                line =
                                    'Не получилось ' + task_names[ logData[1] ] + ' по причине: '
                                    + log_action_fail_reason_names[ logData[2] ];
                                if (logData[4]) {
                                    line += ' (' + logData[4] + ')';
                                }
                            }
                            else {
                                line =
                                    'У ' + (logData[3] === 'building' ? 'здания ' : 'отряда ') + logData[4]
                                    + ' не получилось ' + task_names[ logData[1] ] + ' по причине: '
                                    + log_action_fail_reason_names[ logData[2] ];
                            }
                            break;
                    }
                    log += line + ".\n";
                });
                console.log(log);
                alert(log);
            });
    }

    show_stats() {
        let res;
        let text = "На складах всего:\n";

        for (res in resource_names) {
            text += resource_names[res] + ': ' + this.stored_resources_$[res] + ' ';
        }

        text += "\nДобыча:\n";

        for (res in resource_names) {
            text += resource_names[res] + ': +' + this.being_harversted_resources[res].total + '/' + this.claimed_resources_$[res] + ' ';
        }

        console.log(text);
        alert(text);
    }

    surrender() {
        if ( !confirm('Точно сдаться?') ) {
            return;
        }

        ApiService.get('surrender', {})
            .subscribe(() => {
                this.refresh_game_info();
            });
    }

    toggle_grid() {
        this.map.overlay_container.visible = !this.map.overlay_container.visible;
        this.stageUpdate();
    }

    key_pressed(event: KeyboardEvent) {
        switch (event.code) {
            case 'KeyG':
                this.toggle_grid();
                break;
        }
    }
}

export function resources_map_helper<T>(value: T): {[name: string]: T} {
    return resource_map.reduce((result, res) => {
        if (typeof value === 'object') {
            if (isDefined(value['slice'])) {
                result[res] = [...(value as any)];
            } else {
                result[res] = {...(value as Object)};
            }
        } else {
            result[res] = value;
        }

        return result;
    }, {});
}

export const tile_type_colors = [
    undefined,
    '#6b8d34',
    '#205e0a',
    '#ebe96b',
    '#373737',
    '#1a7ab6',
];
export const tile_image_names: {[name: string]: string}[] = [
    undefined,
    {'background': 'plains'},
    {'background': 'forest',
        '0000': 'forest_0000', '0001': 'forest_0001', '0010': 'forest_0010', '0011': 'forest_0011',
        '0100': 'forest_0100', '0101': 'forest_0101', '0110': 'forest_0110', '0111': 'forest_0111',
        '1000': 'forest_1000', '1001': 'forest_1001', '1010': 'forest_1010', '1011': 'forest_1011',
        '1100': 'forest_1100', '1101': 'forest_1101', '1110': 'forest_1110', '1111': 'forest_1111'
    },
    {'background': 'hills'},
    {'background': 'mountains'},
    {'background': 'water'},
];
export const tile_image_shift = [52, 76];
export const building_origins = [
    [ -25, -35],
    [  25, -35],
    [ -25,  15],
    [  25,  15],
];
export const armies_origins = [
    [0, 5],
    [-25, 5],
    [25, 5],
    [-50, 5],
    [50, 5],
    [0, 25],
    [-25, 25],
    [25, 25],
    [-50, 25],
    [50, 25],

];
// TODO: объекты
export const building_names = [
    'Руины',
    'Лесопилка',
    'Дом',
    '',
    'Шахта',
    'Ферма',
    'Бараки',
    'Стрельбище',
    'Ристалище',
];
export const building_image_names = [
    'ruin',
    'sawmill',
    'house',
    'carts_workshop',
    'mine',
    'farm',
    'barracks',
    'shooting_range',
    'stadium',
];
export const task_names = [
    '',                       // 0
    'Добывать дерево',        // 1
    'Добывать золото',        // 2
    'Добывать еду',           // 3
    'Назначить цель',         // 4
    'Перевезти ресурсы',      // 5
    '',                       // 6
    '',                       // 7
    '',                       // 8
    'Строить дорогу',         // 9
    'Строить дом',            // 10
    'Строить лесопилку',      // 11
    'Строить мастерскую',     // 12
    'Строить шахту',          // 13
    'Строить ферму',          // 14
    'Строить бараки',         // 15
    'Строить стрельбище',     // 16
    'Строить ристалище',      // 17
    '',                       // 18
    '',                       // 19
    '',                       // 20
    'Подготовить пехотинца',  // 21
    'Подготовить лучника',    // 22
    'Подготовить всадника',   // 23
    'Разрушить здание',       // 24
    'Присоединиться к армии', // 25
];
export const task_images_names = [
    '',                                           // 0
    'axe',                            // 1
    'pickaxe',                        // 2
    'apple',                          // 3
    'wagon',                          // 4
    'wagon',                          // 5
    '',                                           // 6
    '',                                           // 7
    '',                                           // 8
    'road',                           // 9
    'house',   // 10
    'sawmill',         // 11
    'carts_workshop',  // 12
    'mine',            // 13
    'farm',            // 14
    'barracks',        // 15
    'shooting_range',  // 16
    'stadium',         // 17
    '',                                           // 18
    '',                                           // 19
    '',                                           // 20
    'pikeman',                        // 21
    'bowman',                         // 22
    'knight',                         // 23
    'burn',                           // 24
    'split',                          // 25
];
export const task_images = [
    '',                                           // 0
    'img/axe.png',                            // 1
    'img/pickaxe.png',                        // 2
    'img/apple.png',                          // 3
    'img/wagon.png',                          // 4
    'img/wagon.png',                          // 5
    '',                                           // 6
    '',                                           // 7
    '',                                           // 8
    'img/road.png',                           // 9
    'img/buildings/workers_guild_gray.png',   // 10
    'img/buildings/sawmill_gray.png',         // 11
    'img/buildings/carts_workshop_gray.png',  // 12
    'img/buildings/mine_gray.png',            // 13
    'img/buildings/farm_gray.png',            // 14
    'img/buildings/barracks_gray.png',        // 15
    'img/buildings/shooting_range_gray.png',  // 16
    'img/buildings/stadium_gray.png',         // 17
    '',                                           // 18
    '',                                           // 19
    '',                                           // 20
    'img/pikeman.png',                        // 21
    'img/bowman.png',                         // 22
    'img/knight.png',                         // 23
    'img/burn.png',                           // 24
    'img/split.png',                          // 25
];

const TASK_TYPE_GLOBAL_BUILD        = 1;
const TASK_TYPE_NORMAL              = 2;
const TASK_TYPE_GLOBAL_TARGETED     = 3;
const TASK_TYPE_TRANSPORT_RESOURCES = 4;
const TASK_TYPE_HIRE                = 5;
export const TASK_TYPE = { // TODO: удалить строки выше и использовать это
    GLOBAL_BUILD:        1,
    NORMAL:              2,
    GLOBAL_TARGETED:     3,
    TRANSPORT_RESOURCES: 4,
    HIRE:                5,
};
export const task_types = [
    0,                             // 0
    TASK_TYPE_NORMAL,              // 1
    TASK_TYPE_NORMAL,              // 2
    TASK_TYPE_NORMAL,              // 3
    TASK_TYPE_GLOBAL_TARGETED,     // 4
    TASK_TYPE_TRANSPORT_RESOURCES, // 5
    0,                             // 6
    0,                             // 7
    0,                             // 8
    0,                             // 9
    TASK_TYPE_GLOBAL_BUILD,        // 10
    TASK_TYPE_GLOBAL_BUILD,        // 11
    0,                             // 12
    TASK_TYPE_GLOBAL_BUILD,        // 13
    TASK_TYPE_GLOBAL_BUILD,        // 14
    TASK_TYPE_GLOBAL_BUILD,        // 15
    TASK_TYPE_GLOBAL_BUILD,        // 16
    TASK_TYPE_GLOBAL_BUILD,        // 17
    0,                             // 18
    0,                             // 19
    0,                             // 20
    TASK_TYPE_HIRE,                // 21
    TASK_TYPE_HIRE,                // 22
    TASK_TYPE_HIRE,                // 23
    0,                             // 24
];

export const unit_names = [
    '',
    'Инженер',
    'Телега',
    'Пехота',
    'Лучники',
    'Кавалерия',
];
export const unit_images_names = [
    '',
    'miner',
    'wagon',
    'pikeman',
    'bowman',
    'knight',
];
export const unit_images = unit_images_names
    .map(name => name ? `img/${name}.png` : '');
export const directions = [
    [ // смещения по сетке для четных рядов
        [1, 0],   // вправо
        [0, 1],   // вправо вниз
        [-1, 1],  // влево вниз
        [-1, 0],  // влево
        [-1, -1], // влево вверх
        [0, -1],  // вправо вверх
    ],
    [ // смещения по сетке для нечетных рядов
        [1, 0],  // вправо
        [1, 1],  // вправо вниз
        [0, 1],  // влево вниз
        [-1, 0], // влево
        [0, -1], // влево вверх
        [1, -1], // вправо вверх
    ]
];
export const opposite_directions = [3, 4, 5, 0, 1, 2];

enum Resources {
    wood = 'wood',
    gold = 'gold',
    food = 'food',
}

export const resource_map = [
    Resources.wood,
    Resources.gold,
    Resources.food,
];

export const resource_names: {
    [name: string]: string;
} = {
    [Resources.wood]: 'Дерево',
    [Resources.gold]: 'Золото',
    [Resources.food]: 'Еда',
};
export const resource_harvesters = {
    [Resources.wood]: HS.BUILDING_TYPE_SAWMILL,
    [Resources.gold]: HS.BUILDING_TYPE_MINE,
    [Resources.food]: HS.BUILDING_TYPE_FARM
};
export const resource_from_harvesters = Object.keys(resource_harvesters).reduce((result, res) => {
    result[resource_harvesters[res]] = res;
    return result;
}, {});
const log_action_fail_reason_names: {
    [name: string]: string;
} = {
    'no_resource':     'Не хватает ресурса',
    'no_room':         'Не хватает места',
    'target_empty':    'Нечего брать',
    'target_no_room':  'Некуда класть',
    'empty':           'Нечего брать',
    'no_room_in_tile': 'Негде строить',
};

export type Callback = (eventObj: ICreateJsEvent) => void;

export const game = new Game();

let global_queue_container: Container;
let global_queue_road_shape: Shape;

let building_ui_shape;

(window as any).init = () => {
    const id_match = document.location.search.match(/id=(\w+)/);

    if (!id_match || typeof id_match[1] === undefined) {
        return;
    }

    preload_images(progress => {
        document.getElementById("progress").innerText = progress.toString();
    })
        .subscribe(() => {
            game.init('game-canvas', 'minimap-canvas', id_match[1]);
            document.getElementById("splash").style.setProperty("display", "none");
        });
};