import {game} from './game';
import {Observable} from 'rxjs/Observable';
import {BuildingsBlock} from './ui/buildingsBlock';
import {TurnTrackerPanel} from './ui/turnTrackerPanel';
import {TopPanel} from './ui/topPanel';
import {GameOverlay} from './ui/GameOverlay';
import {FieldInfo} from './ui/fieldInfo/FieldInfo';
import {BuildOverlay} from './ui/BuildOverlay';
import {HarvestTargetDisplay} from './ui/HarvestTargetDisplay';

export const BUILDINGS_BUTTON_POSITION = 1;
export const LOGISTIC_VISOR_BUTTON_POSITION = 2;

export class UI {
    actions_panel = document.querySelector('actions-panel');
    hide_button = document.querySelector('hide-actions-panel-button');
    buildings_block = new BuildingsBlock(this);
    turn_tracker_panel = new TurnTrackerPanel();
    game_overlay = new GameOverlay();
    top_panel = new TopPanel();
    field_info = new FieldInfo();
    build_overlays = [];
    harvest_target_display: HarvestTargetDisplay;

    constructor() {
        this.setHooks();
    }

    setHooks() {
        Observable.fromEvent(this.hide_button, 'click')
            .subscribe(() => {
                this.hide_button.classList.toggle('on');
                this.actions_panel.classList.toggle('hidden');
            });
        Observable.fromEvent(this.actions_panel.querySelector('action-button:nth-child('
                + BUILDINGS_BUTTON_POSITION + ')'), 'click')
            .subscribe(() => {
                this.buildings_block.toggle();
            });
        Observable.fromEvent(this.actions_panel.querySelector('action-button:nth-child('
                + LOGISTIC_VISOR_BUTTON_POSITION + ')'), 'click')
            .subscribe(() => {

                if (game.map.hover_callback) {
                    game.map.hover_callback = undefined;
                }
                else {
                    game.map.hover_callback = (x, y, q) => {
                        if (this.harvest_target_display) {
                            this.harvest_target_display.destroy();
                        }
                        this.harvest_target_display = new HarvestTargetDisplay(x, y, q);
                    };
                }
            });
    }

    update() {
        this.turn_tracker_panel.update();
    }

    init() {
        this.game_overlay.init();
    }

    resetBuildOverlays() {
        this.build_overlays.forEach(bol => {
            bol.destroy();
        });
        this.build_overlays = [];
    }
    addBuildOverlay(task_type: number, task_data) {
        let new_bol = new BuildOverlay(this);
        new_bol.init_existing(task_type, task_data);
        this.build_overlays.push(new_bol);
    }
}