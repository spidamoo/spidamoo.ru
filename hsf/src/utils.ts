import {ApiService} from './services/api.service';
import {Observable} from 'rxjs/Observable';

export function forEachObject<T>(obj: {[name: string]: T},
                                 callback: (value: T, index: string) => void) {
    Object.keys(obj).forEach((key) => {
        callback(obj[key], key);
    });
}

let images_store: {[name: string]: string};
let image_elements: {[name: string]: HTMLElementTagNameMap['img']} = {};
const IMG_PREFIX = 'data:image/png;base64,';

export function getImage(key: string) {
    if (!image_elements[key]) {
        image_elements[key] = document.createElement('img');
        image_elements[key].src = IMG_PREFIX + images_store[key];
    }

    return image_elements[key];
}

export function setSrcImage(selector: string, key: string) {
    document.querySelector<HTMLElement>(selector)
        .setAttribute('src', IMG_PREFIX + images_store[key]);
}

export function setCssImage(selector: string, key: string) {
    document.querySelector<HTMLElement>(selector).style
        .setProperty('background-image', 'url("' + IMG_PREFIX + images_store[key] + '")');
}

export function preload_images(progress: (progress: number) => void): Observable<{[name: string]: string}> {
    return ApiService.getResource<{[name: string]: string}>('images.json', {}, progress)
        .do((data: {[name: string]: string}) => {
            images_store = data;

            setUiImages();
        });
}

function setUiImages() {
    const head = document.getElementsByTagName('head')[0];
    const styleElement = document.createElement('style') as HTMLStyleElement;
    let gen = '';
    const styles = [
        {selector: '.wood_bg', img: 'img/ui/wood_bg.png'},
        {selector: 'actions-panel action-button:nth-child(1)', img: 'img/ui/buildings.png'},
        {selector: 'actions-panel action-button.active::before', img: 'img/ui/action-active.png'},
        {selector: '.army-icon', img: 'img/buildings/map/army_black.png'},
        {selector: '.barracks-icon', img: 'img/buildings/map/barracks_black.png'},
        {selector: '.farm-icon', img: 'img/buildings/map/farm_black.png'},
        {selector: '.house-icon', img: 'img/buildings/map/house_black.png'},
        {selector: '.mine-icon', img: 'img/buildings/map/mine_black.png'},
        {selector: '.sawmill-icon', img: 'img/buildings/map/sawmill_black.png'},
        {selector: '.shooting-icon', img: 'img/buildings/map/shooting_black.png'},
        {selector: '.stadium-icon', img: 'img/buildings/map/stadium_black.png'},
        {selector: '.sun-icon', img: 'img/ui/sun-icon.png'},
        {selector: '.wood-icon', img: 'img/ui/wood-res.png'},
        {selector: '.gold-icon', img: 'img/ui/gold-res.png'},
        {selector: '.food-icon', img: 'img/ui/food-res.png'},
        {selector: '.box-img', img: 'img/ui/box.png'},
        {selector: '.miner-icon', img: 'img/miner.png'},
        {selector: '.wagon-icon', img: 'img/wagon.png'},
        {selector: '.pikeman-icon', img: 'img/pikeman.png'},
        {selector: '.bowman-icon', img: 'img/bowman.png'},
        {selector: '.knight-icon', img: 'img/knight.png'},
    ];


    styles.forEach(style => {
        gen += `${style.selector}{background-image: url("${IMG_PREFIX}${images_store[style.img]}")}`;
    });

    styleElement.innerText = gen;

    head.appendChild(styleElement);
}

export function isDefined<T>(val: T) {
    return typeof val !== 'undefined';
}