import {
    armies_origins,
    building_image_names, building_names, building_origins, Callback, directions, game, opposite_directions,
    tile_image_names,
    tile_image_shift,
    tile_type_colors, unit_images, unit_names
} from '../game';
import {forEachObject, getImage} from '../utils';
import Shape = createjs.Shape;
import Container = createjs.Container;
import Bitmap = createjs.Bitmap;
import {IBuilding, ICreateJsEvent, ITarget, ITask, ITile, IUnit} from '../types/game.types';
import {ApiService} from '../services/api.service';
import {HS} from '../game.const';
import {Subject} from 'rxjs/Subject';
import {IResourceData, IStoreBoxEvent} from '../ui/resourceTransferBlock/StoreBoxBlock';

export interface IResourceInfoData {

}

export class Map {
    hex_R = 60;
    hex_r = this.hex_R * Math.sqrt(3) / 2;
    hex_w = 104;
    hex_h = 120;
    hex_half_w = this.hex_w * 0.5;
    hex_half_h = this.hex_h * 0.5;

    direction_borders = [
        [[+this.hex_half_w, -this.hex_half_h * 0.5], [+this.hex_half_w, +this.hex_half_h * 0.5]], // вправо
        [[+this.hex_half_w, +this.hex_half_h * 0.5], [0, +this.hex_half_h]], // вправо вниз
        [[0, +this.hex_half_h], [-this.hex_half_w, +this.hex_half_h * 0.5]], // влево вниз
        [[-this.hex_half_w, +this.hex_half_h * 0.5], [-this.hex_half_w, -this.hex_half_h * 0.5]], // влево
        [[-this.hex_half_w, -this.hex_half_h * 0.5], [0, -this.hex_half_h]], // влево вверх
        [[0, -this.hex_half_h], [+this.hex_half_w, -this.hex_half_h * 0.5]], // вправо вверх
    ];

    fields: ITile[][];
    target_callback: (target: ITarget) => void;

    container: Container;
    graph: Graph;
    bg_shape: Shape;
    tiles_container: Container;
    ui_container: Container;
    ui_shape: Shape;
    objects_container: Container;
    overlay_container: Container;
    tiles_subcontainers: Container[][];
    objects_subcontainers: Container[][];
    overlay_subcontainers: Container[][];
    selected_hex_shape: Shape;
    selected_army_container: Container;
    selected_army_path_shape: Shape;
    selected_army_buttons_container: Container;
    unit_ui_container: Container;
    tile_ui_container: Container;
    target_icons: {bitmap: Bitmap, target: ITarget}[] = [];
    army_move_icons: {
        [name: string]: Bitmap;
    };
    army_ok_icons: {
        [name: string]: Bitmap;
    };

    width = 10;
    height = 10;
    length = 100;
    width_px = this.hex_half_w * (this.width * 2 + 1);
    height_px = 1.5 * this.hex_half_h * (this.height - 1) + this.hex_h;

    selected_army_id: number;
    tile_callback: (x: number, y: number, q: number) => void;
    hover_callback: (x: number, y: number, q: number) => void;

    offset_$ = new Subject<{x: number; y: number;}>();

    constructor() {
        this.container = new Container();
        this.graph = new Graph();

        this.offset_$
            .subscribe(offset => {
                this.container.x = offset.x;
                this.container.y = offset.y;
            });
    }

    init() {
        this.bg_shape = new createjs.Shape();
        this.container.addChild(this.bg_shape);
        this.tiles_container = new createjs.Container();
        this.container.addChild(this.tiles_container);
        this.tiles_container.mouseEnabled = false;

        this.selected_hex_shape = new createjs.Shape();
        this.container.addChild(this.selected_hex_shape);
        this.selected_hex_shape.graphics.ss(5, 'round').s('blue').dp(0, 0, this.hex_R, 6, 0, 90);
        this.selected_hex_shape.visible = false;

        // this.ui_container.mouseEnabled = false;

        this.objects_container = new createjs.Container();
        this.container.addChild(this.objects_container);
        this.objects_container.mouseEnabled = false;

        this.overlay_container = new createjs.Container();
        this.container.addChild(this.overlay_container);
        this.overlay_container.visible = false;
        this.overlay_container.mouseEnabled = false;

        this.ui_shape = new Shape();
        this.container.addChild(this.ui_shape);
        this.ui_container = new createjs.Container();
        this.container.addChild(this.ui_container);

        this.selected_army_container = new createjs.Container();
        this.container.addChild(this.selected_army_container);
        this.selected_army_path_shape = new createjs.Shape();
        this.selected_army_container.addChild(this.selected_army_path_shape);
        this.selected_army_buttons_container = new createjs.Container();
        this.selected_army_container.addChild(this.selected_army_buttons_container);

        this.tile_ui_container = new Container();
        game.stage.addChild(this.tile_ui_container);

        this.unit_ui_container = new Container();
        game.stage.addChild(this.unit_ui_container);

        this.initHooks();
    }

    initHooks() {
        let drag = false;
        let dragged: number;
        let drag_x: number;
        let drag_y: number;

        this.container.on("mousedown", (event: ICreateJsEvent) => {
            drag = true;
            dragged = 0;
            drag_x = event.rawX - this.container.x;
            drag_y = event.rawY - this.container.y;
        });
        this.container.on("pressup", (event: ICreateJsEvent) => {
            drag = false;

            if (dragged < 2) {
                const [x, y, q] = this.position_to_hex(event.localX, event.localY);

                if (this.selected_army_id) { // TODO: колбеки?
                    this.set_path_for_army(this.selected_army_id, x, y);
                }
                else if (this.tile_callback) {
                    if ( !this.tile_callback(x, y, q) ) {
                        this.tile_callback = undefined;
                        this.select_hex(x, y);
                    }
                }
                else if (game.global_task) {
                    game.reset_global_task();
                    this.select_hex(x, y);
                }
                else {
                    this.select_hex(x, y);
                }
            }
            game.stageUpdate();
        });
        this.container.on("rollout", () => {
            drag = false;
        });
        this.container.on("dblclick", () => {

            // stage.update();
        });

        game.stage.on("pressmove", (event: ICreateJsEvent) => {
            if (drag) {
                this.offset_$.next({x: event.rawX - drag_x, y: event.rawY - drag_y});

                dragged++;
                // if (map.x + < -1) {
                //     map.x = -1;
                // }
                // if (map.y < -1) {
                //     map.y = -1;
                // }
                // if (map.x > screen_width - map_width_px) {
                //     map.x = screen_width - map_width_px;
                // }
                // if (map.y > screen_height - map_height_px) {
                //     map.y = screen_height - map_height_px;
                // }
                game.minimap.update_screen();
                game.stageUpdate();
            }
        });

        game.stage.on("stagemousemove", (event: ICreateJsEvent) => {
            if (this.hover_callback) {
                const [x, y, q] = this.position_to_hex(event.localX - this.container.x, event.localY - this.container.y);
                if (typeof game.hovered_hex === 'undefined' || game.hovered_hex[0] != x || game.hovered_hex[1] != y || game.hovered_hex[2] != q) {
                    game.hovered_hex = [x, y, q];
                    this.hover_callback(x, y, q);
                    game.stageUpdate();
                }
            }
        });
    }

    // координаты хекса => координаты в пикселях
    hex_to_position(x: number, y: number): [number, number] {
        let hx = x * this.hex_w + this.hex_half_w;
        const hy = y * this.hex_half_h * 1.5 + this.hex_half_h;

        if (y % 2 === 1) {
            hx += this.hex_half_w;
        }

        return [hx, hy];
    }

    // координаты в пикселях => координаты хекса и четверть
    position_to_hex(x: number, y:number): [number, number, number] {
        const hy  = (y - this.hex_half_h) / (this.hex_half_h * 1.5);
        const hyr = Math.round(hy);
        const hx  = ( (hyr % 2) ? (x - this.hex_w) : (x - this.hex_half_w) ) / this.hex_w;
        const hxr = Math.round(hx);
        let q;
        if (hy < hyr) {
            if (hx < hxr) {
                q = 0;
            }
            else {
                q = 1;
            }
        }
        else {
            if (hx < hxr) {
                q = 2;
            }
            else {
                q = 3;
            }
        }

        return [hxr, hyr, q];
    }

    draw_hex(x: number, y: number, tile: ITile, highlighted?: boolean) {
        if (tile.fow === 0) {
            return;
        }

        const subcontainer_x = Math.floor(x / (game.screen_width_hex));
        const subcontainer_y = Math.floor(y / (game.screen_height_hex));

        if (!this.tiles_subcontainers[subcontainer_y]) {
            this.tiles_subcontainers  [subcontainer_y] = [];
            this.objects_subcontainers[subcontainer_y] = [];
            this.overlay_subcontainers[subcontainer_y] = [];
        }

        if (!this.tiles_subcontainers[subcontainer_y][subcontainer_x]) {
            this.tiles_subcontainers  [subcontainer_y][subcontainer_x] = new createjs.Container();
            this.objects_subcontainers[subcontainer_y][subcontainer_x] = new createjs.Container();
            this.overlay_subcontainers[subcontainer_y][subcontainer_x] = new createjs.Container();
            this.tiles_container.addChild(  this.tiles_subcontainers  [subcontainer_y][subcontainer_x]);
            this.objects_container.addChild(this.objects_subcontainers[subcontainer_y][subcontainer_x]);
            this.overlay_container.addChild(this.overlay_subcontainers[subcontainer_y][subcontainer_x]);

            this.overlay_subcontainers[subcontainer_y][subcontainer_x].addChild(new createjs.Shape() );
        }

        const tiles_sc = this.tiles_subcontainers  [subcontainer_y][subcontainer_x];
        const objects_sc = this.objects_subcontainers[subcontainer_y][subcontainer_x];
        const overlay_sc = this.overlay_subcontainers[subcontainer_y][subcontainer_x];

        const grid_shape = this.overlay_subcontainers[subcontainer_y][subcontainer_x].getChildAt(0) as Shape;

        const [hx, hy] = this.hex_to_position(x, y);

        let tile_image_name = 'img/tiles/';
        if (tile.fow === 1) {
            tile_image_name += 'shaded/';
        }
        if (highlighted !== undefined && !highlighted) {
            tile_image_name += 'darkened/';
        }
        tile_image_name += tile_image_names[tile.tile_type]['background'] + '.png';
        let tile_bitmap = new createjs.Bitmap(getImage(tile_image_name));

        tiles_sc.addChild(tile_bitmap);
        tile_bitmap.x = hx - tile_image_shift[0];
        tile_bitmap.y = hy - tile_image_shift[1];

        if (tile.roads) {
            for (let direction = 0; direction < 6; direction++) {
                if ( tile.roads & (1 << direction) ) {
                    tile_bitmap = new createjs.Bitmap(getImage('img/tiles/road/road_' + direction + '.png'));
                    tiles_sc.addChild(tile_bitmap);
                    tile_bitmap.x = hx - tile_image_shift[0];
                    tile_bitmap.y = hy - tile_image_shift[1];
                }
            }
        }

        let buildings1;
        let buildings2;

        if (tile.buildings) {
            buildings1 = Object.keys(tile.buildings)
                .map(key => tile.buildings[key])
                .filter(building => building.slot <  2)
                .sort((a, b) => a.slot - b.slot);
            buildings2 = Object.keys(tile.buildings)
                .map(key => tile.buildings[key])
                .filter(building => building.slot >= 2)
                .sort((a, b) => a.slot - b.slot);
        }

        if (buildings1) {
            buildings1.forEach(building => this.draw_building(building, objects_sc, hx, hy));
        }

        if (tile.armies) {
            let i = 0;
            let army_bitmap;

            forEachObject(tile.armies, army => {
                let color = game.players[ army.user_id ] ? game.players[ army.user_id ].color : 'gray';
                army_bitmap = new createjs.Bitmap(getImage('img/buildings/map/army_' + color + '.png'));
                objects_sc.addChild(army_bitmap);
                army_bitmap.x = hx + (armies_origins[i] ? armies_origins[i][0] : 0) - 16;
                army_bitmap.y = hy + (armies_origins[i] ? armies_origins[i][1] : 0) - 41;
                i++;
            });
        }

        if (buildings2) {
            buildings2.forEach(building => this.draw_building(building, objects_sc, hx, hy));
        }

        if (tile.fow === 2) {
            const slots_taken_string = tile.slots_taken.join('');
            if (tile_image_names[tile.tile_type][slots_taken_string]) {
                tile_image_name = 'img/tiles/';
                if (highlighted !== undefined && !highlighted) {
                    tile_image_name += 'darkened/';
                }
                tile_image_name += tile_image_names[tile.tile_type][slots_taken_string] + '.png'
                tile_bitmap = new createjs.Bitmap(getImage(tile_image_name));
                objects_sc.addChild(tile_bitmap);
                tile_bitmap.x = hx - tile_image_shift[0];
                tile_bitmap.y = hy - tile_image_shift[1];
            }
        }

        grid_shape.graphics.ss(1).f(null).s('white').dp(hx, hy, this.hex_R, 6, 0, 90);

        let tile_resource_bitmap;
        let tile_resource_text;

        if (tile.raw) {
            if (tile.raw.wood) {
                tile_resource_bitmap = new createjs.Bitmap(getImage('img/wood.png'));
                tile_resource_text = new createjs.Text(tile.raw.wood.toString(), '14px Arial', 'black');
            }
            else if (tile.raw.gold) {
                tile_resource_bitmap = new createjs.Bitmap(getImage('img/gold.png'));
                tile_resource_text = new createjs.Text(tile.raw.gold.toString(), '14px Arial', 'black');
            }
            else if (tile.raw.food) {
                tile_resource_bitmap = new createjs.Bitmap(getImage('img/food.png'));
                tile_resource_text = new createjs.Text(tile.raw.food.toString(), '14px Arial', 'black');
            }
        }

        if (tile_resource_bitmap) {
            overlay_sc.addChild(tile_resource_bitmap);
            overlay_sc.addChild(tile_resource_text);
            tile_resource_bitmap.x = hx - 25;
            tile_resource_bitmap.y = hy - 35;
            tile_resource_text.x = tile_resource_bitmap.x + 16;
            tile_resource_text.y = tile_resource_bitmap.y + 18;
            tile_resource_text.textAlign = 'center';
            tile_resource_text.textBaseline = 'middle';
        }

        const tile_pos_text = new createjs.Text(x + ';' + y, '14px Arial', 'black');

        tile_pos_text.x = hx;
        tile_pos_text.y = hy;
        tile_pos_text.textAlign = 'center';
        tile_pos_text.textBaseline = 'middle';
        overlay_sc.addChild(tile_pos_text);
    }

    draw(highlighted_map?:Object) {
        game.now = window.performance.now();

        this.bg_shape.graphics.clear();
        this.bg_shape.graphics.f("lightgrey").rect(-2000, -2000, this.width_px + 4000, this.height_px + 4000);

        this.tiles_container.removeAllChildren();
        this.objects_container.removeAllChildren();
        this.overlay_container.removeAllChildren();

        this.ui_shape.graphics.clear();

        this.tiles_subcontainers   = [];
        this.objects_subcontainers = [];
        this.overlay_subcontainers = [];

        document.getElementById('loading_text').innerHTML = 'caching...';

        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                if (highlighted_map) {
                    const node = x + ';' + y;
                    this.draw_hex(x, y, this.fields[y][x], highlighted_map[node] || false);

                    if (highlighted_map[node]) {
                        this.ui_shape.graphics.s('rgba(56, 254, 65, 0.44)').ss(5, 'square');
                        const [hx, hy] = this.hex_to_position(x, y);
                        for (let dir of [0, 1, 2, 3, 4, 5]) {
                            const neighbor = (x + directions[y % 2][dir][0]) + ';' + (y + directions[y % 2][dir][1]);
                            if (!highlighted_map[neighbor]) {
                                this.ui_shape.graphics
                                    .mt(hx + this.direction_borders[dir][0][0], hy + this.direction_borders[dir][0][1])
                                    .lt(hx + this.direction_borders[dir][1][0], hy + this.direction_borders[dir][1][1]);
                            }
                        }
                    }
                }
                else {
                    this.draw_hex(x, y, this.fields[y][x]);
                }
            }
        }

        let left: number, top: number;
        const waiters: (() => void)[] = [];

        this.tiles_subcontainers.forEach((row, y) => {
            row.forEach((tile, x) => {
                const left_hex = x * (game.screen_width_hex);
                const top_hex = y * (game.screen_height_hex);

                [left, top] = this.hex_to_position(left_hex, top_hex);

                const cache_x = left - this.hex_half_w;
                const cache_y = top - this.hex_half_h;
                const cache_w = game.screen_width_hex * this.hex_w + this.hex_half_w;
                const cache_h = game.screen_height_hex * this.hex_half_h * 1.5 + this.hex_half_h * 0.5;

                waiters.push(() => tile.cache(cache_x, cache_y, cache_w, cache_h));
                waiters.push(() => this.objects_subcontainers[y][x].cache(cache_x, cache_y, cache_w, cache_h));
                waiters.push(() => this.overlay_subcontainers[y][x].cache(cache_x, cache_y, cache_w, cache_h));
            });
        });

        this.resolver(waiters);

        game.minimap.update_screen();

        game.overlay.visible = false;
        setTimeout(() => {
            game.stageUpdate();
        });
        console.log('draw_map', window.performance.now() - game.now);
    }

    resolver(waiters: (() => void)[]) {
        if (waiters.length === 0) {
            return;
        }

        setTimeout(() => {
            waiters.pop()();

            this.resolver(waiters);
        });
    }

    update_army_ui(army_id?: number, queue?: ITask[]) {
        this.selected_army_path_shape.graphics.clear();
        this.selected_army_buttons_container.removeAllChildren();

        for (let id in this.army_move_icons) {
            if (+id === army_id) {
                this.army_move_icons[id].visible = false;
                this.army_ok_icons[id].visible = true;
            }
            else {
                this.army_move_icons[id].visible = true;
                this.army_ok_icons[id].visible = false;
            }
        }

        if (!army_id) {
            this.selected_army_container.visible = false;
            game.stageUpdate();
            return false;
        }

        this.selected_army_container.visible = true;
        this.selected_army_path_shape.graphics.ss(5, 'round');

        let next_hx = game.selected_hex[0];
        let next_hy = game.selected_hex[1];
        let [next_x, next_y] = this.hex_to_position(next_hx, next_hy);

        let last_task;
        queue.forEach(task => {
            last_task = task;
            switch (task.task_type) {
                case HS.ARMY_TASK_TYPE_MOVE:
                    this.selected_army_path_shape.graphics.s('rgba(0,100,255,0.75)').mt(next_x, next_y);

                    next_hx += directions[next_hy % 2][task.task_data][0];
                    next_hy += directions[next_hy % 2][task.task_data][1];
                    [next_x, next_y] = this.hex_to_position(next_hx, next_hy);

                    this.selected_army_path_shape.graphics.lt(next_x, next_y);
                    break;
                case HS.ARMY_TASK_TYPE_INTERCEPT:
                    this.selected_army_path_shape.graphics.s('rgba(255,100,0,0.75)').mt(next_x, next_y);

                    [next_hx, next_hy] = game.armies_positions[task.task_data];
                    [next_x, next_y] = this.hex_to_position(next_hx, next_hy);
                    this.selected_army_path_shape.graphics.lt(next_x, next_y);
                    break;
                default:
                    return;
            }
        });
        game.selected_army.target_pos = [next_hx, next_hy];

        if (last_task) {
            this.selected_army_buttons_container.addChild(
                this.add_army_button('img/cancel_big.png', next_x + 18, next_y - 16, 'Отмена', event => {
                    game.remove_army_tasks(army_id);
                    event.stopImmediatePropagation();
                })
            );
        }

        this.selected_army_buttons_container.addChild(
            this.add_army_button('img/ok2.png', next_x - 18, next_y - 16, 'Закончить движение', event => {
                this.select_army();
                event.stopImmediatePropagation();
            })
        );

        game.armies_on_map.forEach(armies_location => {
            const [hx, hy] = this.hex_to_position(armies_location.x, armies_location.y);

            Object.keys(armies_location.armies).forEach((id, i) => {
                if (+id === army_id) {
                    return;
                }

                this.selected_army_buttons_container.addChild(
                    this.add_army_button(
                        'img/arrow_down.png',
                        hx + armies_origins[i][0],
                        hy + armies_origins[i][1],
                        'Перехват',
                        (event: ICreateJsEvent) => {
                            // remove_army_tasks(army.id);
                            this.add_army_tasks(army_id, [{
                                task_type: HS.ARMY_TASK_TYPE_INTERCEPT,
                                task_data: +id
                            }]);
                            event.stopImmediatePropagation();
                        }
                    )
                );
            });
        });

        game.stageUpdate();
    }

    set_path_for_army(army_id: number, x: number, y: number) {
        const from = game.selected_army.target_pos[0] + ';' + game.selected_army.target_pos[1];
        const to = x + ';' + y;

        console.log('set_path_for_army', game.selected_army, from, to);

        const now = window.performance.now();
        const path = this.graph.shortestPath(from, to)[to];

        console.log('took', window.performance.now() - now);

        const tasks: ITask[] = [];
        let prev_x = game.selected_army.target_pos[0];
        let prev_y = game.selected_army.target_pos[1];
        let next_x, next_y;

        for (let step in path) {
            [next_x, next_y] = path[step].split(';');

            const direction = game.determine_direction(prev_x, prev_y, +next_x, +next_y);

            // console.log('step', prev_x + ';' + prev_y + ' -> ' + pos[0] + ';' + pos[1], direction);
            // console.log(directions[prev_y % 2], pos[0] - prev_x, pos[1] - prev_y);
            tasks.push({task_type: HS.ARMY_TASK_TYPE_MOVE, task_data: direction});
            prev_x = +next_x;
            prev_y = +next_y;
        }

        this.add_army_tasks(army_id, tasks);
    }

    select_hex(x?: number, y?: number) {
        game.selected_hex = [x, y];
        this.select_army();
        this.update_unit_ui();

        if ( typeof x === 'undefined' ) {
            game.ui.field_info.toggle(false);
            this.selected_hex_shape.visible = false;
            this.update_tile_ui();
            return;
        }

        const [hx, hy] = this.hex_to_position(x, y);

        this.selected_hex_shape.x = hx;
        this.selected_hex_shape.y = hy;
        this.selected_hex_shape.visible = true;

        this.update_tile_ui(this.fields[y][x]);
        game.ui.field_info.setField(this.fields[y][x]);
        game.ui.field_info.toggle(true);
    }

    add_army_button(img: string, x: number, y: number, hint: string, action: Callback) {
        let bitmap = new createjs.Bitmap(getImage(img));

        bitmap.x = x - 16;
        bitmap.y = y - 16;
        bitmap.on('mouseover', () => {
            let point = bitmap.localToGlobal(0, 0);
            game.draw_hint(hint, point.x, point.y + 33);
        });
        bitmap.on('mouseout', () => {
            game.draw_hint();
        });
        bitmap.on('pressup', action);

        return bitmap;
    }

    add_army_task(army_id: number, task_type: number, task_data: number) {
        this.add_army_tasks(army_id, [{task_type, task_data}]);
    }

    add_army_tasks(army_id: number, tasks: ITask[]) {
        ApiService.post<ITask[]>('add_army_tasks', {army_id, tasks: JSON.stringify(tasks)})
            .subscribe(data => {
                this.update_army_ui(army_id, data);
            });
    }

    select_army(army_id?: number) {
        this.selected_army_id = army_id;

        if (!this.selected_army_id) {
            game.selected_army = undefined;
            this.update_army_ui();
            return;
        }

        game.selected_army = this.fields[ game.selected_hex[1] ][  game.selected_hex[0] ].armies[this.selected_army_id];

        ApiService.get<ITask[]>('get_army_details', {id: army_id})
            .subscribe(data => {
                this.update_army_ui(army_id, data);
            });
    }

    update_unit_ui(unit?: IUnit, x?: number, y?: number) {
        game.selected_unit = unit;

        this.unit_ui_container.removeAllChildren();

        if (!unit) {
            game.stageUpdate();
            return;
        }

        if (x !== undefined && y !== undefined) {
            this.unit_ui_container.x = x;
            this.unit_ui_container.y = y;
        }

        const unit_shape = new createjs.Shape();

        this.unit_ui_container.addChild(unit_shape);
        unit_shape.x = 0;
        unit_shape.y = 0;
        unit_shape.graphics.f('white').s('black').r(0, 0, 200, 80);

        this.unit_ui_container.addChild(
            this.add_unit_icon(unit, 0, 0, true)
        );

        let resource_text = new createjs.Text(unit.wood, '14px Arial', 'black');
        this.unit_ui_container.addChild(resource_text);
        resource_text.x = 150;
        resource_text.y = 5;

        resource_text = new createjs.Text(unit.gold, '14px Arial', 'black');
        this.unit_ui_container.addChild(resource_text);
        resource_text.x = 170;
        resource_text.y = 5;

        resource_text = new createjs.Text(unit.food, '14px Arial', 'black');
        this.unit_ui_container.addChild(resource_text);
        resource_text.x = 190;
        resource_text.y = 5;

        if (unit.user_id === game.user.id) {
            // for (var i = 0; i < HS.unit_tasks[unit.unit_type].length; i++) {
            //     var task = HS.unit_tasks[unit.unit_type][i];
            //     unit_ui_container.addChild(
            //         add_unit_task_icon(unit, task, 5 + i * 34, 42)
            //     );
            // }

            if (!game.unit_queues[unit.id]) {
                ApiService.get<ITask[]>('get_unit_queue', {unit_id: unit.id})
                    .subscribe(data => {
                        game.unit_queues[unit.id] = data;
                        this.update_unit_ui(unit); // TODO: переделать чтоб не обновлять все?
                    });
            }
            else {
                game.unit_queues[unit.id].forEach((task, i) => {
                    this.unit_ui_container.addChild(
                        game.add_unit_task_queue_icon(unit.id, task, 5 + (+i) * 34, 42)
                    );
                });
            }
        }

        game.stageUpdate();
    }

    update_tile_ui(tile?: ITile) {
        let ui_shift = 50;
        let ui_shift_x = 0;
        this.tile_ui_container.removeAllChildren();
        this.target_icons = [];
        this.army_move_icons = {};
        this.army_ok_icons = {};

        if (!tile) {
            return;
        }

        const tile_shape = new createjs.Shape();
        this.tile_ui_container.addChild(tile_shape);
        tile_shape.y = ui_shift;
        tile_shape.graphics.f('lightgrey').s('black').r(5, 5, 200, 20);

        if (tile.fow === 2 && tile.raw && tile.stored) {
            const target = game.selected_hex[0] + ';' + game.selected_hex[1];
            let harvest_str = game.being_harversted_resources.wood[target] ? '+' + game.being_harversted_resources.wood[target] : '';
            let resource_text = new createjs.Text(tile.stored.wood + '/' + tile.raw.wood + harvest_str, '12px Arial', 'black');

            this.tile_ui_container.addChild(resource_text);
            resource_text.x = 10 + ui_shift_x;
            resource_text.y = 10 + ui_shift;
            resource_text.textAlign = 'left';

            harvest_str = game.being_harversted_resources.gold[target] ? '+' + game.being_harversted_resources.gold[target] : '';
            resource_text = new createjs.Text(tile.stored.gold + '/' + tile.raw.gold + harvest_str, '12px Arial', 'black');
            this.tile_ui_container.addChild(resource_text);
            resource_text.x = 105 + ui_shift_x;
            resource_text.y = 10 + ui_shift;
            resource_text.textAlign = 'center';

            harvest_str = game.being_harversted_resources.food[target] ? '+' + game.being_harversted_resources.food[target] : '';
            resource_text = new createjs.Text(tile.stored.food + '/' + tile.raw.food + harvest_str, '12px Arial', 'black');
            this.tile_ui_container.addChild(resource_text);
            resource_text.x = 200 + ui_shift_x;
            resource_text.y = 10 + ui_shift;
            resource_text.textAlign = 'right';
        }

        ui_shift += 25;

        if (tile.buildings) {
            Object.keys(tile.buildings)
                .sort((a, b) => tile.buildings[a].slot - tile.buildings[b].slot)
                .forEach(i => {
                const building = tile.buildings[i];
                const building_shape = new createjs.Shape();

                this.tile_ui_container.addChild(building_shape);
                building_shape.y = ui_shift;
                building_shape.x = ui_shift_x;
                building_shape.graphics.f('lightgrey').s('black').r(5, 5, 200, 100);

                const building_text = new createjs.Text(
                    building_names[building.building_type]
                    + '(' + building.id + ')[' + building.building_data + ']',
                    '14px Arial', 'black'
                );

                this.tile_ui_container.addChild(building_text);
                building_text.x = 10 + ui_shift_x;
                building_text.y = 10 + ui_shift;

                this.tile_ui_container.addChild(
                    this.add_target_icon({type: 'building', building: building}, 130 + ui_shift_x, 10 + ui_shift)
                );

                let j = 0;

                if (building.user_id === game.user.id) {
                    forEachObject(HS.AVAILABLE_TASKS['building'][ building.building_type ], (active, task_type) => {
                        this.tile_ui_container.addChild(
                            game.add_task_icon(building.id, parseInt(task_type), 10 + j * 34 + ui_shift_x, 30 + ui_shift)
                        );
                        j++;
                    });

                    if (building.tasks) {
                        for (let j = 0; j < building.tasks.length; j++) {
                            const task = building.tasks[j];
                            this.tile_ui_container.addChild(
                                game.add_task_queue_icon(building.id, task, 10 + j * 34 + ui_shift_x, 64 + ui_shift)
                            );
                        }
                    }
                }

                ui_shift += 105;

                // TODO: функция
                if (ui_shift > game.screen_height - 105) {
                    ui_shift = 50;
                    ui_shift_x += 210;
                }
            });
        }

        if (tile.armies) {
            forEachObject(tile.armies, (army, army_id) => {
                const army_shape = new createjs.Shape();
                this.tile_ui_container.addChild(army_shape);
                army_shape.x = ui_shift_x;
                army_shape.y = ui_shift;
                army_shape.graphics.f('white').s('black').r(5, 5, 200, 100);

                const army_text = new createjs.Text('Армия ' + army_id, '14px Arial', 'black');
                this.tile_ui_container.addChild(army_text);
                army_text.x = 10 + ui_shift_x;
                army_text.y = 10 + ui_shift;

                if (army.user_id === game.user.id) {
                    this.army_move_icons[army_id] = game.add_icon('img/arrow.png', 10 + ui_shift_x, 30 + ui_shift, 'Двигаться', event => {
                        this.select_army(+army_id);
                    });
                    this.army_ok_icons[army_id] = game.add_icon('img/ok.png', 10 + ui_shift_x, 30 + ui_shift, 'Закончить движение', event => {
                        this.select_army();
                    });
                    this.army_ok_icons[army_id].visible = false;

                    this.tile_ui_container.addChild(this.army_move_icons[army_id]);
                    this.tile_ui_container.addChild(this.army_ok_icons[army_id]);
                    this.tile_ui_container.addChild(
                        game.add_icon('img/split.png', 44 + ui_shift_x, 30 + ui_shift, 'Разделить армию', () => {
                            this.target_callback = target => {
                                const unit_id = target.unit.id;

                                this.show_target_icons();
                                this.target_callback = target => {
                                    game.add_unit_task(unit_id, HS.TASK_TYPE_JOIN_ARMY, target.id);
                                    this.target_callback = undefined;
                                    this.show_target_icons(false);
                                };
                                this.show_target_icons(true, target =>
                                    target.type === 'army' && target.id && target.id !== parseInt(army_id)
                                );
                            };

                            this.show_target_icons(true, target =>
                                target.type === 'unit' && target.unit.army_id === parseInt(army_id)
                            );
                        })
                    );
                }

                army.units.forEach((unit, i) => {
                    this.tile_ui_container.addChild(
                       this.add_unit_icon(unit, 10 + i * 34 + ui_shift_x, 64 + ui_shift)
                    );
                    this.tile_ui_container.addChild(
                        this.add_target_icon({type: 'unit', unit: unit}, 18 + i * 34 + ui_shift_x, 72 + ui_shift)
                    );
                });

                this.tile_ui_container.addChild(
                    this.add_target_icon({type: 'army', id: +army_id}, 18 + (army.units.length - 1) * 34 + ui_shift_x, 72 + ui_shift)
                );

                ui_shift += 105;

                // TODO: функция
                if (ui_shift > game.screen_height - 105) {
                    ui_shift = 50;
                    ui_shift_x += 210;
                }
            });
        }
        this.tile_ui_container.addChild(
            this.add_target_icon({type: 'army', id: 0}, 18 + ui_shift_x, 22 + ui_shift)
        );
        game.stageUpdate();
    }

    add_unit_icon(unit: IUnit, x: number, y: number, deselect?: boolean) {
        return game.add_icon(unit_images[unit.unit_type], x, y, unit_names[unit.unit_type] + '(' + unit.id + ')', () => {
            if (deselect) {
                this.update_unit_ui();
            }
            else {
                this.update_unit_ui(unit, x, y);
            }
        });
    }

    show_target_icons(show?: boolean, filter?: (target: ITarget) => boolean) {
        this.target_icons.forEach(icon => {
            if ( filter && !filter(icon.target) ) {
                return;
            }

            icon.bitmap.visible = show;
        });
        game.stageUpdate();
    }

    add_target_icon(target: ITarget, x: number, y: number) {
        const bitmap = new createjs.Bitmap(getImage('img/arrow_small.png'));

        bitmap.x = x;
        bitmap.y = y;
        bitmap.on('mouseover', () => {
            // draw_hint(data, x, y + 17);
        });
        bitmap.on('mouseout', () => {
            // draw_hint(false);
        });
        bitmap.on('click', () => {
            this.target_callback(target);
        });

        bitmap.visible = false;
        this.target_icons.push({bitmap, target});

        return bitmap;
    }

    draw_building(building: IBuilding, objects_sc: Container, hx: number, hy: number) {
        const color = game.players[ building.user_id ] ? game.players[ building.user_id ].color : 'gray';
        let building_bitmap = new Bitmap(getImage('img/buildings/map/' + building_image_names[building.building_type] + '_' + color + '.png'));
        objects_sc.addChild(building_bitmap);
        building_bitmap.x = hx + building_origins[building.slot][0] - 20;
        building_bitmap.y = hy + building_origins[building.slot][1] - 30;
    }

    add_road_button(army_id: number, unit_id: number, direction: number, x: number, y: number) {
        return this.add_army_button('img/road_small.png', x, y, 'Строить дорогу', (event) => {
            game.add_unit_task(unit_id, 9, undefined, () => { // TODO: константа
                this.add_army_task(army_id, HS.ARMY_TASK_TYPE_MOVE, direction);
            });
            event.stopImmediatePropagation();
        });
    }

    add_target_settlement_button(settlement: ITarget, x: number, y: number) {
        return this.add_army_button('img/arrow_down_big.png', x - 18, y, 'Выбрать целью', event => {
            this.target_callback(settlement);
            game.global_task_current_container.removeAllChildren();
            game.stageUpdate();

            event.stopImmediatePropagation();
        });
    }

    reload() {
        game.players_settlements        = [];
        game.armies_on_map              = [];
        game.armies_positions           = {};
        game.now = window.performance.now();
        console.log('reload_map');
        game.overlay.visible = true;
        this.select_hex();
        game.reset_global_task();

        ApiService.get<ITile[][]>('get_map', {})
            .subscribe(data => {
                console.log('loaded', window.performance.now() - game.now);
                game.minimap_canvas.setAttribute('width', (data.length * 2).toString());
                game.minimap_canvas.setAttribute( 'height', (data[0].length * 2).toString());
                game.now = window.performance.now();
                this.fields = data;
                this.height = this.fields.length;
                this.width  = this.fields[0].length;
                this.width_px  = this.hex_half_w * (this.width * 2 + 1);
                this.height_px = 1.5 * this.hex_half_h * (this.height - 1) + this.hex_half_h * 2;
                this.length = this.height * this.width;

                this.fields.forEach((row, y) => {
                    row.forEach((field, x) => {
                        if (field.buildings) {
                            let is_players_settlement = false;

                            forEachObject(field.buildings, building => {
                                if (building.user_id === game.user.id) {
                                    is_players_settlement = true;

                                    if (game.capital === undefined || building.id < game.capital.id) {
                                        game.capital = {
                                            id: building.id,
                                            x:  x,
                                            y:  y,
                                        };
                                    }
                                }
                            });

                            if (is_players_settlement) {
                                game.players_settlements.push([x, y]);
                            }
                        }

                        if (field.armies) {
                            game.armies_on_map.push({armies: field.armies, x, y});
                            Object.keys(field.armies).forEach(army_id => {
                                game.armies_positions[army_id] = [x, y];
                            });
                        }

                        const neighbours: {
                            [name: string]: number;
                        } = {};
                        const road_neighbours: {
                            [name: string]: number;
                        } = {};

                        directions[y % 2].forEach(dir => {
                            const neighbour_x = x + dir[0];
                            const neighbour_y = y + dir[1];

                            if (neighbour_x < 0 || neighbour_x >= this.width || neighbour_y < 0 || neighbour_y >= this.height) {
                                return;
                            }

                            const cost = this.calculate_move_cost(x, y, neighbour_x, neighbour_y);

                            if (cost < 13) {
                                neighbours[neighbour_x + ';' + neighbour_y] = cost;
                            }

                            if ( this.is_there_road(x, y, neighbour_x, neighbour_y) ) {
                                road_neighbours[neighbour_x + ';' + neighbour_y] = 1;
                            }
                        });

                        this.graph.addVertex(x + ';' + y, neighbours);
                        game.roads_graph.addVertex(x + ';' + y, road_neighbours);

                        let color = tile_type_colors[field.tile_type] || 'transparent';
                        let user_id = undefined;

                        if (field.buildings) {
                            user_id = field.buildings[Object.keys(field.buildings)[0]].user_id;
                        }
                        else if (field.armies) {
                            user_id = field.armies[ Object.keys(field.armies)[0] ].user_id;
                        }

                        if (user_id) {
                            color = game.players[user_id].color
                        }

                        game.minimap.shape.graphics.f(color).s(null).rect(
                            x * game.minimap.tile_size + (y % 2) * game.minimap.tile_size * 0.5,
                            y * game.minimap.tile_size,
                            game.minimap.tile_size,
                            game.minimap.tile_size
                        );
                    });
                });

            console.log('being_harversted_resources', game.being_harversted_resources);

            const capital_pos = this.hex_to_position(game.capital.x, game.capital.y);

            this.offset_$.next({
                x: game.screen_width * 0.5 - capital_pos[0],
                y: game.screen_height * 0.5 - capital_pos[1]
            });

            game.minimap.screen_shape.x = game.minimap.shape.x - this.container.x * game.minimap.tile_size / this.hex_w;
            game.minimap.screen_shape.y = game.minimap.shape.y - this.container.y * game.minimap.tile_size / (this.hex_half_h * 1.5);

            game.minimap.shape.cache(0, 0, this.width * game.minimap.tile_size, this.height * game.minimap.tile_size);

            game.calculate_resources_stats();

            ApiService.get<ITask[]>('load_building_queue', {building_id: 0})
                .subscribe(data => {
                    game.global_tasks_queue = data;
                    game.update_global_queue_ui();
                });

            this.draw();
        });
    }

    calculate_move_cost(x1: number, y1: number, x2: number, y2: number): number {

        const tile1 = game.map.fields[y1][x1];
        const tile2 = game.map.fields[y2][x2];

        let tile1_type = tile1.buildings ? HS.TILE_TYPE_SETTLEMENT : tile1.tile_type;
        let tile2_type = tile2.buildings ? HS.TILE_TYPE_SETTLEMENT : tile2.tile_type;

        const direction_forth = game.determine_direction(x1, y1, x2, y2);
        const direction_back = opposite_directions[direction_forth];

        if ( tile1.roads & (1 << direction_forth) ) {
            tile1_type = HS.TILE_TYPE_ROAD;
        }

        if ( tile2.roads & (1 << direction_back) ) {
            tile2_type = HS.TILE_TYPE_ROAD;
        }

        return HS.TILE_MOVE_HALF_COST[tile1_type] + (HS.TILE_MOVE_HALF_COST as any)[tile2_type];
    }

    is_there_road(x1: number, y1: number, x2: number, y2: number): number {
        const tile1 = this.fields[y1][x1];
        const tile2 = this.fields[y2][x2];

        const direction_forth = game.determine_direction(x1, y1, x2, y2);
        const direction_back = opposite_directions[direction_forth];

        return ( tile1.roads & (1 << direction_forth) ) && ( tile2.roads & (1 << direction_back) );
    }

    building_at(x: number, y: number, q: number) {
        if (!this.fields[y] || !this.fields[y][x]) {
            return undefined;
        }
        if (!this.fields[y][x].buildings) {
            return undefined;
        }
        for (let id in this.fields[y][x].buildings) {
            if (this.fields[y][x].buildings[id].slot == q) {
                return this.fields[y][x].buildings[id];
            }
        }

        return undefined;
    }
}
