import {IPlayerData} from '../entities/player';

export interface IBuilding {
    id: number;
    user_id?: string;
    tasks?: any;
    slot?: number;
    building_type?: number;
    building_data?: any;
    x: number;
    y: number;
}

export interface IResource {
    [name: string]: number;
    total: number;
}

export interface ITask {
    id?: number;
    task_data: any;
    task_type: number;
    ap_cost?: number;
}

export interface IArmy {
    user_id: string;
    units: IUnit[];
    target_pos: number[];
}

export interface ITile {
    [name: string]: any;
    fow: number;
    raw?: {[name: string]: number};
    stored?: {[name: string]: number};
    raw_mod?: {[name: string]: number};
    stored_mod: {[name: string]: number};
    buildings: {[name: string]: IBuilding};
    tile_type: number;
    slots_taken: number[];
    armies: {[name: string]: IArmy};
    roads: number;
}

export interface ITarget {
    [index: number]: string;
    id?: number;
    type?: string;
    building?: IBuilding;
    unit?: IUnit;
}

export interface IUnit {
    id: number;
    user_id: string;
    unit_type: number;
    wood: string;
    gold: string;
    food: string;
    army_id: number;
}

// Здесь слит интерфейс всех типов событий
export interface ICreateJsEvent extends Event {
    nativeEvent: {
        shiftKey: boolean;
        altKey: boolean;
    };
    currentTarget: {
        x: number;
        y: number;
    } & EventTarget;
    localX: number;
    localY: number;
    rawX: number;
    rawY: number;
}

export interface IUser {
    id: string;
}

export interface ICampaign {
    id?: number;
    short_id: string;
    turn?: string;
}

export interface IGameInfo {
    user: IUser;
    campaign: ICampaign;
    players: IPlayerData[];
}