import {game, resource_names} from '../game';
import {forEachObject, isDefined} from '../utils';

export class TopPanel {
    element = document.querySelector('top-panel');
    resources = this.element.querySelector('resources');
    resource_vars_elements: {
        [name: string]: {
            icon: HTMLElement;
            count: HTMLElement;
            grow: HTMLElement
        }
    } = {};

    constructor() {
        this.render();
    }

    render() {
        this.resources.innerHTML = '';

        forEachObject(resource_names, (name, key) => {
            const resource = document.createElement('resource');
            const icon = document.createElement('icon');
            const count = document.createElement('count');
            const grow = document.createElement('grow');

            resource.classList.add(`res-${key}`);
            icon.classList.add(key + '-icon');
            count.innerText = '0';
            grow.innerText = '';

            resource.appendChild(icon);
            resource.appendChild(count);
            resource.appendChild(grow);

            this.resource_vars_elements[key] = {
                icon,
                count,
                grow,
            };

            this.resources.appendChild(resource);
        });

        game.stored_resources_$.subscribe(resources => {
            forEachObject(resources, (data, res) => {
                this.resources.querySelector(`.res-${res} count`).innerHTML = data.count.toString();
                this.resources.querySelector(`.res-${res} grow`).innerHTML =
                    (data.mod < 0 ? '' : '+') + data.mod.toString();
            });
        });
    }

    set_resources(resources: {[name: string]: {count?: string; grow?: string;}}) {
        forEachObject(resources, (res, key) => {
            if (isDefined(res.count)) {
                this.resource_vars_elements[key].count.innerText = res.count;
            }

            if (isDefined(res.grow)) {
                this.resource_vars_elements[key].grow.innerText = res.grow;

                this.resource_vars_elements[key].grow.classList
                    .toggle('down', res.grow[0] === '-');
            }
        });
    }
}