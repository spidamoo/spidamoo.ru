import {Observable} from 'rxjs/Observable';
import {HS} from '../game.const';
import {forEachObject, isDefined} from '../utils';
import {BUILDINGS_BUTTON_POSITION, UI} from '../ui';
import {game, resource_names, task_images_names, task_names, TASK_TYPE, task_types} from '../game';
import {BuildOverlay} from './BuildOverlay';
import {Subject} from 'rxjs/Subject';
import {BuildingBoxEventType, IBuildingBoxData, IBuildingBoxEvent} from './resourcesInfo/BuildingBoxBlock';

export const task_tabs = [
    [HS.TASK_TYPE_BUILD_HOUSE, HS.TASK_TYPE_BUILD_SAWMILL, HS.TASK_TYPE_BUILD_MINE, HS.TASK_TYPE_BUILD_FARM]
];

export class BuildingsBlock {
    element = document.querySelector('buildings-block');
    action_panel_button = document.querySelector('actions-panel action-button:nth-child('
        + BUILDINGS_BUTTON_POSITION + ')');
    buildings = this.element.querySelector('buildings');
    hide_button = document.querySelector('hide-buildings-block-button');
    build_overlay: BuildOverlay;
    building_select_$ = new Subject<number>();
    control$ = new Subject<IBuildingBoxEvent>();
    resources_subscriptions = [];
    private active_element: HTMLElement;

    constructor(private ui: UI) {
        this.setHooks();
        this.render_buildings();
    }

    render_buildings() {
        this.buildings.innerHTML = '';

        for (let task_type of task_tabs[0]) {
            this.buildings.appendChild( this.newBuilding(task_type) );
        }
    }

    toggle() {
        this.hide_button.classList.toggle('on');
        this.element.classList.toggle('hidden');

        this.action_panel_button.classList.toggle('active');
        this.resetBuilding();
    }

    setBuildingData(data: IBuildingBoxData) {
        this.resources_subscriptions.forEach(sub => sub.unsubscribe());
        this.resources_subscriptions.length = 0;

        forEachObject(data, res => {
            this.resources_subscriptions.push(res.changes_$
                .subscribe(() => {
                    const cost_cover = this.active_element.querySelector(`.res-${res.name} cost-cover-count`);
                    const cost_need = this.active_element.querySelector(`.res-${res.name} cost-count`);

                    cost_cover.innerHTML = res.have.toString();
                    cost_cover.classList.toggle('ok', res.need === res.have);
                    cost_need.innerHTML = res.need.toString();
                }));
        });
    }

    config({ok, back}: {ok?: boolean, back?: boolean}) {
        if (isDefined(ok)) {
            this.active_element.querySelector('ok-button').classList.toggle('disabled', !ok);
        }

        if (isDefined(back)) {
            this.active_element.querySelector('back-button').classList.toggle('disabled', !back);
        }
    }

    private newBuilding(task_type: number): HTMLElement {
        const element = document.createElement('building');
        const header = document.createElement('building-header');
        const icon = document.createElement('building-icon');
        const costs_block = document.createElement('costs-block');
        const ok_button = document.createElement('ok-button');
        const back_button = document.createElement('back-button');

        element.appendChild(header);
        element.appendChild(icon);
        element.appendChild(costs_block);
        ok_button.classList.add('disabled');
        element.appendChild(ok_button);
        element.appendChild(back_button);

        Observable.fromEvent<MouseEvent>(back_button, 'click')
            .subscribe(event => {
                event.stopPropagation();
                this.resetBuilding();
                this.sendEvent({type: BuildingBoxEventType.CLICK_BACK});

            });
        Observable.fromEvent<MouseEvent>(ok_button, 'click')
            .subscribe(event => {
                event.stopPropagation();
                this.sendEvent({type: BuildingBoxEventType.CLICK_OK});
            });

        icon.classList.add(task_images_names[task_type] + '-icon');

        Object.keys(resource_names).forEach(res => {
            if (typeof HS.TASK_RESOURCE_COST[task_type][res] !== 'undefined') {
                const cost_block = document.createElement('cost-block');
                const cost_icon = document.createElement('cost-icon');
                const cost_cover = document.createElement('cost-cover');
                const cost_cover_count = document.createElement('cost-cover-count');
                const cost_cover_delim = document.createElement('cost-cover-delim');
                const cost_count = document.createElement('cost-count');

                cost_block.classList.add('res-' + res);

                cost_cover_count.innerText = '0';
                cost_cover_delim.innerText = ' / ';
                cost_icon.classList.add(res + '-icon');
                cost_count.innerText = HS.TASK_RESOURCE_COST[task_type][res];
                cost_count.setAttribute('data-default', HS.TASK_RESOURCE_COST[task_type][res]);

                cost_cover.appendChild(cost_cover_count);
                cost_cover.appendChild(cost_cover_delim);

                cost_block.appendChild(cost_icon);
                cost_block.appendChild(cost_cover);
                cost_block.appendChild(cost_count);

                costs_block.appendChild(cost_block);
            }
        });

        header.innerHTML = task_names[task_type];

        Observable.fromEvent(element, 'click')
            .subscribe(() => {
                switch (task_types[task_type]) {
                    case TASK_TYPE.NORMAL:
                        game.add_building_task(0, task_type);
                        break;
                    case TASK_TYPE.GLOBAL_BUILD:
                        game.map.select_hex(undefined);
                        if (this.build_overlay) {
                            this.build_overlay.destroy();
                        }
                        this.build_overlay = new BuildOverlay(this.ui);
                        this.build_overlay.init_new(task_type);
                        break;
                }
                // TODO: добавить
                this.building_select_$.next(task_type);
            });

        this.building_select_$
            .subscribe(selected_building => {
                (element.querySelectorAll('cost-cover-count') as any)
                    .forEach(element => element.innerText = '0');
                element.classList.toggle('active', selected_building === task_type);

                if (selected_building === task_type) {
                    this.active_element = element;
                }
            });

        return element;
    }

    private resetBuilding() {
        this.building_select_$.next(0);

        if (this.build_overlay) {
            this.build_overlay.destroy();
            delete this.build_overlay;
        }

        if (!this.active_element) {
            return;
        }

        // reset resources costs
        (this.active_element.querySelectorAll('cost-count') as any).forEach(cost_count_element => {
            cost_count_element.innerText = cost_count_element.getAttribute('data-default');
        });
        this.active_element = null;
    }

    private sendEvent(event) {
        this.control$.next({
            ...event,
            state: {},
        });
    }

    private setHooks() {
        Observable.fromEvent(this.hide_button, 'click')
            .subscribe(() => {
                this.toggle();
            });
    }
}