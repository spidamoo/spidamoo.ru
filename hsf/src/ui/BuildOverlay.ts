import {HS} from '../game.const';
import {game, resource_harvesters, resource_map, building_image_names, building_origins,
    tile_image_shift, opposite_directions} from '../game';
import {getImage} from '../utils';
import {Subject} from 'rxjs/Subject';
import {
    IStoreBoxEvent, IStoreBoxState,
    StoreBoxEventType, StoreBoxBlock
} from './resourceTransferBlock/StoreBoxBlock';
import Shape = createjs.Shape;
import Bitmap = createjs.Bitmap;
import Container = createjs.Container;
import {UI} from '../ui';
import {
    BuildingBoxBlock, BuildingBoxEventType, IBuildingBoxData, IBuildingBoxEvent,
    ResourceInfoData
} from './resourcesInfo/BuildingBoxBlock';

// TODO: сделать два класса с наследованием для существующего задания и для нового
export class BuildOverlay {
    task_type: number;
    redraw_map: boolean = false;
    available_tiles_map: Object = {};
    selected_position = [];
    built_roads: Array<Array<Object>> = [];
    current_road: Array<string> = [];
    planned_roads_map: Object = {};

    ghost_bitmap: Bitmap;
    suggested_roads_shape: Shape = new Shape();
    built_roads_container = new Container();
    unclaimed_resources_container = new Container();
    store_boxes_shape = new Shape();

    destroy_$ = new Subject<void>();
    private store_boxes: Object = {};
    private resources: IBuildingBoxData;

    constructor(private ui: UI) {
    }

    // Общая инициализация
    init(task_type: number) {
        this.task_type = task_type;
        // Сделаем картинку чтобы обозначить будущее здание
        const task_result = (HS.BUILDING_TASK_RESULTS as {[name: string]: number})[this.task_type.toString()];
        const bitmap_src = 'img/buildings/map/' + building_image_names[task_result] + '_ghost.png';
        this.ghost_bitmap = new Bitmap( getImage(bitmap_src) );
        this.ghost_bitmap.visible = false;
        game.map.ui_container.addChild(this.ghost_bitmap);
        game.map.ui_container.addChild(this.built_roads_container);
        game.map.ui_container.addChild(this.unclaimed_resources_container);
        game.map.ui_container.addChild(this.suggested_roads_shape);
        game.map.ui_container.addChild(this.store_boxes_shape);
    }

    // Инициализация для интерфейса добавления нового задания
    init_new(task_type: number) {
        const self = this;

        // Находим все клетки на которых можно строить — то есть находящиеся на расстоянии не более
        // дня пути от какого-либо нашего поселения
        game.players_settlements.forEach(function(settlement) {
            const available_tiles_map_subset = game.map.graph.nodesInRange(settlement[0] + ';' + settlement[1], 12);
            Object.assign(self.available_tiles_map, available_tiles_map_subset);
        });

        // Перерисовываем карту, подсвечивая доступные клетки
        game.map.draw(this.available_tiles_map);
        this.redraw_map = true;

        // Расставляем ящики на поселениях со складами
        Object.keys(game.stored_resources_map).forEach( (hex) => {
            this.store_boxes[hex] = this.prepare_store_box(hex);
        });

        this.init(task_type);
        this.step1();
    }

    // Инициализация для интерфейса существующего задания
    init_existing(task_type: number, task_data) {
        this.init(task_type);
        this.select_build_place(task_data.position[0], task_data.position[1], task_data.position[2]);
        task_data.roads.forEach(road => {
            this.built_roads.push([]);
            road.forEach(step => {
                this.built_roads[this.built_roads.length - 1].push(step);
            });
        });
        this.draw_built_roads();
    }

    prepare_store_box(hex: string): StoreBoxBlock {
        const [x, y] = hex.split(';').map(function(s) {return parseInt(s)});
        const [hx, hy] = game.map.hex_to_position(x, y);

        this.store_boxes_shape.graphics.ss(5, 'square').s('rgba(246, 252, 85, 0.63)').f(null).dp(hx, hy, game.map.hex_R, 6, 0, 90);

        const store_box = this.ui.game_overlay.showStoreBox({
            x: hx,
            y: hy,
            resources: resource_map
                .reduce( (result, res) => {
                    result[res] = {name: res, count: game.stored_resources_map[hex][res], mod: 0};
                    return result;
                }, {} ),
        });

        store_box
            .takeUntil(this.destroy_$)
            .subscribe((event: IStoreBoxEvent) => {
                switch (event.type) {
                    case StoreBoxEventType.CLICK:
                        game.map.tile_callback(x, y, 0);
                        break;
                    case StoreBoxEventType.CLICK_BACK:
                        break;
                    case StoreBoxEventType.CLICK_MINUS:
                    case StoreBoxEventType.CLICK_PLUS:
                        this.allocate_resources(hex, event.state);
                        break;
                }
            });

        return store_box;
    }

    // Шаг 1 — выбор клетки, на которой мы хотим строить здание
    step1() {
        // Если это добывающее здание, то будем рисовать иконки сырых ресурсов, чтобы подсказать,
        // где имеет смысл строить
        for ( let res of resource_map ) {
            if (game.unclaimed_resources_map[res] === undefined) {
                continue;
            }
            if (HS.BUILDING_TASK_RESULTS[this.task_type] === resource_harvesters[res]) {
                console.log(res, resource_harvesters[res], game.unclaimed_resources_map);
                Object.keys(game.unclaimed_resources_map[res]).forEach( (node) => {
                    if (this.available_tiles_map[node]) {
                        const [x, y] = node.split(';').map(c => {return parseInt(c)});
                        const [hx, hy] = game.map.hex_to_position(x, y);
                        const unclaimed_resources_shape = new Shape();

                        unclaimed_resources_shape.graphics.ss(5, 'square')
                            .s('rgba(250, 131, 25, 0.81)').f(null)
                            .dp(hx, hy, game.map.hex_R, 6, 0, 90);
                        unclaimed_resources_shape.graphics.ss(1).s('black')
                            .f('black')
                            .dp(hx, hy, game.map.hex_R * 0.5, 6, 0, 90);

                        let tile_resource_bitmap = new createjs.Bitmap(getImage('img/ui/' + res + '-res.png'));
                        tile_resource_bitmap.x = hx - 9;
                        tile_resource_bitmap.y = hy - 19;

                        let tile_resource_text = new createjs.Text(
                            game.map.fields[y][x].raw[res].toString(),
                            '14px Verdana',
                            'white');
                        tile_resource_text.x = hx;
                        tile_resource_text.y = hy + 10;
                        tile_resource_text.textAlign    = 'center';
                        tile_resource_text.textBaseline = 'middle';

                        this.unclaimed_resources_container.addChild(unclaimed_resources_shape);
                        this.unclaimed_resources_container.addChild(tile_resource_bitmap);
                        this.unclaimed_resources_container.addChild(tile_resource_text);
                    }
                } );
            }
        }

        // Рисуем картинку будущего здания под мышкой, чтобы было понятно, сейчас надо выбрать место
        game.map.hover_callback = (x, y, q) => {
            this.move_ghost(x, y, q);
        };

        // По клику (если кликнули в зоне где можно строить) место выбрано
        game.map.tile_callback = (x, y, q) => {
            if (this.available_tiles_map[x + ';' + y]) {
                this.select_build_place(x, y, q);
                this.step2();
            }
            return true;
        }
    }

    // Шаг 2 — прокладываем дороги к поселениям со складами
    step2() {
        const [hx, hy] = game.map.hex_to_position(this.selected_position[0], this.selected_position[1]);

        this.draw_suggested_roads();

        this.unclaimed_resources_container.removeAllChildren();
        game.map.ui_container.removeChild(this.unclaimed_resources_container);

        // Рисуем ящик, на котором видно, сколько еще ресурсов надо выделить
        this.resources = resource_map
            .reduce((result, res) => {
                result[res] = new ResourceInfoData(res, HS.TASK_RESOURCE_COST[this.task_type][res], 0);
                return result;
            }, {});
        this.ui.buildings_block.setBuildingData(this.resources);

        this.ui.buildings_block
            .control$
            .takeUntil(this.destroy_$)
            .subscribe((event: IBuildingBoxEvent) => {
                switch (event.type) {
                    case BuildingBoxEventType.CLICK_OK:
                        this.confirm_build();
                        break;
                    case BuildingBoxEventType.CLICK_BACK:
                        this.destroy();
                        break;
                }
            });

        if (game.stored_resources_map[this.selected_position[0] + ';' + this.selected_position[1]]) {
            // Если строим в поселении со складом, активизируем его ящик
            this.store_boxes[this.selected_position[0] + ';' + this.selected_position[1]].set_disabled(false);
        }

        // Если водим мышкой по карте, значит хотим проложить еще дорогу, нарисуем предлагаемый маршрут
        game.map.hover_callback = (x, y) => {
            this.draw_suggested_roads([x, y]);
        };
        // Если кликнули по точке, добавим в план дорог
        game.map.tile_callback = (x, y) => {
            // Где-то здесь нужно сделать так, чтобы по клику на ящике склада вызывался этот же код,
            // и соответственно была возможность из эвента узнать координаты гекса
            return this.lay_road(x, y);
        };
    }

    move_ghost(x, y, q) {
        if (this.available_tiles_map[x + ';' + y]) {
            this.selected_position = [x, y, q];
            this.ghost_bitmap.visible = true;
            const [hx, hy] = game.map.hex_to_position(x, y);
            this.ghost_bitmap.x = hx + building_origins[q][0] - 20;
            this.ghost_bitmap.y = hy + building_origins[q][1] - 30;
            this.draw_suggested_roads();
        }
        else {
            this.ghost_bitmap.visible = false;
            this.selected_position = undefined;
            this.draw_suggested_roads();
        }
    }
    select_build_place(x, y, q) {
        this.selected_position = [x, y, q];
        // Закрепляем картинку будущего здания на выбранном гексе
        this.ghost_bitmap.visible = true;
        const [hx, hy] = game.map.hex_to_position(x, y);
        this.ghost_bitmap.x = hx + building_origins[this.selected_position[2]][0] - 20;
        this.ghost_bitmap.y = hy + building_origins[this.selected_position[2]][1] - 30;
    }
    // Добавляем в план дорог
    lay_road(x, y) {
        const hex = x + ';' + y;
        let current_hex = this.selected_position[0] + ';' + this.selected_position[1];
        if (this.current_road.length > 0) {
            current_hex = this.current_road[this.current_road.length - 1];
        }
        // Ищем маршрут от последней точки в промежуточном плане до выбранной
        const path = game.map.graph.shortestPath(
            current_hex,
            hex,
            12
        );
        if (!path[hex]) {
            return true;
        }
        // Добавляем найденый маршрут в промежуточный план
        let [cx, cy] = current_hex.split(';').map( s => parseInt(s) );
        path[hex].forEach( h => {
            this.current_road.push(h);
            const [nx, ny] = h.split(';').map( s => parseInt(s) );
            if ( !game.map.is_there_road(cx, cy, nx, ny) && !this.is_road_planned(cx + ';' + cy, h) ) {
                this.resources.gold.need += 2;
                this.set_road_planned(cx + ';' + cy, h);
            }

            [cx, cy] = [nx, ny];
        });
        // Здесь нужно увеличить количество требуемого золота в ящике "0/15" на path[hex] * 2
        this.draw_built_roads(); // Здесь мы перерисовываем все дороги. Возможно стоит оптимизировать?

        if (game.stored_resources_map[hex]) {
            // Если это поселение со складом, закрепляем эту дорогу в плане и обнуляем промежуточный план
            this.built_roads.push(this.current_road);
            this.current_road = [];
            // Здесь мы этот ящик со складом "активизируем", то есть теперь на нем можно
            // нажимать + и -
            this.store_boxes[hex].set_disabled(false);
        }
        return true;
    }
    is_road_planned(h1, h2) {
        if (this.planned_roads_map[h1 + '>' + h2] || this.planned_roads_map[h2 + '>' + h1]) {
            return true;
        }
        else {
            return false;
        }
    }
    set_road_planned(h1, h2) {
        this.planned_roads_map[h1 + '>' + h2] = true;
    }
    // Эта функция должна вызываться по событию от нажатия + и -
    allocate_resources(hex: string, state: IStoreBoxState) {
        const current_allocated = {};
        resource_map.forEach(res => {current_allocated[res] = 0});
        Object.keys(this.store_boxes).forEach(hex => {
            resource_map.forEach(res => {
                current_allocated[res] += this.store_boxes[hex].data.resources[res].mod;
            });
        });
        let ok_to_build = true;
        resource_map.forEach(res => {
            this.resources[res].have = current_allocated[res];
            ok_to_build =
                ok_to_build && this.resources[res].have == this.resources[res].need;
        });
        this.ui.buildings_block.config({ok: ok_to_build});
    }
    // Эта функция должна вызываться по событию от "OK"
    confirm_build() {
        const task_data = {
            'position': [ this.selected_position[0], this.selected_position[1], this.selected_position[2] ],
            'storages': {},
            'roads': [],
        };
        // Здесь должно быть итерирование по всем задействованным складам (то есть ящикам с + и -)
        this.built_roads.forEach(road => {
            task_data.roads.push([]);
            road.forEach(step => {
                task_data.roads[task_data.roads.length - 1].push(step);
            });
        });
        Object.keys(this.store_boxes).forEach(hex => {
            task_data.storages[hex] = {wood: 0, gold: 0, food: 0};
            resource_map.forEach(res => {
                task_data.storages[hex][res] = this.store_boxes[hex].data.resources[res].mod;
            });
        });
        console.log(this.store_boxes, task_data);
        game.add_building_task(0, this.task_type, task_data);
        this.destroy();
    }


    draw_suggested_roads(intermediate_position?: Array<Number>) {
        this.suggested_roads_shape.graphics.clear();
        if (!this.selected_position) {
            return;
        }

        this.suggested_roads_shape.graphics.s('rgba(237, 237, 236, 0.8)').ss(5, 'square').sd([15, 13]);

        let current_position = this.selected_position;
        if (this.current_road.length > 0) {
            current_position = this.current_road[this.current_road.length - 1].split(';');
        }

        if (intermediate_position) {
            const path = game.map.graph.shortestPath(
                current_position[0] + ';' + current_position[1],
                intermediate_position[0] + ';' + intermediate_position[1],
                12
            );
            if (!path[intermediate_position[0] + ';' + intermediate_position[1]]) {
                return;
            }

            const [hx, hy] = game.map.hex_to_position(current_position[0], current_position[1]);
            this.suggested_roads_shape.graphics.mt(hx, hy);
            path[ intermediate_position[0] + ';' + intermediate_position[1] ].forEach( (hex) => {
                const [x, y] = hex.split(';');
                const [hx, hy] = game.map.hex_to_position( parseInt(x), parseInt(y) );

                this.suggested_roads_shape.graphics.lt(hx, hy);
            } );
            
            current_position = intermediate_position;
        }

        const paths_to_settlements = game.map.graph.shortestPath(
            current_position[0] + ';' + current_position[1],
            Object.keys(game.stored_resources_map),
            12
        );
        Object.keys(paths_to_settlements).forEach( (settlement) => {
            const [hx, hy] = game.map.hex_to_position(current_position[0], current_position[1]);
            this.suggested_roads_shape.graphics.mt(hx, hy);
            paths_to_settlements[settlement].forEach( (hex) => {
                const [x, y] = hex.split(';');
                const [hx, hy] = game.map.hex_to_position( parseInt(x), parseInt(y) );

                this.suggested_roads_shape.graphics.lt(hx, hy);
            } );
        });
    }

    draw_built_roads() {
        this.built_roads_container.removeAllChildren();
        this.built_roads.forEach(road => {
            this.draw_road(road);
        });
        this.draw_road(this.current_road);
        game.stage.update();
    }
    draw_road(road) {
        let prev_pos = this.selected_position;

        for (let i = 0; i < road.length; i++) {
            let [hx, hy] = game.map.hex_to_position(prev_pos[0], prev_pos[1]);
            const pos = road[i].split(';').map( c => parseInt(c) );
            const direction = game.determine_direction(prev_pos[0], prev_pos[1], pos[0], pos[1]);
            let road_bitmap = new Bitmap( getImage('img/tiles/road/road_' + direction + '.png') );
            this.built_roads_container.addChild(road_bitmap);
            road_bitmap.x = hx - tile_image_shift[0];
            road_bitmap.y = hy - tile_image_shift[1];

            [hx, hy] = game.map.hex_to_position(pos[0], pos[1]);
            road_bitmap = new Bitmap( getImage('img/tiles/road/road_' + opposite_directions[direction] + '.png') );
            this.built_roads_container.addChild(road_bitmap);
            road_bitmap.x = hx - tile_image_shift[0];
            road_bitmap.y = hy - tile_image_shift[1];

            prev_pos = pos;
        }
    }

    destroy() {
        this.destroy_$.next();

        game.map.ui_container.removeChild(this.ghost_bitmap);
        game.map.ui_container.removeChild(this.built_roads_container);
        game.map.ui_container.removeChild(this.unclaimed_resources_container);
        game.map.ui_container.removeChild(this.suggested_roads_shape);
        game.map.ui_container.removeChild(this.store_boxes_shape);

        Object.keys(this.store_boxes).forEach(hex => {
            this.store_boxes[hex].destroy();
            delete this.store_boxes[hex];
        });
        game.map.hover_callback = undefined;
        game.map.tile_callback = undefined;

        if (this.redraw_map) {
            game.map.draw();
        }
        else {
            game.stage.update();
        }
    }
}