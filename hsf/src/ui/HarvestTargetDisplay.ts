import {HS} from '../game.const';
import {game, resource_from_harvesters} from '../game';
import {getImage} from '../utils';
import {Subject} from 'rxjs/Subject';
import Shape = createjs.Shape;
import Bitmap = createjs.Bitmap;
import Container = createjs.Container;

// TODO: сделать два класса с наследованием для существующего задания и для нового
export class HarvestTargetDisplay {
    destroy_$ = new Subject<void>();
    shape: Shape = new Shape();

    constructor(x, y, q) {
        const building = game.map.building_at(x, y, q);
        if (!building || !resource_from_harvesters[building.building_type]) {
            return;
        }
        const target_hex = building.building_data;
        let t_x = x;
        let t_y = y;

        game.map.ui_container.addChild(this.shape);

        let [hx, hy] = game.map.hex_to_position(x, y);
        if (target_hex) {
            const path = game.map.graph.shortestPath(
                x + ';' + y,
                target_hex
            );

            this.shape.graphics.s('rgba(237, 237, 236, 0.8)').ss(5, 'square').sd([15, 13]).mt(hx, hy);
            path[target_hex].forEach( (hex) => {
                const [x, y] = hex.split(';').map(v => {return parseInt(v)});
                [hx, hy] = game.map.hex_to_position(x, y);

                this.shape.graphics.lt(hx, hy);
            } );
        }

        this.shape.graphics.ss(5, 'square').sd(null)
            .s('rgba(250, 131, 25, 0.81)').f(null)
            .dp(hx, hy, game.map.hex_R, 6, 0, 90);
    }

    destroy() {
        this.destroy_$.next();
        game.map.ui_container.removeChild(this.shape);
    }
}