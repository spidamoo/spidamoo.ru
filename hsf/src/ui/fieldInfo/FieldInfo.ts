import {forEachObject} from '../../utils';
import {building_image_names, HARVEST_COUNT, resource_from_harvesters, unit_images_names} from '../../game';
import {ITile} from '../../types/game.types';

const DISPLAY_CLASS_NAME = 'show';

export class FieldInfo {
    element = document.querySelector('field-info');
    resources_list = this.element.querySelector('resources-list');
    buildings_list = this.element.querySelector('buildings-list');
    armies_list = this.element.querySelector('armies-list');

    setField(field_data: ITile) {
        const resources_table = document.createElement('table');
        const table_head = document.createElement('tr');
        const head_icon = document.createElement('th');
        const head_raw = document.createElement('th');
        const head_storage = document.createElement('th');

        head_raw.innerText = 'Сырье';
        head_storage.innerText = 'Склад';
        table_head.appendChild(head_icon);
        table_head.appendChild(head_raw);
        table_head.appendChild(head_storage);
        resources_table.appendChild(table_head);

        if (field_data.raw) {
            this.show_resources(field_data, resources_table);
        }

        this.resources_list.innerHTML = '';
        this.resources_list.appendChild(resources_table);

        // Buildings list
        this.buildings_list.innerHTML = '';

        if (field_data.buildings) {
            this.show_buildings(field_data);
        }

        // Armies list
        this.armies_list.innerHTML = '';

        if (field_data.armies) {
            this.show_armies(field_data);
        }
    }

    show_armies(field_data: ITile) {
        forEachObject(field_data.armies, (army) => {
            const army_item = document.createElement('army');
            const packs = {};

            army.units.forEach(unit => {
                if (packs[unit.unit_type]) {
                    packs[unit.unit_type].push(unit);
                } else {
                    packs[unit.unit_type] = [unit];
                }
            });

            Object.keys(packs).sort((a, b) => +a - +b).forEach(key => {
                const pack_element = document.createElement('pack');

                pack_element.classList.add('pack');
                pack_element.classList.add(unit_images_names[key] + '-icon');
                pack_element.innerText = packs[key].length;

                army_item.appendChild(pack_element);
            });

            this.armies_list.appendChild(army_item);
        });
    }

    show_buildings(field_data: ITile) {
        forEachObject(field_data.buildings, (building) => {
            const building_item = document.createElement('building');
            const building_icon = document.createElement('icon');
            const icon = document.createElement('icon');
            const info = document.createElement('info');
            const res = resource_from_harvesters[building.building_type];

            building_icon.classList.add(building_image_names[building.building_type] + '-icon');
            icon.classList.add(res + '-icon');

            if (typeof field_data.raw_mod[res] !== 'undefined') {
                const to_harvest = Math.min(HARVEST_COUNT, field_data.raw_mod[res]);

                if (to_harvest > 0) {
                    info.innerText = '+' + to_harvest.toString();

                    if (building.building_data) {
                        info.classList.add('export');
                    }
                } else if (to_harvest === 0) {
                    info.innerText = 'простой';
                    info.classList.add('down');
                } else {
                    info.innerText = to_harvest.toString();
                    info.classList.add('down');
                }
            }

            building_item.appendChild(building_icon);
            building_item.appendChild(icon);
            building_item.appendChild(info);

            this.buildings_list.appendChild(building_item);
        });
    }

    show_resources(field_data: ITile, resources_table: HTMLElement) {
        forEachObject(field_data.raw, (raw, res) => {
            const resource_line = document.createElement('tr');
            const icon = document.createElement('icon');

            const raw_count = document.createElement('count');
            const raw_grow = document.createElement('grow');
            const storage_count = document.createElement('count');
            const storage_grow = document.createElement('grow');

            icon.classList.add(res + '-icon');
            raw_count.innerText = raw.toString();
            storage_count.innerText = field_data.stored[res].toString();

            if (field_data.raw_mod) {
                if (field_data.raw_mod[res] < 0) {
                    raw_grow.innerText = '-' + Math.abs(field_data.raw_mod[res]).toString();
                } else if (field_data.raw_mod[res] > 0) {
                    raw_grow.classList.add('down');
                    raw_grow.innerText = '-' + field_data.raw_mod[res].toString();
                }
            }

            if (field_data.stored_mod) {
                if (field_data.stored_mod[res] < 0) {
                    storage_grow.classList.add('down');
                    storage_grow.innerText = field_data.stored_mod[res].toString();
                } else if (field_data.stored_mod[res] > 0) {
                    storage_grow.innerText = '+' + field_data.stored_mod[res].toString();
                } else {
                    storage_grow.innerText = '+0';
                }
            }

            resource_line.appendChild(this.tdWrap([icon]));
            resource_line.appendChild(this.tdWrap([raw_count, raw_grow]));
            resource_line.appendChild(this.tdWrap([storage_count, storage_grow]));

            resources_table.appendChild(resource_line);
        });
    }

    toggle(toggle: boolean) {
        if (typeof toggle === 'undefined') {
            this.element.classList.toggle(DISPLAY_CLASS_NAME, this.element.classList.contains(DISPLAY_CLASS_NAME));
        } else {
            this.element.classList.toggle(DISPLAY_CLASS_NAME, toggle);
        }
    }

    tdWrap(elements: HTMLElement[]) {
        const blackBox = document.createElement('td');

        elements.forEach(element => blackBox.appendChild(element));

        return blackBox;
    }
}