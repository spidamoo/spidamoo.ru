import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/observable/combineLatest';
import {Observable} from 'rxjs/Observable';
import {ResourceInfoBlock} from './ResourceInfoBlock';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {resource_map} from '../../game';
import {isDefined} from '../../utils';

export enum BuildingBoxEventType {
    CLICK_BACK,
    CLICK_OK,
}

export interface IBuildingBoxEvent {
    type: BuildingBoxEventType;
    state: IBuildingBoxState;
}

export interface IBuildingBoxState {
    resources: ResourceInfoData[];
}

export class ResourceInfoData {
    changes_$: BehaviorSubject<void>;

    constructor(public name: string, private _need: number, private _have: number) {
        this.changes_$ = new BehaviorSubject(null);
    }

    set have(have: number) {
        this._have = have;
        this.changes_$.next(null);
    }

    get have(): number {
        return this._have;
    }

    set need(have: number) {
        this._need = have;
        this.changes_$.next(null);
    }

    get need(): number {
        return this._need;
    }
}

export interface IBuildingBoxData {
    [name: string]: ResourceInfoData;
}

export class BuildingBoxBlock extends Subject<IBuildingBoxEvent> {

    element: HTMLElement;

    private destroy_$ = new Subject<void>();
    private resource_info_blocks: ResourceInfoBlock[] = [];
    private disabled: boolean;
    private back_button: HTMLElement;
    private ok_button: HTMLElement;

    constructor(public data: IBuildingBoxData) {
        super();

        this.element = document.createElement('resource-transfer');

        // this.createBoxImg();
        this.element.innerHTML = '<svg height="164" width="142" class="box-img">\n' +
            '  <path d="M72 0 L142 40 L142 40 L142 123 L70 164 L0 123 L0 40 Z" fill="transparent" />\n' +
            '</svg>';
        this.fillResourceTransferBlock();
        this.createControlButtons();
        this.config({ok: false, back: true});
    }

    destroy() {
        this.destroy_$.next();
        this.element.remove();
        this.resource_info_blocks.forEach(block => {
            block.destroy();
        });
    }

    update(resources: {[name: string]: ResourceInfoData}) {
        resource_map.forEach((res) => {
            this.data.resources[res].have = resources[res].have;
            this.data.resources[res].need = resources[res].need;
        });
    }

    config({ok, back}: {ok?: boolean, back?: boolean}) {
        if (isDefined(ok)) {
            this.ok_button.classList.toggle('disabled', !ok);
        }

        if (isDefined(back)) {
            this.back_button.classList.toggle('disabled', !back);
        }
    }

    private fillResourceTransferBlock() {
        const element = document.createElement('counters-block');

        resource_map.forEach((res) => {
            const counterBox = new ResourceInfoBlock(this.data.resources[res], this.destroy_$);

            element.appendChild(counterBox.element);

            this.resource_info_blocks.push(counterBox);
        });

        this.element.appendChild(element);
    }

    private createControlButtons() {
        const changeControls = document.createElement('change-control');
        this.back_button = document.createElement('back-button');
        this.ok_button = document.createElement('ok-button');

        this.element.appendChild(changeControls);
        changeControls.appendChild(this.back_button);
        changeControls.appendChild(this.ok_button);

        Observable.fromEvent(this.back_button, 'click')
            .takeUntil(this.destroy_$)
            .filter(() => !this.disabled)
            .subscribe(() => {
                this.sendEvent({type: BuildingBoxEventType.CLICK_BACK});
            });

        Observable.fromEvent(this.ok_button, 'click')
            .takeUntil(this.destroy_$)
            .filter(() => !this.disabled)
            .subscribe(() => {
                this.sendEvent({type: BuildingBoxEventType.CLICK_OK});
            });
    }

    private sendEvent(event) {
        this.next({
            ...event,
            state: {
                resources: this.data.resources
            },
        });
    }
}