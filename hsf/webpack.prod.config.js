const config = require('./webpack.config');
const webpackMerge = require('webpack-merge');
const UglifyJsParallelPlugin = require('webpack-uglify-parallel');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const jsDest = __dirname + "/../public/hs/javascript";

module.exports = webpackMerge(config, {
    output: {
        path: jsDest,
        filename: 'game.bundle.js'
    },
    plugins: [
        new UglifyJsParallelPlugin({
            beautify: false,
            output: {
                comments: false
            },
            compress: {
                warnings: false,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                if_return: true,
                join_vars: true,
                negate_iife: false
            },
            sourceMap: false
        }),
        new CopyWebpackPlugin([ {from: './node_modules/easeljs/lib/easeljs.min.js', to: jsDest} ], {})
    ]
});
