const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const jsDest = __dirname + "/../public/hs/javascript";

module.exports = {
    context: __dirname,
    devtool: "source-map",
    entry: "./src/game.ts",
    output: {
        path: jsDest,
        filename: "game.js"
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    module:{
        rules: [
            {test: /\.js$/, loader: 'strict-loader'},
            {test : /\.ts$/, loader: 'awesome-typescript-loader'},
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'postcss-loader',
                    'less-loader'
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            hash: true,
            template: 'src/play.html',
            filename: '../play.html'
        }),
        new CopyWebpackPlugin([ {from: './node_modules/easeljs/lib/easeljs.min.js', to: jsDest} ], {})
    ],
    watchOptions: {
        ignored: /node_modules/
    }
};