package HexStrat;
# TODO: naming

use strict;
use warnings;
use POSIX;
use DBI;
require DBD::SQLite;
use Crypt::PBKDF2;
use Math::Random::Secure qw(rand);
use List::Util qw(min);
use XML::Simple qw(:strict);
use JSON::MaybeXS;
use Time::HiRes qw(time);
use LWP::UserAgent ();

use Data::Dumper;

use constant {
    BUILDING_TYPE_GLOBAL         => 0,

    BUILDING_TYPE_SAWMILL        => 1,
    BUILDING_TYPE_HOUSE          => 2,

    BUILDING_TYPE_MINE           => 4,
    BUILDING_TYPE_FARM           => 5,
    BUILDING_TYPE_BARRACKS       => 6,
    BUILDING_TYPE_SHOOTING_RANGE => 7,
    BUILDING_TYPE_STADIUM        => 8,

    TASK_TYPE_HARVEST_WOOD         => 1,
    TASK_TYPE_HARVEST_GOLD         => 2,
    TASK_TYPE_HARVEST_FOOD         => 3,

    TASK_TYPE_DIRECT_RESOURCE      => 4,
    TASK_TYPE_TRANSPORT_RESOURCE   => 5,
    TASK_TYPE_IDLE                 => 6,

    TASK_TYPE_BUILD_ROAD           => 9,
    TASK_TYPE_BUILD_HOUSE          => 10,

    TASK_TYPE_BUILD_SAWMILL        => 11,
    TASK_TYPE_BUILD_MINE           => 13,
    TASK_TYPE_BUILD_FARM           => 14,
    TASK_TYPE_BUILD_BARRACKS       => 15,
    TASK_TYPE_BUILD_SHOOTING_RANGE => 16,
    TASK_TYPE_BUILD_STADIUM        => 17,

    TASK_TYPE_HIRE_INFANTRY        => 21,
    TASK_TYPE_HIRE_ARCHER          => 22,
    TASK_TYPE_HIRE_CAVALRY         => 23,
    TASK_TYPE_DESTROY_BUILDING     => 24,

    TASK_TYPE_JOIN_ARMY            => 25,

    ARMY_TASK_TYPE_MOVE      => 1,

    ARMY_TASK_TYPE_INTERCEPT => 3,


    UNIT_TYPE_INFANTRY      => 3,
    UNIT_TYPE_ARCHER        => 4,
    UNIT_TYPE_CAVALRY       => 5,

    DIRECTIONS              => [
        [ # смещения по сетке для четных рядов
            [1, 0],   # вправо
            [0, 1],   # вправо вниз
            [-1, 1],  # влево вниз
            [-1, 0],  # влево
            [-1, -1], # влево вверх
            [0, -1],  # вправо вверх
        ],
        [ # смещения по сетке для нечетных рядов
            [1, 0],  # вправо
            [1, 1],  # вправо вниз
            [0, 1],  # влево вниз
            [-1, 0], # влево
            [0, -1], # влево вверх
            [1, -1], # вправо вверх
        ]
    ],
    OPPOSITE_DIRECTIONS     => [3, 4, 5, 0, 1, 2],
    TILE_TYPE_PLAIN         => 1,
    TILE_TYPE_WOODS         => 2,
    TILE_TYPE_HILLS         => 3,
    TILE_TYPE_MOUNTAINS     => 4,
    TILE_TYPE_WATER         => 5,
    TILE_TYPE_ROAD          => 6, # не настоящий тип тайла
    TILE_TYPE_SETTLEMENT    => 7, # не настоящий тип тайла

    DEFAULT_COLORS          => ['gray', 'red', 'blue', 'green', 'yellow', 'orange', 'purple', 'teal', 'black', 'white'],

    MAP_OBJECTS             => {
        1 => 'gold_deposit',
        2 => 'wood_deposit',
        3 => 'food_deposit',
    },

    LOG_TYPE_ARMY_MOVE        => 1,
    LOG_TYPE_ARMY_FIGHT       => 2,
    LOG_TYPE_ARMY_FORMATION   => 3,
    LOG_TYPE_HIRE_UNIT        => 4,
    LOG_TYPE_BUILD_BUILDING   => 5,
    LOG_TYPE_HARVEST_RESOURCE => 6,
    LOG_TYPE_LOAD_RESOURCE    => 7,
    LOG_TYPE_UNLOAD_RESOURCE  => 8,
    LOG_TYPE_FAIL             => 9,
    LOG_TYPE_ARMY_INTERCEPT   => 10,
};

use constant {
    BUILDING_TYPE_NAMES => {
        BUILDING_TYPE_SAWMILL()        => 'sawmill',
        BUILDING_TYPE_HOUSE()          => 'house',
        BUILDING_TYPE_MINE()           => 'mine',
        BUILDING_TYPE_FARM()           => 'farm',
        BUILDING_TYPE_BARRACKS()       => 'barracks',
        BUILDING_TYPE_SHOOTING_RANGE() => 'shooting_range',
        BUILDING_TYPE_STADIUM()        => 'stadium',
    },
    BUILDING_SPACES => {
        BUILDING_TYPE_SAWMILL()        => '1x1',
        BUILDING_TYPE_HOUSE()          => '1x1',
        BUILDING_TYPE_MINE()           => '1x1',
        BUILDING_TYPE_FARM()           => '1x1',
        BUILDING_TYPE_BARRACKS()       => '2x1',
        BUILDING_TYPE_SHOOTING_RANGE() => '2x1',
        BUILDING_TYPE_STADIUM()        => '2x1',
    },
    BUILDING_TASK_RESULTS => {
        TASK_TYPE_BUILD_HOUSE()          => BUILDING_TYPE_HOUSE(),
        TASK_TYPE_BUILD_SAWMILL()        => BUILDING_TYPE_SAWMILL(),
        TASK_TYPE_BUILD_MINE()           => BUILDING_TYPE_MINE(),
        TASK_TYPE_BUILD_FARM()           => BUILDING_TYPE_FARM(),
        TASK_TYPE_BUILD_BARRACKS()       => BUILDING_TYPE_BARRACKS(),
        TASK_TYPE_BUILD_SHOOTING_RANGE() => BUILDING_TYPE_SHOOTING_RANGE(),
        TASK_TYPE_BUILD_STADIUM()        => BUILDING_TYPE_STADIUM(),
    },
    AVAILABLE_TASKS => {
        building => {
            BUILDING_TYPE_GLOBAL() => {
                # TASK_TYPE_BUILD_ROAD()           => 1,
                TASK_TYPE_BUILD_HOUSE()          => 1,
                TASK_TYPE_BUILD_SAWMILL()        => 1,
                TASK_TYPE_BUILD_MINE()           => 1,
                TASK_TYPE_BUILD_FARM()           => 1,
                TASK_TYPE_BUILD_BARRACKS()       => 1,
                TASK_TYPE_BUILD_SHOOTING_RANGE() => 1,
                TASK_TYPE_BUILD_STADIUM()        => 1,
                TASK_TYPE_TRANSPORT_RESOURCE()   => 1,
            },
            BUILDING_TYPE_HOUSE() => {
            },
            BUILDING_TYPE_SAWMILL() => {
                TASK_TYPE_HARVEST_WOOD()    => 1,
                TASK_TYPE_DIRECT_RESOURCE() => 1,
                TASK_TYPE_IDLE()            => 1,
            },
            BUILDING_TYPE_MINE() => {
                TASK_TYPE_HARVEST_GOLD()    => 1,
                TASK_TYPE_DIRECT_RESOURCE() => 1,
                TASK_TYPE_IDLE()            => 1,
            },
            BUILDING_TYPE_FARM() => {
                TASK_TYPE_HARVEST_FOOD()    => 1,
                TASK_TYPE_DIRECT_RESOURCE() => 1,
                TASK_TYPE_IDLE()            => 1,
            },
            BUILDING_TYPE_BARRACKS() => {
                TASK_TYPE_HIRE_INFANTRY()   => 1,
            },
            BUILDING_TYPE_SHOOTING_RANGE() => {
                TASK_TYPE_HIRE_ARCHER()     => 1,
            },
            BUILDING_TYPE_STADIUM() => {
                TASK_TYPE_HIRE_CAVALRY()    => 1,
            },
        },
        unit => {
            UNIT_TYPE_INFANTRY() => {
                TASK_TYPE_JOIN_ARMY()       => 1,
            },
            UNIT_TYPE_ARCHER() => {
                TASK_TYPE_JOIN_ARMY()       => 1,
            },
            UNIT_TYPE_CAVALRY() => {
                TASK_TYPE_JOIN_ARMY()       => 1,
            },
        },
    },
    TASK_AP_COSTS => {
        TASK_TYPE_HARVEST_WOOD()         => {
            default => 12,
        },
        TASK_TYPE_HARVEST_GOLD()         => {
            default => 12,
        },
        TASK_TYPE_HARVEST_FOOD()         => {
            default => 12,
        },
        TASK_TYPE_HIRE_INFANTRY()        => {
            default => 12,
        },
        TASK_TYPE_HIRE_ARCHER()          => {
            default => 12,
        },
        TASK_TYPE_HIRE_CAVALRY()         => {
            default => 12,
        },
        TASK_TYPE_BUILD_ROAD()           => {
            default => 1,
        },
        TASK_TYPE_BUILD_SAWMILL()        => {
            default => 12,
        },
        TASK_TYPE_BUILD_HOUSE()          => {
            default => 12,
        },
        TASK_TYPE_BUILD_MINE()           => {
            default => 12,
        },
        TASK_TYPE_BUILD_FARM()           => {
            default => 12,
        },
        TASK_TYPE_BUILD_BARRACKS()       => {
            default => 12,
        },
        TASK_TYPE_BUILD_SHOOTING_RANGE() => {
            default => 12,
        },
        TASK_TYPE_BUILD_STADIUM()        => {
            default => 12,
        },
        TASK_TYPE_DESTROY_BUILDING()     => {
            default => 12,
        },
        TASK_TYPE_JOIN_ARMY()            => {
            default => 6,
        }
    },
    TASK_RESOURCE_COST => {
        TASK_TYPE_HIRE_INFANTRY() => {
            wood => 0,
            gold => 15,
            food => 15,
        },
        TASK_TYPE_HIRE_ARCHER() => {
            wood => 0,
            gold => 15,
            food => 15,
        },
        TASK_TYPE_HIRE_CAVALRY() => {
            wood => 0,
            gold => 15,
            food => 15,
        },
        TASK_TYPE_BUILD_ROAD() => {
            gold => 1,
        },
        TASK_TYPE_BUILD_HOUSE() => {
            wood => 10,
            food => 10,
            gold => 0,
        },
        TASK_TYPE_BUILD_SAWMILL() => {
            wood => 20,
            food => 20,
            gold => 0,
        },
        TASK_TYPE_BUILD_MINE() => {
            wood => 20,
            food => 20,
            gold => 0,
        },
        TASK_TYPE_BUILD_FARM() => {
            wood => 20,
            food => 20,
            gold => 0,
        },
        TASK_TYPE_BUILD_BARRACKS() => {
            wood => 30,
            food => 0,
            gold => 30,
        },
        TASK_TYPE_BUILD_SHOOTING_RANGE() => {
            wood => 30,
            food => 0,
            gold => 30,
        },
        TASK_TYPE_BUILD_STADIUM() => {
            wood => 30,
            food => 0,
            gold => 30,
        },
    },
    DEFAULT_TASKS => {
        BUILDING_TYPE_SAWMILL() => TASK_TYPE_HARVEST_WOOD(),
        BUILDING_TYPE_MINE()    => TASK_TYPE_HARVEST_GOLD(),
        BUILDING_TYPE_FARM()    => TASK_TYPE_HARVEST_FOOD(),
    },
    AUTOCOPY_TASKS => {
        TASK_TYPE_HARVEST_WOOD() => 1,
        TASK_TYPE_HARVEST_GOLD() => 1,
        TASK_TYPE_HARVEST_FOOD() => 1,
    },
    FAILED_TASK_REPLACEMENT => {
        TASK_TYPE_HARVEST_WOOD() => TASK_TYPE_IDLE(),
        TASK_TYPE_HARVEST_GOLD() => TASK_TYPE_IDLE(),
        TASK_TYPE_HARVEST_FOOD() => TASK_TYPE_IDLE(),
    },
    ARMY_TASK_AP_COSTS => {
        ARMY_TASK_TYPE_MOVE()      => 0,
        ARMY_TASK_TYPE_INTERCEPT() => 0,
    },
    TILE_MOVE_HALF_COST => {
        0                      => 13,
        TILE_TYPE_PLAIN()      => 1,
        TILE_TYPE_WOODS()      => 1.5,
        TILE_TYPE_HILLS()      => 2,
        TILE_TYPE_MOUNTAINS()  => 13,
        TILE_TYPE_WATER()      => 13,
        TILE_TYPE_ROAD()       => 0.5,
        TILE_TYPE_SETTLEMENT() => 1.5,
    },
    TILE_NAMES          => {
        TILE_TYPE_PLAIN()      => 'plain',
        TILE_TYPE_WOODS()      => 'forest',
        TILE_TYPE_HILLS()      => 'hill',
        TILE_TYPE_MOUNTAINS()  => 'mountain',
        TILE_TYPE_WATER()      => 'water',
    },
    UNIT_ATTACK_ORDER => {
        UNIT_TYPE_INFANTRY() => [
            {type => UNIT_TYPE_CAVALRY(),  damage => 1},
            {type => UNIT_TYPE_INFANTRY(), damage => 0.5},
            {type => UNIT_TYPE_ARCHER(),   damage => 0.25},
        ],
        UNIT_TYPE_ARCHER() => [
            {type => UNIT_TYPE_INFANTRY(), damage => 1},
            {type => UNIT_TYPE_ARCHER(),   damage => 0.5},
            {type => UNIT_TYPE_CAVALRY(),  damage => 0.25},
        ],
        UNIT_TYPE_CAVALRY() => [
            {type => UNIT_TYPE_ARCHER(),   damage => 1},
            {type => UNIT_TYPE_CAVALRY(),  damage => 0.5},
            {type => UNIT_TYPE_INFANTRY(), damage => 0.25},
        ],
    },
};

sub new {
    my ($class, %params) = @_;

    my $ua = LWP::UserAgent->new();
    $ua->agent('Mozilla/5.0');
    return bless {
        _data_path    => $params{data_path} // '',
        debug_enabled => $params{debug},
        _ua           => $ua,
    }, $class;
}

sub _get_debug {
    my ($self) = @_;
    unless ($self->{_debug}) {
        require HexStrat::Debug;
        HexStrat::Debug->import();
        $self->{_debug} = new HexStrat::Debug;
    }
    return $self->{_debug};
}
# TODO: запихнуть в HexStrat::Debug и грамотный вывод
sub _debug_message {
    my $self = shift @_;
    return unless $self->{debug_enabled};

    my $now = time();
    my $dt = $self->{_last_message_time} ? $now - $self->{_last_message_time} : 0 ;
    $self->{_last_message_time} = $now;

    print STDERR sprintf('+%8.2f ', $dt * 1000), Dumper(@_);
}

sub _get_db_name {
    my ($self, $dbname, %opts) = @_;

    $dbname //= 'game';
    if ($dbname eq 'game') {
        my $campaign_id = $opts{campaign_id} // $self->{_campaign} && $self->{_campaign}{id};
        die 'no campaign provided' unless $campaign_id;
        $dbname .= ".$campaign_id";
    }

    return $dbname;
}
sub _get_db_filename {
    my ($self, $dbname, %opts) = @_;
    $dbname = $self->_get_db_name(undef, %opts) unless defined $dbname;
    return $self->{_data_path} . "db/$dbname.db";
}
sub _get_dbh {
    my ($self, $dbname, %opts) = @_;
    $dbname = $self->_get_db_name($dbname, %opts);

    unless ($self->{_dbh}{$dbname}) {
        my $dbfile = $self->_get_db_filename($dbname);

        $self->{_dbh}{$dbname} = DBI->connect("dbi:SQLite:dbname=$dbfile", "", "", {
            PrintError       => 0,
            RaiseError       => 1,
            AutoCommit       => 1,
            FetchHashKeyName => 'NAME_lc',
        }) or die $DBI::errstr;
    }
    return $self->{_dbh}{$dbname};
}

sub _get_pbkdf2 {
    my ($self) = @_;
    $self->{_pbkdf2} = Crypt::PBKDF2->new(
        hash_class => 'HMACSHA2',
        hash_args => {
            sha_size => '512',
        },
        salt_len => '32',
    ) unless $self->{_pbkdf2};
    return $self->{_pbkdf2};
}
sub password_hash {
    my ($self, $password, $salt) = @_;
    $salt = $self->generate_salt() unless $salt; # можно полагаться на соль генерируемую pbkdf2 но там вроде как используется стандартный rand

    return $self->_get_pbkdf2()->generate($password, $salt);
}
sub generate_salt {
    my ($self, $length) = @_;
    $length //= 32;
    return join( '', map {('0' .. '9', 'a' .. 'z', 'A' .. 'Z')[int( rand(62) )]} (1 .. $length) );
}
sub check_password {
    my ($self, $plaintext, $hash) = @_;

    my $validation_result = eval {$self->_get_pbkdf2()->validate($hash, $plaintext)};
    if ($@) {
        # print STDERR $@, "\n";
        die $@;
    }
    return $validation_result;
}
sub register_user {
    my ($self, %params) = @_;

    return 0 unless $params{user} && $params{pass};
    return 0 if $params{user} =~ /\W/;
    return 0 if $self->load_user($params{user});

    my $hash = $self->password_hash($params{pass});

    $self->_get_dbh('general')->do(
        "INSERT INTO users(username, password) VALUES(?, ?)", 
        undef,
        $params{user}, $hash
    );
    return {
        username => $params{user},
    };
}
sub authenticate_user {
    my ($self, $username, $password) = @_;
    # print STDERR 'authenticate_user ' . Dumper(\@_);

    my $user = $self->_get_dbh('general')->selectrow_hashref(
        "SELECT * FROM users WHERE username=?",
        undef,
        $username
    );
    # print STDERR 'user ' . Dumper($user);
    return 0 unless $user;
    return 1 if $self->check_password($password, $user->{password});
    # print STDERR "wrong password\n";
    return 0;
}
sub load_user {
    my ($self, $username) = @_;
    $username = $username->{username} if ref $username; # если у нас уже объект, перезагрузим его на всякий случай

    return $self->_get_dbh('general')->selectrow_hashref(
        "SELECT * FROM users WHERE username=?",
        undef,
        $username
    );
}
sub set_user {
    my ($self, $username) = @_;

    $self->{_user} = $self->load_user($username);
}

sub _fill_campaign_details {
    my ($self, $campaign) = @_;

    $campaign->{users}      = $self->get_users(campaign => $campaign);
    $campaign->{is_started} = $self->is_campaign_started($campaign);
    if ($campaign->{is_started}) {
        $campaign->{info} = $self->_get_game_info(campaign_id => $campaign->{id});
        $campaign->{players} = $self->get_players(campaign_id => $campaign->{id});
    }
    if ($campaign->{is_started} || !$campaign->{max_players} || scalar(@{ $campaign->{users} }) > $campaign->{max_players}) {
        $campaign->{total_players} = scalar(@{$campaign->{users}});
    }
    else {
        $campaign->{total_players} = $campaign->{max_players};
    }
    $campaign->{owned_by_me} = $campaign->{user_id} == $self->{_user}{id};

    # $self->_debug_message($campaign);
}
sub get_user_owned_campaigns {
    my ($self, $user) = @_;
    $user = $self->{_user} unless $user;
    $user = $self->load_user($user) unless ref $user;

    return [] unless $user;
    my $result = [];

    my $sth = $self->_get_dbh('general')->prepare("SELECT * FROM campaigns WHERE user_id=? AND archived=0");
    $sth->execute($user->{id});
    while ( my $row = $sth->fetchrow_hashref() ) {
        $self->_fill_campaign_details($row);
        push @$result, $row;
    }

    return wantarray ? @$result : $result;
}
sub get_user_participating_campaigns {
    my ($self, $user) = @_;
    $user = $self->{_user} unless $user;
    $user = $self->load_user($user) unless ref $user;

    return [] unless $user;
    my $result = [];

    my $sth = $self->_get_dbh('general')->prepare("
        SELECT c.*
        FROM user_campaign_ref r
        INNER JOIN campaigns c ON c.id=r.campaign_id
        WHERE r.user_id=? AND archived=0
    ");
    $sth->execute($user->{id});
    while ( my $row = $sth->fetchrow_hashref() ) {
        $self->_fill_campaign_details($row);
        push @$result, $row;
    }

    return wantarray ? @$result : $result;
}

# 12000052408 > oOZ66d
sub shorten_id {
    my ($self, $numeric_id) = @_;
    my @char_map = ('0' .. '9', 'a' .. 'z', 'A' .. 'Z');
    my $result = '';

    # printf STDERR "shorten %s\n", $numeric_id;

    do {
        my $remainder = $numeric_id % scalar(@char_map);
        $numeric_id = int( $numeric_id / scalar(@char_map) );
        $result .= $char_map[$remainder];
        # printf STDERR "%d > %s. %s\n", $remainder, $char_map[$remainder], $numeric_id;
    } while ( $numeric_id >= 1 );

    # printf STDERR "result: %s\n\n", $result;

    return $result;
}
# oOZ66d > 12000052408
sub unshorten_id {
    my ($self, $short_id) = @_;
    my @char_map = ('0' .. '9', 'a' .. 'z', 'A' .. 'Z');
    my %char_index;
    @char_index{@char_map} = (0 .. $#char_map);

    my $result;

    # printf STDERR "unshorten %s\n", $short_id;

    my $i = 0;
    while ( length($short_id) ) {
        my $char = substr($short_id, 0, 1, '');
        $result += $char_index{$char} * (scalar(@char_map) ** $i);
        # printf STDERR "%s > %d. * %d = %d. %d\n", $char, $char_index{$char}, scalar(@char_map) ** $i, $char_index{$char} * (scalar(@char_map) ** $i), $result;
        $i++;
    }

    # printf STDERR "result: %s\n\n", $result;
    
    return $result;
}
sub create_campaign {
    my ($self, $user) = @_;
    $user = $self->load_user($user) unless ref $user;

    $self->_get_dbh('general')->do("INSERT INTO campaigns(user_id, max_players) VALUES(?, 4)", undef, $user->{id});
    my $new_id = $self->_get_dbh('general')->sqlite_last_insert_rowid();
    my $new_short_id = $self->shorten_id($new_id);
    my $new_secret_number = sprintf("%d%09d", $new_id, int( rand(1_000_000_000) ));
    my $new_secret_string = $self->shorten_id($new_secret_number);
    
    $self->_get_dbh('general')->do(
        "UPDATE campaigns SET short_id=:1, name=:1, secret=:2 WHERE id=:3",
        undef,
        $new_short_id, $new_secret_string, $new_id
    );
    unlink $self->_get_db_filename(undef, campaign_id => $new_id);
    $self->{_campaign} = {
        id      => $new_id,
        user_id => $user->{id},
    };
    return {
        id       => $new_id,
        short_id => $new_short_id,
    };
}
sub find_campaign_by_secret {
    my ($self, $secret, %opts) = @_;

    return $self->_get_dbh('general')->selectrow_hashref(
        "SELECT * FROM campaigns WHERE secret IS NOT NULL AND secret=?",
        undef,
        $secret
    );
}
sub add_user_to_campaign {
    my ($self, $campaign_secret, %opts) = @_;

    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my $campaign = $self->find_campaign_by_secret($campaign_secret);
    return 0 unless $campaign;
    return $campaign if $campaign->{user_id} == $user->{id};
    return 0 if $self->is_campaign_started($campaign);
    my ($players) = $self->_get_dbh('general')->selectrow_array("SELECT COUNT(*) FROM user_campaign_ref WHERE campaign_id=?", undef, $campaign->{id});
    $players++; # owner
    return 0 if $players >= $campaign->{max_players};

    $self->_get_dbh('general')->do(
        "INSERT OR IGNORE INTO user_campaign_ref(user_id, campaign_id) VALUES(?, ?)",
        undef,
        $user->{id}, $campaign->{id}
    );
    return $campaign;
}
sub is_campaign_started {
    my ($self, $campaign, %opts) = @_;
    $campaign //= $self->{_campaign};
    $campaign = $self->load_campaign($campaign) unless ref $campaign;
    die("something's wrong") unless $campaign;

    return -f $self->_get_db_filename(undef, campaign_id => $campaign->{id});
}
sub start_campaign {
    my ($self, %params) = @_;

    $self->_debug_message('start_campaign');

    my $campaign   = $params{campaign} // $self->{_campaign};
    my $user_owner = $params{user}     // $self->{_user};
    $user_owner = $self->load_user($user_owner) unless ref $user_owner;
    $campaign   = $self->load_campaign($campaign) unless ref $campaign;

    die("something's wrong") unless $campaign->{user_id} == $user_owner->{id};

    my $users = $self->get_users(campaign => $campaign);

    $self->_get_dbh()->begin_work();
    eval {

        $self->_create_tables(users => $users);
        # $self->_debug_message('tables created');
        $self->_get_dbh()->do("INSERT INTO game_info(key, value) VALUES('turn', '1')");
        $self->_create_map(users => $users);
        # $self->_debug_message('map created');

        $self->_load_actors();
        $self->_load_map_data();
        $self->update_fow(campaign => $campaign);
        # $self->_debug_message('fow initialized');

        $self->_unload_actors();
        $self->_save_map_data();

        $self->_get_dbh()->commit();
    };
    if ($@) {
        $self->_get_dbh()->rollback();
        die $@;
    }

    $self->_debug_message('started campaign');
}
sub _create_tables {
    my ($self, %params) = @_;

    if ($params{general}) {
        $self->_get_dbh('general')->do($_) for (
            "DROP TABLE IF EXISTS users;",
            "CREATE TABLE users (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                username TEXT UNIQUE,
                password TEXT
            );",
            "DROP TABLE IF EXISTS campaigns;",
            "CREATE TABLE campaigns (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                short_id TEXT,
                user_id INTEGER,
                secret TEXT,
                name TEXT,
                map TEXT,
                max_players INTEGER NOT NULL default 4,
                archived INTEGER NOT NULL default 0
            );",
            "DROP TABLE IF EXISTS user_campaign_ref;",
            "CREATE TABLE user_campaign_ref (
                campaign_id INTEGER,
                user_id INTEGER,
                PRIMARY KEY (campaign_id, user_id)
            );",
        );
    }
    else {
        my $campaign_id = $params{campaign_id} // $self->{_campaign}{id};
        my $users = $params{users};
        my $user_columns = join(",\n", map {'user_' . $_->{id} . '_fow INTEGER DEFAULT 1'} @$users);
        $self->_get_dbh('game', campaign_id => $campaign_id)->do($_) for (
            "DROP TABLE IF EXISTS tiles;",
            "CREATE TABLE tiles (
                x INTEGER,
                y INTEGER,
                tile_type INTEGER,
                raw_wood INTEGER DEFAULT 0,
                raw_gold INTEGER DEFAULT 0,
                raw_food INTEGER DEFAULT 0,
                stored_wood INTEGER DEFAULT 0,
                stored_gold INTEGER DEFAULT 0,
                stored_food INTEGER DEFAULT 0,
                roads INTEGER DEFAULT 0,
                $user_columns,
                PRIMARY KEY (x, y)
            );",
            "DROP TABLE IF EXISTS buildings;",
            "CREATE TABLE buildings (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                user_id INTEGER DEFAULT 0,
                x INTEGER,
                y INTEGER,
                building_type INTEGER,
                building_data TEXT,
                slot INTEGER
            );",
            "DROP TABLE IF EXISTS building_tasks;",
            "CREATE TABLE building_tasks (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                user_id INTEGER,
                building_id INTEGER,
                task_type INTEGER,
                task_data TEXT,
                ap_cost INTEGER DEFAULT NULL,
                progress INTEGER DEFAULT NULL
            );",
            "DROP TABLE IF EXISTS units;",
            "CREATE TABLE units (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                user_id INTEGER,
                army_id INTEGER,
                unit_type INTEGER
            );",
            "DROP TABLE IF EXISTS unit_tasks;",
            "CREATE TABLE unit_tasks (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                unit_id INTEGER DEFAULT 0,
                army_id INTEGER,
                task_type INTEGER,
                task_data TEXT
            );",
            "DROP TABLE IF EXISTS armies;",
            "CREATE TABLE armies (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                user_id INTEGER DEFAULT 0,
                x INTEGER,
                y INTEGER
            );",
            "DROP TABLE IF EXISTS players;",
            "CREATE TABLE players (
                user_id INTEGER PRIMARY KEY,
                color TEXT,
                turn_over INTEGER DEFAULT 0,
                defeated INTEGER DEFAULT 0
            );",
            "DROP TABLE IF EXISTS game_info;",
            "CREATE TABLE game_info (
                key TEXT PRIMARY KEY,
                value TEXT
            );",
            "DROP TABLE IF EXISTS player_log;",
            "CREATE TABLE player_log (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                user_id INTEGER,
                turn INTEGER,
                value TEXT
            );",
        );
    }
}
sub _create_map {
    my ($self, %params) = @_;
    my $users = $params{users};

    my $tmx = $self->{_campaign}{map} || $self->list_tmxs()->[0];
    my $map = $self->load_tmx($tmx);

    
    $self->_get_dbh()->do("DELETE FROM tiles");
    for my $y (0 .. $#{ $map->{tiles} }) {
        for my $x (0 .. $#{$map->{tiles}[$y]}) {
            my $sql = "INSERT INTO tiles(x, y, tile_type, raw_wood, raw_gold, raw_food) VALUES(?, ?, ?, ?, ?, ?);";
            $self->_get_dbh()->do($sql, undef,
                $x,
                $y,
                $map->{tiles}[$y][$x]{tile_type},
                $map->{tiles}[$y][$x]{resources}{wood} // 0,
                $map->{tiles}[$y][$x]{resources}{gold} // 0,
                $map->{tiles}[$y][$x]{resources}{food} // 0,
            );
        }
    }

    my $i = 1;
    for my $user(@$users) {
        last unless scalar(@{ $map->{spawns} });
        $self->_get_dbh()->do(
            "INSERT INTO players(user_id, color) VALUES (?, ?)",
            undef,
            $user->{id}, DEFAULT_COLORS->[$i]
        );

        my $spawn = splice(@{ $map->{spawns} }, int( rand scalar(@{ $map->{spawns} }) ), 1);
        my $x = $spawn->[0];
        my $y = $spawn->[1];
        $self->_get_dbh()->do(
            "INSERT INTO buildings(x, y, user_id, building_type, slot) VALUES (?, ?, ?, ?, ?)",
            undef,
            $x, $y, $user->{id}, BUILDING_TYPE_HOUSE(), 1
        );
        $self->_get_dbh()->do(
            "UPDATE tiles SET stored_wood=?, stored_gold=?, stored_food=? WHERE x=? AND y=?",
            undef,
            100, 100, 100, $x, $y
        );
        $self->_get_dbh()->do(
            "INSERT INTO armies(x, y, user_id) VALUES(?, ?, ?)",
            undef,
            $x, $y, $user->{id}
        );
        my $army_id = $self->_get_dbh()->sqlite_last_insert_rowid();
        $self->_get_dbh()->do(
            "INSERT INTO units(user_id, army_id, unit_type) VALUES(?, ?, ?)",
            undef,
            $user->{id}, $army_id, UNIT_TYPE_INFANTRY()
        );

        $i++;
    }
}
sub list_tmxs {
    my ($self) = @_;

    if (!$self->{_tmxs_list}) {
        my $dirpath = $self->{_data_path} . 'maps/';
        opendir(my $dh, $dirpath) || die "Can't opendir $dirpath: $!";
        my @tmxs = grep {-f "$dirpath/$_" && /\.tmx$/} readdir($dh);
        closedir $dh;

        $self->{_tmxs_list} = \@tmxs;
    }

    return $self->{_tmxs_list};
}
sub load_tmx {
    my ($self, $mapname) = @_;

    $self->_debug_message($mapname);

    my $filepath = $self->{_data_path} . 'maps/' . $mapname;

    my $out;

    my $in_xml = XMLin($filepath,
        KeyAttr => {},
        ForceArray => ['tileset', 'tile', 'layer', 'property', 'object'],
    );

    my $properties = {};
    my $objects_offset = 0;
    for my $property(@{ $in_xml->{properties}{property} }) {
        my @keys = split ':', $property->{name};
        my $hash = $properties;
        while (@keys > 1) {
            my $key = shift @keys;
            $hash = $hash->{$key} = {};
        }
        $hash->{$keys[0]} = $property->{value};
    }
    for my $tileset(@{ $in_xml->{tileset} }) {
        if ($tileset->{name} eq 'objects') {
            $objects_offset = $tileset->{firstgid} - 1;
        }
    }

    for my $layer(@{ $in_xml->{layer} }) {
        my $y = 0;
        for my $row(split "\n", $layer->{data}{content}) {
            next unless $row;
            my $x = 0;
            for my $col(split ",", $row) {
                if ($layer->{name} eq 'tiles') {
                    $out->{tiles}[$y][$x]{tile_type} = $col;
                    $out->{tiles}[$y][$x]{resources} //= $properties->{ TILE_NAMES->{$col} };
                }
                elsif ($layer->{name} eq 'objects') {
                    if ($col) {
                        my $object_type = $col - $objects_offset;
                        if ($object_type == 1 || $object_type == 2 || $object_type == 3) {
                            $out->{tiles}[$y][$x]{resources} = $properties->{ MAP_OBJECTS->{$object_type} };
                        }
                        elsif ($object_type == 4) {
                            push @{ $out->{spawns} }, [$x, $y];
                        }
                    }
                }
                $x++;
            }
            $y++;
        }
    }

    return $out;
}
sub load_campaign {
    my ($self, $short_id) = @_;

    return $self->_get_dbh('general')->selectrow_hashref(
        "SELECT * FROM campaigns WHERE short_id=?",
        undef,
        $short_id
    );
}
sub set_campaign {
    my ($self, $campaign) = @_;
    $campaign = $self->load_campaign($campaign) unless ref $campaign;
    $self->{_campaign} = $campaign;
}
sub is_user_owner {
    my ($self, $user) = @_;
    $user = $self->{_user} unless $user;
    $user = $self->load_user($user) unless ref $user;
    die('no user provided') unless $user;
    die('no campaign provided') unless $self->{_campaign} && $self->{_campaign}{user_id};

    return 1 if $self->{_campaign}{user_id} == $user->{id};
}
sub is_user_allowed {
    my ($self, $user) = @_;
    $user = $self->{_user} unless $user;
    $user = $self->load_user($user) unless ref $user;
    die('no user provided') unless $user;
    die('no campaign provided') unless $self->{_campaign} && $self->{_campaign}{user_id};

    return 1 if $self->{_campaign}{user_id} == $user->{id};
    my $rel = $self->_get_dbh('general')->selectrow_hashref(
        "
            SELECT *
            FROM user_campaign_ref
            WHERE user_id=? AND campaign_id=?
        ",
        undef,
        $user->{id}, $self->{_campaign}{id}
    );
    return 1 if $rel;
    return 0;
}
sub change_campaign_details {
    my ($self, %params) = @_;

    die('no campaign provided') unless $self->{_campaign} && $self->{_campaign}{user_id};

    my @change = grep { $params{$_} } grep {$_ eq 'map' || $_ eq 'name' || $_ eq 'max_players'} keys %params;

    return unless @change;

    if (exists $params{map}) {
        return unless grep {$params{map} eq $_} @{$self->list_tmxs()};
    }

    my $set_sql = 'SET ' . join(', ', map {"$_ = ?"} @change);

    return $self->_get_dbh('general')->do(
        "UPDATE campaigns $set_sql WHERE id=?",
        undef,
        @params{@change}, $self->{_campaign}{id}
    );
}
sub get_players {
    my ($self, %opts) = @_;

    return $self->_get_dbh('game', campaign_id => $opts{campaign_id})->selectall_hashref("SELECT * FROM players", 'user_id');
}
sub get_users {
    my ($self, %params) = @_;
    my $campaign = $params{campaign} // $self->{_campaign};
    $campaign = $self->load_campaign($campaign) unless ref $campaign;

    my $users = $self->_get_dbh('general')->selectall_arrayref(
        "
            SELECT users.*
            FROM user_campaign_ref r
            INNER JOIN users ON users.id=r.user_id
            WHERE r.campaign_id=?
        ",
        { Slice => {} },
        $campaign->{id}
    );
    my $user_owner = $self->_get_dbh('general')->selectrow_hashref(
        "SELECT * FROM users WHERE id=?",
        undef,
        $campaign->{user_id}
    );
    $user_owner->{is_owner} = 1;
    unshift @$users, $user_owner;

    return $users;
}

sub _get_game_info {
    my ($self, %opts) = @_;

    my $info;
    my $sth = $self->_get_dbh('game', campaign_id => $opts{campaign_id})->prepare("SELECT key, value FROM game_info");
    $sth->execute();
    while ( my $row = $sth->fetchrow_arrayref() ) {
        $info->{ $row->[0] } = $row->[1];
    }
    return $info;
}
sub get_game_info {
    my ($self, %opts) = @_;

    my $info = {};
    $info->{campaign} = {
        id          => $self->{_campaign}{id},
        short_id    => $self->{_campaign}{short_id},
        %{ $self->_get_game_info() },
    };
    $info->{user} = {
        id          => $self->{_user}{id},
    };
    $info->{players} = $self->_get_dbh('game', campaign_id => $opts{campaign_id})->selectall_arrayref(
        "SELECT user_id, color, turn_over, defeated FROM players",
        { Slice => {} },
    );
    return $info;
}
sub get_player_log {
    my ($self, %params) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my @result = ();
    my $sth = $self->_get_dbh()->prepare("SELECT * FROM player_log WHERE user_id=? AND turn=? ORDER BY id");
    $sth->execute($user->{id}, $params{turn});
    while ( my $line = $sth->fetchrow_hashref() ) {
        push @result, decode_json($line->{value});
    }

    return \@result;
}
sub surrender {
    my ($self, %params) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    $self->_get_dbh()->do(
        "UPDATE players SET turn_over=1, defeated=1 WHERE user_id=?",
        undef,
        $user->{id}
    );

    return 'ok';
}

sub _load_map {
    my ($self, $left, $top, $right, $bottom) = @_;
    my $result;

    my $tiles_sth = $self->_get_dbh()->prepare(
        "SELECT * FROM tiles WHERE x >= ? AND y >= ? AND x <= ? AND y <= ?",
    );
    $tiles_sth->execute($left, $top, $right, $bottom);
    while ( my $tile = $tiles_sth->fetchrow_hashref() ) {
        my $fow = $tile->{'user_' . $self->{_user}{id} . '_fow'};
        $result->[$tile->{y}][$tile->{x}] = {
            fow         => $fow,
            slots_taken => [0, 0, 0, 0],
            $fow > 0 ? (
                tile_type => $tile->{tile_type},
                roads     => 0,
            ) : (),
            $fow == 2 ? (
                raw => {
                    wood    => $tile->{raw_wood},
                    gold    => $tile->{raw_gold},
                    food    => $tile->{raw_food},
                },
                stored => {
                    wood    => $tile->{stored_wood},
                    gold    => $tile->{stored_gold},
                    food    => $tile->{stored_food},
                },
                roads       => $tile->{roads},
            ) : (),
        };
    }

    my $buildings_sth = $self->_get_dbh()->prepare(
        "SELECT * FROM buildings WHERE x >= ? AND y >= ? AND x <= ? AND y <= ? ORDER BY id",
    );
    $buildings_sth->execute($left, $top, $right, $bottom);
    while ( my $building = $buildings_sth->fetchrow_hashref() ) {
        next unless $result->[$building->{y}][$building->{x}]{fow} == 2;
        $result->[$building->{y}][$building->{x}]{buildings}{ $building->{id} } = {
            id            => $building->{id},
            user_id       => $building->{user_id},
            building_type => $building->{building_type},
            building_data => $building->{building_data},
            slot          => $building->{slot},
        };
        $result->[$building->{y}][$building->{x}]{slots_taken}[$building->{slot}] = 1;
        my ($hsize, $vsize) = split('x', BUILDING_SPACES->{ $building->{building_type} });
        $result->[$building->{y}][$building->{x}]{slots_taken}[$building->{slot} + 1] = 1 if $hsize > 1;
        $result->[$building->{y}][$building->{x}]{slots_taken}[$building->{slot} + 2] = 1 if $vsize > 1;
    }

    my $buildings_tasks_sth = $self->_get_dbh()->prepare("
        SELECT building_tasks.*, buildings.x, buildings.y
        FROM buildings
        INNER JOIN building_tasks ON building_tasks.building_id=buildings.id
        WHERE x >= ? AND y >= ? AND x <= ? AND y <= ? AND buildings.user_id=?
        ORDER BY building_tasks.id
    ");
    $buildings_tasks_sth->execute($left, $top, $right, $bottom, $self->{_user}{id});
    while ( my $task = $buildings_tasks_sth->fetchrow_hashref() ) {
        push @{ $result->[$task->{y}][$task->{x}]{buildings}{ $task->{building_id} }{tasks} }, {
            id        => $task->{id},
            task_type => $task->{task_type},
            task_data => $task->{task_data},
            ap_cost   => $task->{ap_cost},
            progress  => $task->{progress},
        };
    }

    my $armies_sth = $self->_get_dbh()->prepare(
        "SELECT * FROM armies WHERE x >= ? AND y >= ? AND x <= ? AND y <= ?"
    );
    $armies_sth->execute($left, $top, $right, $bottom);
    while ( my $army = $armies_sth->fetchrow_hashref() ) {
        next unless $result->[$army->{y}][$army->{x}]{fow} == 2;
        $result->[$army->{y}][$army->{x}]{armies}{ $army->{id} } = {
            user_id          => $army->{user_id},
            units            => [],
        };
    }

    my $units_sth = $self->_get_dbh()->prepare("
        SELECT units.*, armies.x, armies.y
        FROM armies
        INNER JOIN units ON units.army_id=armies.id
        WHERE x >= ? AND y >= ? AND x <= ? AND y <= ?
        ORDER BY units.id
    ");
    $units_sth->execute($left, $top, $right, $bottom);
    while ( my $unit = $units_sth->fetchrow_hashref() ) {
        next unless $result->[$unit->{y}][$unit->{x}]{fow} == 2;
        push @{ $result->[$unit->{y}][$unit->{x}]{armies}{ $unit->{army_id} }{units} }, {
            unit_type => $unit->{unit_type},
            user_id   => $unit->{user_id},
            army_id   => $unit->{army_id},
            id        => $unit->{id},
        };
    }

    return $result;
}

sub get_map {
    my ($self) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;
    # TODO: не умирать а возвращать 400

    return $self->_load_map(0, 0, 100, 100);
}
sub get_army_details {
    my ($self, %params) = @_;

    return $self->get_army_queue($params{id});
}

sub validate_structure {
    my ($self, $data, $pattern) = @_;

    if (my $data_type = ref($pattern) ) {
        return 0 unless ref($data) eq $data_type;

        if ($data_type eq 'HASH') {
            if (keys(%$pattern) == 1) {
                my $subpattern = (keys(%$pattern))[0];
                for my $key(keys %$data) {
                    return 0 unless $key =~ $subpattern;
                    return 0 unless $self->validate_structure($data->{$key}, $pattern->{$subpattern});
                }
                return 1;
            }
            else {
                return 0 unless keys(%$data) == keys(%$pattern);

                for my $key(keys %$pattern) {
                    return 0 unless exists $data->{$key};
                    return 0 unless $self->validate_structure($data->{$key}, $pattern->{$key});
                }
                return 1;
            }
        }
        elsif ($data_type eq 'ARRAY') {
            my $j = 0;
            for my $i(0 .. $#{$pattern}) {
                my $subpattern = $pattern->[$i];

                if (ref($subpattern) eq 'HASH' && keys(%$subpattern) == 1 && defined $subpattern->{'?'}) {
                    next unless $j <= $#{$data} && $self->validate_structure($data->[$i], $subpattern->{'?'});
                    $j++;
                }
                elsif (ref($subpattern) eq 'HASH' && keys(%$subpattern) == 1 && defined $subpattern->{'*'}) {
                    while ($j <= $#{$data} && $self->validate_structure($data->[$j], $subpattern->{'*'}) ) {
                        $j++;
                    }
                }
                else {
                    return 0 unless exists $data->[$i];
                    return 0 unless $self->validate_structure($data->[$i], $subpattern);
                    $j++;
                }
            }

            if ( $j == scalar(@$data) ) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }
    else {
        return $data =~ $pattern;
    }

    return 0;
}
sub validate_task_data {
    my ($self, $performer, %params) = @_;

    my $performer_group = exists $performer->{building_type} ? 'building' : 'unit';
    my $performer_type  = $performer->{$performer_group . '_type'};

    return 0 unless AVAILABLE_TASKS->{$performer_group}{$performer_type}{ $params{task_type} };
    if (
        $params{task_type} == TASK_TYPE_HARVEST_WOOD ||
        $params{task_type} == TASK_TYPE_HARVEST_GOLD ||
        $params{task_type} == TASK_TYPE_HARVEST_FOOD ||
        $params{task_type} == TASK_TYPE_IDLE
    ) {
        if (!$params{task_data}) {
            return 1;
        }
    }
    elsif (
        $params{task_type} == TASK_TYPE_HIRE_INFANTRY ||
        $params{task_type} == TASK_TYPE_HIRE_ARCHER ||
        $params{task_type} == TASK_TYPE_HIRE_CAVALRY
    ) {
        return $self->validate_structure($params{task_data}, '^\d+$');
    }
    elsif (
        # $params{task_type} == TASK_TYPE_BUILD_ROAD ||
        $params{task_type} == TASK_TYPE_BUILD_HOUSE ||
        $params{task_type} == TASK_TYPE_BUILD_SAWMILL ||
        $params{task_type} == TASK_TYPE_BUILD_MINE ||
        $params{task_type} == TASK_TYPE_BUILD_FARM ||
        $params{task_type} == TASK_TYPE_BUILD_BARRACKS ||
        $params{task_type} == TASK_TYPE_BUILD_SHOOTING_RANGE ||
        $params{task_type} == TASK_TYPE_BUILD_STADIUM
    ) {
        my $task_data = decode_json($params{task_data});
        print STDERR Dumper($task_data);

        return $self->validate_structure($task_data, {
            position => ['^\d+$', '^\d+$', '^\d+$'],
            storages => {
                '^\d+\;\d+$' => {wood => '^\d+$', gold => '^\d+$', food => '^\d+$'},
            },
            roads    => [ {'*' => [{ '*' => '^\d+\;\d+$'}]} ],
        });
    }
    elsif (
        $params{task_type} == TASK_TYPE_DESTROY_BUILDING
    ) {
        if ($params{task_data} =~ /^[b]\:\d+$/) {
            return 1;
        }
    }
    elsif (
        $params{task_type} == TASK_TYPE_DIRECT_RESOURCE
    ) {
        if ($params{task_data} =~ /^\d+\;\d+$/) {
            return 1;
        }
    }
    elsif (
        $params{task_type} == TASK_TYPE_TRANSPORT_RESOURCE
    ) {
        my $task_data = decode_json($params{task_data});

        return $self->validate_structure($task_data, {
            from => ['^\d+$', '^\d+$'],
            to   => ['^\d+$', '^\d+$'],
            wood => '^\d+$',
            gold => '^\d+$',
            food => '^\d+$',
        });
    }
    elsif (
        $params{task_type} == TASK_TYPE_JOIN_ARMY
    ) {
        $self->_debug_message($params{task_data});
        return $self->validate_structure($params{task_data}, '^\d+$');
    }

    return 0;
}
sub validate_army_task_data {
    my ($self, $tasks) = @_;

    for my $task(@$tasks) {
        if ($task->{task_type} == ARMY_TASK_TYPE_MOVE) {
            return 0 unless scalar(keys %$task) == 2;
            unless ($task->{task_data} eq int($task->{task_data}) && $task->{task_data} >= 0 && $task->{task_data} <= 5) {
                return 0;
            }
        }
        elsif ($task->{task_type} == ARMY_TASK_TYPE_INTERCEPT) {
            return $self->validate_structure($task->{task_data}, '^\d+$');
        }
        else {
            return 0;
        }
    }

    return 1;
}
sub add_building_task {
    my ($self, %params) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my $building;
    if ($params{building_id} eq '0') {
        unless ( $self->validate_task_data({building_type => BUILDING_TYPE_GLOBAL}, %params) ) {
            die "something's wrong";
        }
    }
    else {
        $building = $self->_get_dbh()->selectrow_hashref(
            "SELECT * FROM buildings WHERE id=? AND user_id=?",
            undef,
            $params{building_id}, $user->{id}
        );
        unless ($building && $self->validate_task_data($building, %params) ) {
            die "something's wrong";
        }
    }

    if ($params{clear}) {
        $self->_get_dbh()->do(
            "DELETE FROM building_tasks WHERE building_id=? AND user_id=?",
            undef,
            $params{building_id}, $user->{id}
        );
    }

    my $building_name = $building ? BUILDING_TYPE_NAMES->{ $building->{building_type} } : 'global';
    my $ap_cost = TASK_AP_COSTS->{ $params{task_type} }{$building_name}
               // TASK_AP_COSTS->{ $params{task_type} }{default}
               // $self->calculate_task_cost($building, \%params);
    # $self->_debug_message('ap_cost', $building_name, \%params, $ap_cost);

    $self->_get_dbh()->do(
        "INSERT INTO building_tasks(user_id, building_id, task_type, task_data, ap_cost) VALUES(?, ?, ?, ?, ?)",
        undef,
        $user->{id}, $params{building_id}, $params{task_type}, $params{task_data}, $ap_cost
    );

    return $self->get_building_queue($params{building_id});
}
sub remove_building_task {
    my ($self, %params) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my $task = $self->_get_dbh()->selectrow_hashref(
        "SELECT * FROM building_tasks WHERE id=? AND user_id=?",
        undef,
        $params{id}, $user->{id}
    );
    die( sprintf('not found task %s for user %d', $params{id}, $user->{id}) ) unless $task;

    $self->_get_dbh()->do(
        "DELETE FROM building_tasks WHERE id=?",
        undef,
        $task->{id}
    );

    return $self->get_building_queue($task->{building_id});
}
sub get_building_queue {
    my ($self, $building_id) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    unless ($building_id eq '0') {
        my $building = $self->_get_dbh()->selectrow_hashref(
            "SELECT * FROM buildings WHERE id=? AND user_id=?",
            undef,
            $building_id, $user->{id}
        );
        die("something's wrong") unless $building;
    }

    return $self->_get_dbh()->selectall_arrayref(
        "
            SELECT id, task_type, task_data, ap_cost, progress
            FROM building_tasks
            WHERE building_id=? AND user_id=?
            ORDER BY id
        ",
        { Slice => {} },
        $building_id, $user->{id}
    );
}
sub load_building_queue {
    my ($self, %params) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    return $self->get_building_queue($params{building_id});
}

# TODO: объединить со зданиями?
sub add_unit_task {
    my ($self, %params) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my $unit = $self->_get_dbh()->selectrow_hashref(
        "SELECT * FROM units WHERE id=? AND user_id=?",
        undef,
        $params{unit_id}, $user->{id}
    );
    $self->_debug_message('add_unit_task', $unit, \%params);
    if ($unit && $self->validate_task_data($unit, %params) ) {
        $self->_get_dbh()->do(
            "INSERT INTO unit_tasks(army_id, unit_id, task_type, task_data) VALUES(?, ?, ?, ?)",
            undef,
            $unit->{army_id}, $unit->{id}, $params{task_type}, $params{task_data}
        );
    }
    else {
        die "something's wrong";
    }

    return $self->_get_unit_queue($params{unit_id});
}
sub remove_unit_task {
    my ($self, %params) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my $task = $self->_get_dbh()->selectrow_hashref("SELECT * FROM unit_tasks WHERE id=?", undef, $params{id});
    if ($task) {
        my $unit = $self->_get_dbh()->selectrow_hashref(
            "SELECT * FROM units WHERE id=? AND user_id=?",
            undef,
            $task->{unit_id}, $user->{id}
        );
        die("something's wrong") unless $unit;
        $self->_get_dbh()->do(
            "DELETE FROM unit_tasks WHERE id=?",
            undef,
            $task->{id}
        );

        return $self->_get_unit_queue($task->{unit_id});
    }
    return [];
}
sub _get_unit_queue {
    my ($self, $unit_id) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my $unit = $self->_get_dbh()->selectrow_hashref(
        "SELECT * FROM units WHERE id=? AND user_id=?",
        undef,
        $unit_id, $user->{id}
    );
    die("something's wrong") unless $unit;

    my $result = [];
    my $tasks = $self->_get_dbh()->selectall_arrayref(
        "
            SELECT *
            FROM unit_tasks
            WHERE unit_id=?
            ORDER BY id
        ",
        { Slice => {} },
        $unit_id
    );
    for my $task(@$tasks) {
        push @$result, {
            id        => $task->{id},
            task_type => $task->{task_type},
            task_data => $task->{task_data},
        };
    }

    return $result;
}
sub get_unit_queue {
    my ($self, %params) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    return $self->_get_unit_queue($params{unit_id});
}

sub add_army_tasks {
    my ($self, %params) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my $army = $self->_get_dbh()->selectrow_hashref(
        "SELECT * FROM armies WHERE id=? AND user_id=?",
        undef,
        $params{army_id}, $user->{id}
    );

    my $tasks = decode_json($params{tasks});
    $tasks = [$tasks] unless ref($tasks) eq 'ARRAY';

    if ($army && $self->validate_army_task_data($tasks) ) {
        $self->_get_dbh()->begin_work();
        eval {
            for my $task(@$tasks) {
                $self->_get_dbh()->do(
                    "INSERT INTO unit_tasks(army_id, task_type, task_data) VALUES(?, ?, ?)",
                    undef,
                    $params{army_id}, $task->{task_type}, $task->{task_data}
                );
            }
            $self->_get_dbh()->commit();
        };
        if ($@) {
            $self->_get_dbh()->rollback();
            die $@;
        }
    }
    else {
        die("something's wrong");
    }

    return $self->get_army_queue($params{army_id});
}
sub remove_army_tasks {
    my ($self, %params) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my $army = $self->_get_dbh()->selectrow_hashref(
        "SELECT * FROM armies WHERE id=? AND user_id=?",
        undef,
        $params{army_id}, $user->{id}
    );
    
    if ($army) {
        $self->_get_dbh()->do(
            "DELETE FROM unit_tasks WHERE army_id=?",
            undef,
            $params{army_id}
        );

        return $self->get_army_queue($params{army_id});
    }
    else {
        die("something's wrong");
    }
    return [];
}
sub get_army_queue {
    my ($self, $army_id) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my $army = $self->_get_dbh()->selectrow_hashref(
        "SELECT * FROM armies WHERE id=? AND user_id=?",
        undef,
        $army_id, $user->{id}
    );
    die("something's wrong") unless $army;

    my $result = [];
    my $tasks = $self->_get_dbh()->selectall_arrayref(
        "
            SELECT *
            FROM unit_tasks
            WHERE army_id=?
            ORDER BY id
        ",
        { Slice => {} },
        $army_id
    );
    for my $task(@$tasks) {
        push @$result, {
            id        => $task->{id},
            task_type => $task->{task_type},
            task_data => $task->{task_data},
            unit_id   => $task->{unit_id},
        };
    }

    return $result;
}

sub end_turn {
    my ($self) = @_;
    my $user = $self->{_user};
    die 'no user provided' unless $user;

    my ($turn) = $self->_get_dbh()->selectrow_array("SELECT value FROM game_info WHERE key='turn'");

    # mutex
    my $updated = $self->_get_dbh()->do(
        "UPDATE players SET turn_over=1 WHERE user_id=?",
        undef,
        $user->{id}
    );
    return {result => 'ok'} unless $updated;

    my ($nonready_players) = $self->_get_dbh()->selectrow_array(
        "SELECT COUNT(*) FROM players WHERE turn_over=0" # или under_roll?
    );
    return {result => 'ok'} if $nonready_players;

    $self->{_current_turn} = $turn;
    $self->_get_dbh()->begin_work();
    my $result = eval {
        $self->_debug_message("end turn");
        # TODO: lazy load? or no load?
        $self->_load_actors();
        $self->_load_map_data();

        my $players = $self->get_players();
        while (my ($user_id, $player) = each %$players) {
            $self->process_global_queue($player);
        }
        
        while (my ($building_id, $building) = each %{ $self->{_buildings} }) {
            $self->process_building_queue($building);
        }

        # my $units = $self->_get_dbh()->selectall_arrayref(
        #     "SELECT id FROM units",
        #     { Slice => {} },
        # );
        # for my $unit(@$units) {
        #     $self->process_unit_queue($unit);
        # }

        my $step_ap = 12;
        $self->_update_armies_in_tiles();
        while ($step_ap > 0) {
            while (my ($army_id, $army) = each %{ $self->{_armies} }) {
                next if $army->{defeated};
                next if $army->{action_points} < $step_ap;
                $self->process_army_queue($army);
            }

            $self->_update_armies_in_tiles();
            while (my ($pos, $users) = each %{ $self->{_armies_in_tiles} }) {
                if (scalar(keys %$users) > 1) {
                    $self->resolve_battle($users);
                }
            }

            $step_ap -= 0.5;
        }

        $self->update_fow();
        $self->update_intercepts();
        $self->capture_buildings();
        $self->update_defeated();

        $self->_save_actors();
        $self->_unload_actors();
        $self->_save_map_data();

        $self->_get_dbh()->do("UPDATE game_info SET value=? WHERE key='turn'", undef, $turn + 1);
        $self->_get_dbh()->do("UPDATE players SET turn_over=0 WHERE defeated=0");
        $self->_get_dbh()->commit();
        $self->_debug_message("turn ended");

        undef $self->{_map};
        undef $self->{_map_graph};
        undef $self->{_roads_graph};
    };
    undef $self->{_current_turn};
    if ($@) {
        $self->_get_dbh()->rollback();
        die $@;
    }

    unless ($self->{debug_enabled}) {
        $self->{_ua}->post('https://discordapp.com/api/webhooks/365082941857988608/jPOJLPWSlHohp7AhBbiN-MuxM7RQo0RWcXYS5JAf5XhsVZdkbUZsuIf0U1eED2MksIeN', {
            content => 'Игра ' . $self->{_campaign}{name} . ': ход №' . $turn . ' окончен',
        });
    }

    return {result => 'turn_over'};
}
sub update_fow {
    my ($self, %params) = @_;
    my $campaign = $params{campaign} // $self->{_campaign};
    $campaign = $self->load_campaign($campaign) unless ref $campaign;

    for my $actor(values %{ $self->{_buildings} }, values %{ $self->{_armies} }) {
        for my $actor_pos([$actor->{x}, $actor->{y}], $actor->{path} ? @{ $actor->{path} } : ()) {
            my $surrounding_pos = $self->get_surrounding_pos(@$actor_pos);

            for my $pos ($actor_pos, @$surrounding_pos) {
                $self->{_map}{$pos->[0]}{$pos->[1]}{'user_' . $actor->{user_id} . '_fow'} = 2;
            }
        }

        # TODO: перенести отсюда
        if ($actor->{path}) {
            $self->add_player_log(
                $actor->{user_id},
                LOG_TYPE_ARMY_MOVE,
                $actor->{id},
                $actor->{path}[0][0] . ';' . $actor->{path}[0][1],
                $actor->{x} . ';' . $actor->{y},
                12 - $actor->{action_points},
            );
        }
    }
}
sub update_defeated {
    my ($self, %params) = @_;

    my %undefeated_players;
    for my $actor(values %{ $self->{_buildings} }, values %{ $self->{_armies} }) {
        next if $actor->{defeated};
        $undefeated_players{ $actor->{user_id} } = 1;
    }

    $self->_get_dbh()->do(
        'UPDATE players SET defeated=1 WHERE user_id NOT IN(' . join(',', keys %undefeated_players) . ')'
    );
}
sub update_intercepts {
    my ($self, %params) = @_;

    my @lost;
    my $sth = $self->_get_dbh()->prepare("SELECT * FROM unit_tasks WHERE task_type=?");
    $sth->execute();
    while ( my $task = $sth->fetchrow_hashref() ) {
        my $army = $self->{_armies}{ $task->{task_data} };
        if (
            $army->{defeated} ||
            $self->{_map}{ $army->{x} }{ $army->{y} }{'user_' . $task->{user_id} . '_fow'} < 2
        ) {
            push @lost, $task->{id};
        }
    }

    $self->_get_dbh()->do('DELETE FROM unit_tasks WHERE id IN(' . join(',', @lost) . ')');
}
sub get_surrounding_pos {
    my ($self, $x, $y, $distance) = @_;
    $distance //= 2;

    my %surrounding_pos;
    for my $direction(@{ DIRECTIONS->[$y % 2] }) {
        my $adjacent_x = $x + $direction->[0];
        my $adjacent_y = $y + $direction->[1];
        $surrounding_pos{$adjacent_x . '_' . $adjacent_y} = 1;
        if ($distance > 1) {
            my $secondary_surrounding_pos = $self->get_surrounding_pos($adjacent_x, $adjacent_y, $distance - 1);
            for my $pos(@$secondary_surrounding_pos) {
                $surrounding_pos{$pos->[0] . '_' . $pos->[1]} = 1;
            }
        }
    }
    return [map {[split '_', $_]} keys %surrounding_pos];
}
sub capture_buildings {
    my ($self, %params) = @_;
    my $campaign = $params{campaign} // $self->{_campaign};
    $campaign = $self->load_campaign($campaign) unless ref $campaign;

    my $users_armies_in_tiles;
    while (my ($army_id, $army) = each %{ $self->{_armies} }) {
        next if $army->{defeated};
        $users_armies_in_tiles->{ $army->{x} . '_' . $army->{y} }{ $army->{user_id} } = 1;
    }
    while (my ($building_id, $building) = each %{ $self->{_buildings} }) {
        my @user_presense = keys %{ $users_armies_in_tiles->{ $building->{x} . '_' . $building->{y} } };
        if (scalar(@user_presense) == 1) {
            $building->{user_id} = $user_presense[0];
        }
    }
}
sub add_player_log {
    my ($self, $user_id, $log_type, @params) = @_;
    my $turn = $self->{_current_turn};

    $self->_get_dbh()->do(
        "INSERT INTO player_log(user_id, turn, value) VALUES(?, ?, ?)",
        undef,
        $user_id, $turn, encode_json([$log_type, @params])
    );
}


sub process_global_queue {
    my ($self, $player, %opts) = @_;

    my $tasks = $self->_get_dbh()->selectall_arrayref(
        "SELECT * FROM building_tasks WHERE building_id=? AND user_id=? ORDER BY id",
        { Slice => {} },
        0, $player->{user_id}
    );

    my @processed_task_ids;
    for my $task(@$tasks) {
        $task->{ap_cost} = -1 unless defined $task->{ap_cost};
        if ($task->{ap_cost} < 0) {
            next;
        }
        unless (defined $task->{progress}) {
            $task->{progress} = 0;
            my $initialized = $self->initialize_task(undef, $task);
            unless ($initialized) {
                push @processed_task_ids, $task->{id};
                next;
            }
        }
        my $ap_available = 12;
        if ($ap_available < ($task->{ap_cost} - $task->{progress})) {
            $self->_get_dbh()->do(
                "UPDATE building_tasks SET progress=? WHERE id=?",
                undef,
                $task->{progress} + $ap_available, $task->{id}
            );
            next;
        }

        $self->perform_global_task($player, $task);

        push @processed_task_ids, $task->{id};
    }

    $self->_get_dbh()->do("DELETE FROM building_tasks WHERE id IN (" . join(',', @processed_task_ids) . ")");
}
sub process_building_queue {
    my ($self, $building, %opts) = @_;

    my $tasks = $self->_get_dbh()->selectall_arrayref(
        "SELECT * FROM building_tasks WHERE building_id=? ORDER BY id",
        { Slice => {} },
        $building->{id},
    );

    my $defaulted;
    if (!scalar(@$tasks) && defined DEFAULT_TASKS->{ $building->{building_type} }) {
        @$tasks = ({
            task_type   => DEFAULT_TASKS->{ $building->{building_type} },
            id          => 'default_' . $building->{id},
            building_id => $building->{id},
            ap_cost     => 12,
            user_id     => $building->{user_id},
        });
        $defaulted = 1;
    }

    my %task_results;
    for my $task(@$tasks) {
        $task->{ap_cost} = -1 unless defined $task->{ap_cost};
        if ($task->{ap_cost} < 0) {
            $task_results{ $task->{id} } = 0;
            last;
        }
        unless (defined $task->{progress}) {
            $task->{progress} = 0;
            my $initialized = $self->initialize_task($building, $task);
            unless ($initialized) {
                $task_results{ $task->{id} } = 0;
                next;
            }
        }

        if ( $building->{action_points} < ($task->{ap_cost} - $task->{progress}) ) {
            $self->_get_dbh()->do(
                "UPDATE building_tasks SET progress=? WHERE id=?",
                undef,
                $task->{progress} + $building->{action_points}, $task->{id}
            );
            last;
        }
        $building->{action_points} -= $task->{ap_cost};

        $task_results{ $task->{id} } = $self->perform_task($building, $task, performer_group => 'building', dont_log_fail => $defaulted);
    }

    $self->_get_dbh()->do("DELETE FROM building_tasks WHERE id IN (" . join(',', grep {substr($_, 0, 7) ne 'default'} keys %task_results) . ")");

    if (@$tasks == 1 && keys(%task_results) == 1 && AUTOCOPY_TASKS->{ $tasks->[0]{task_type} }) {
        # Больше не осталось задач, так что заполним очередь тем же что было
        my $task = $tasks->[0];
        if (!$task_results{ $task->{id} }) {
            $task->{task_type} = FAILED_TASK_REPLACEMENT->{ $task->{task_type} };
        }
        $self->_debug_message('duplicate task', $task, $task_results{ $task->{id} });
        $self->_get_dbh()->do(
            "INSERT INTO building_tasks (user_id, building_id, task_type, task_data, ap_cost) VALUES(?, ?, ?, ?, ?)",
            undef,
            $task->{user_id}, $task->{building_id}, $task->{task_type}, $task->{task_data}, $task->{ap_cost}
        );
    }
}
sub process_army_queue {
    my ($self, $army) = @_;

    my $delete_task = 1;

    my $task = $army->{tasks}[0] or return;

    if ($task->{unit_id}) {
        my $unit = $self->{_units}{ $task->{unit_id} };
        my $task_cost =
            TASK_AP_COSTS->{ $task->{task_type} }{ 'unit_' . $unit->{unit_type} }
            // TASK_AP_COSTS->{ $task->{task_type} }{default}
            // 12;
        return if ($unit->{action_points} < $task_cost);
        $unit->{action_points} -= $task_cost;
        $self->perform_task($unit, $task, performer_group => 'unit');

        $army->{action_points} = $unit->{action_points} if $unit->{action_points} < $army->{action_points};
    }
    else {
        my $task_cost = ARMY_TASK_AP_COSTS->{ $task->{task_type} } // 12;
        return if ($army->{action_points} < $task_cost);
        $army->{action_points} -= $task_cost;
        if ($task->{task_type} == ARMY_TASK_TYPE_MOVE) {
            my $new_x = $army->{x} + DIRECTIONS->[$army->{y} % 2][$task->{task_data}][0];
            my $new_y = $army->{y} + DIRECTIONS->[$army->{y} % 2][$task->{task_data}][1];

            $delete_task = 0 if !$self->move_army($army, $new_x, $new_y);
        }
        elsif ($task->{task_type} == ARMY_TASK_TYPE_INTERCEPT) {
            if ($self->{_armies}{ $task->{task_data} }) {
                unless ($army->{x} == $self->{_armies}{ $task->{task_data} }{x}
                     && $army->{y} == $self->{_armies}{ $task->{task_data} }{y}
                ) {
                    $self->add_player_log(
                        $army->{user_id},
                        LOG_TYPE_ARMY_INTERCEPT,
                        $army->{id},
                        $task->{task_data},
                    );

                    my $from = $army->{x} . ';' . $army->{y};
                    my $to = $self->{_armies}{ $task->{task_data} }{x} . ';' . $self->{_armies}{ $task->{task_data} }{y};
                    my %path_to_target = $self->find_shortest_path($from, $to);
                    if ($path_to_target{$to} && @{ $path_to_target{$to} }) {
                        my ($new_x, $new_y) = split(';', $path_to_target{$to}->[0], 2);

                        $self->move_army($army, $new_x, $new_y, $task->{task_data});
                    }
                    else {
                        warn sprintf('intercept: no path found from %s to %s', $from, $to);
                    }
                    $delete_task = 0;
                }
            }
        }

        for my $unit(@{ $army->{units} }) {
            $unit->{action_points} = $army->{action_points};
            $unit->{x} = $army->{x};
            $unit->{y} = $army->{y};
        }
    }

    if ($delete_task) {
        $self->_get_dbh()->do("DELETE FROM unit_tasks WHERE id=?", undef, $task->{id});
        shift @{ $army->{tasks} };
    }
}
sub move_army {
    my ($self, $army, $new_x, $new_y, $intercepted_id) = @_;

    # for my $a(@{ $self->{_armies_in_tiles}{$new_x . '_' . $new_y} }) {
    #     next if $a->{id} == $army->{id};
    #     next if $a->{id} == $intercepted_id;
    #     return 0;
    # }

    my $task_cost = $self->calculate_move_cost($army->{x}, $army->{y}, $new_x, $new_y);
    # printf STDERR "move cost is %f/%f\n", $task_cost, $army->{action_points};
    return 0 if ($army->{action_points} < $task_cost);

    push @{ $army->{path} }, [$army->{x}, $army->{y}];
    $army->{x} = $new_x;
    $army->{y} = $new_y;

    $army->{action_points} -= $task_cost;

    return 1;
}
sub calculate_move_cost {
    my ($self, $x1, $y1, $x2, $y2, %opts) = @_;

    my $tile1 = $opts{tile1} // $self->_get_dbh()->selectrow_hashref("SELECT * FROM tiles WHERE x=? AND y=?", undef, $x1, $y1);
    return 13 if TILE_MOVE_HALF_COST->{ $tile1->{tile_type} } > 12; # этот шорткат может стать ошибочным если игровая логика поменяется. на данный момент предполагается что клетки у которых стоимость прохода > 12 непроходимы и на них нельзя строить дороги и т. п.
    my $tile1_buildings = $opts{tile1_buildings} // $tile1->{buildings_count};
    unless (defined $tile1_buildings) {
        ($tile1_buildings) = $self->_get_dbh()->selectrow_array(
            "SELECT COUNT(*) FROM buildings WHERE x=? AND y=?",
            undef,
            $x1, $y1
        );
    }
    my $tile2 = $opts{tile2} // $self->_get_dbh()->selectrow_hashref("SELECT * FROM tiles WHERE x=? AND y=?", undef, $x2, $y2);
    return 13 if TILE_MOVE_HALF_COST->{ $tile2->{tile_type} } > 12; # этот шорткат может стать ошибочным если игровая логика поменяется. на данный момент предполагается что клетки у которых стоимость прохода > 12 непроходимы и на них нельзя строить дороги и т. п.
    my $tile2_buildings = $opts{tile2_buildings} // $tile2->{buildings_count};
    unless (defined $tile2_buildings) {
        ($tile2_buildings) = $self->_get_dbh()->selectrow_array(
            "SELECT COUNT(*) FROM buildings WHERE x=? AND y=?",
            undef,
            $x2, $y2
        );
    }

    # print STDERR Dumper($tile1, $tile2);

    my $tile1_type = $tile1_buildings > 0 ? TILE_TYPE_SETTLEMENT : $tile1->{tile_type};
    my $tile2_type = $tile2_buildings > 0 ? TILE_TYPE_SETTLEMENT : $tile2->{tile_type};

    my $xd = $x2 - $x1;
    my $yd = $y2 - $y1;
    my $direction_forth;
    for my $i( 0 .. $#{ DIRECTIONS->[$y1 % 2] }) {
        if ($xd == DIRECTIONS->[$y1 % 2][$i][0] && $yd == DIRECTIONS->[$y1 % 2][$i][1]) {
            $direction_forth = $i;
            last;
        }
    }
    my $direction_back = OPPOSITE_DIRECTIONS->[$direction_forth];


    $tile1_type = TILE_TYPE_ROAD if ($tile1->{roads} & (1 << $direction_forth));
    $tile2_type = TILE_TYPE_ROAD if ($tile2->{roads} & (1 << $direction_back));
    my $is_road = $tile1_type == TILE_TYPE_ROAD && $tile2_type == TILE_TYPE_ROAD;

    # printf STDERR "directions are %d and %d\n", $direction_forth, $direction_back;
    # printf STDERR "tiles are %s and %s\n", Dumper($tile1), Dumper($tile2);;
    # printf STDERR "tile types are %d and %d\n", $tile1_type, $tile2_type;

    return wantarray
        ? (TILE_MOVE_HALF_COST->{$tile1_type} + TILE_MOVE_HALF_COST->{$tile2_type}, $is_road)
        : TILE_MOVE_HALF_COST->{$tile1_type} + TILE_MOVE_HALF_COST->{$tile2_type}
    ;
}
sub calculate_task_cost {
    my ($self, $building, $task, %opts) = @_;

    if ($task->{task_type} == TASK_TYPE_DIRECT_RESOURCE) {
        my $road_length = $self->calculate_road_length([$building->{x}, $building->{y}], $task->{task_data});
        return ceil($road_length / 12) * 12 if defined $road_length;
    }
    elsif ($task->{task_type} == TASK_TYPE_TRANSPORT_RESOURCE) {
        my $task_data = decode_json($task->{task_data});
        my $road_length = $self->calculate_road_length($task_data->{from}, $task_data->{to});
        return ceil($road_length / 12) * 12 if defined $road_length;
    }

    return -1;
}
sub calculate_road_length {
    my ($self, $from, $to) = @_;

    $from = $from->[0] . ';' . $from->[1] if ref($from) eq 'ARRAY';
    $to   = $to->[0]   . ';' . $to->[1]   if ref($to)   eq 'ARRAY';

    my %path_to_target = $self->find_shortest_path(
        $from, $to, graph => '_roads_graph'
    );
    # $self->_debug_message('calculate_road_length', \%path_to_target);
    if ($path_to_target{$to}) {
        return scalar(@{ $path_to_target{$to} });
    }
    return undef;
}
sub find_shortest_path {
    my ($self, $start, $finishes, %opts) = @_;

    # $self->_debug_message("find_shortest_path");
    my $graph_name = $opts{graph} // '_map_graph';
    $self->{$graph_name} or $self->_load_map_data();

    my @nodes;
    my %distances;
    my %previous;
    my %paths;
    if (ref($finishes) ne 'ARRAY') {
        $finishes = [$finishes];
    }
    $start = $start->[0] . ';' . $start->[1] if ref($start) eq 'ARRAY';
    @$finishes = map {ref($_) eq 'ARRAY' ? $_->[0] . ';' . $_->[1] : $_} @$finishes;
    my %finishes_map = map {$_ => 1} @$finishes;

    # $self->_debug_message( 'find_shortest_path ' . $start . ' => ' . join(',', @$finishes) );

    for my $vertex(keys %{ $self->{$graph_name} }) {
        if ($vertex eq $start) {
            $distances{$vertex} = 0;
            push @nodes, {key => $vertex, priority => 0};
        }
        else {
            $distances{$vertex} = 'inf';
            push @nodes, {key => $vertex, priority => 'inf'};
        }
    }
    @nodes = sort {$a->{priority} <=> $b->{priority}} @nodes;

    # $self->_debug_message('sorted');

    while(@nodes) {
        my $smallest = shift @nodes;
        $smallest = $smallest->{key};

        if ($opts{max} && $distances{$smallest} > $opts{max}) {
            last;
        }

        if( !$smallest || $distances{$smallest} == 'inf' ) {
            next;
        }

        if ($finishes_map{$smallest}) {
            my $step = $smallest;
            $paths{$smallest} = [];

            while($previous{$step}) {
                unshift @{ $paths{$smallest} }, $step;
                $step = $previous{$step};
            }

            delete $finishes_map{$smallest};

            if (scalar(keys %finishes_map) == 0) {
                last;
            }
        }

        while (my ($neighbor, $distance) = each %{ $self->{$graph_name}{$smallest} }) {
            my $alt = $distances{$smallest} + $distance;

            if ($alt < $distances{$neighbor}) {
                $distances{$neighbor} = $alt;
                $previous{$neighbor} = $smallest;

                push @nodes, {key => $neighbor, priority => $alt}
            }
        }
        
        @nodes = sort {$a->{priority} <=> $b->{priority}} @nodes;
    }

    return %paths;
}
sub resolve_battle {
    my ($self, $user_armies) = @_;

    my @participating_users = keys %$user_armies;
    my %army_force;

    my %troops_count;

    # определяем состав сил каждого игрока
    while (my ($user_id, $armies) = each %$user_armies) {
        for my $army(@$armies) {
            next if $army->{defeated};
            for my $unit(@{ $army->{units} }) {
                # добавляем всех живых боевых юнитов из всех армий в общий пул
                next if $unit->{defeated};
                next unless grep { $_ == $unit->{unit_type} } (UNIT_TYPE_INFANTRY, UNIT_TYPE_ARCHER, UNIT_TYPE_CAVALRY);

                push @{ $army_force{$user_id}->{ $unit->{unit_type} } }, {
                    unit_id => $unit->{id},
                    power   => 1,
                };
            }
        }

        # считаем сколько всего юнитов каждого типа имеется у всех игроков чтобы написать об этом в лог
        $troops_count{$user_id} = [map {scalar(@{$army_force{$user_id}->{$_} // []})} (UNIT_TYPE_INFANTRY, UNIT_TYPE_ARCHER, UNIT_TYPE_CAVALRY)];
    }

    for my $user_id(keys %army_force) { # пишем в лог каждому игроку, с кем сражаются его армии
        $self->add_player_log(
            $user_id,
            LOG_TYPE_ARMY_FIGHT,
            [map { $_->{id} } @{ $user_armies->{$user_id} }],
            $user_armies->{$user_id}[0]{x} . ';' . $user_armies->{$user_id}[0]{y},
            [grep {$_ != $user_id} @participating_users],
            $troops_count{$user_id},
            [map {$_ != $user_id ? $troops_count{$_} : ()} @participating_users],
        );
    }

    my @fighting_users = keys %$user_armies;

    while (scalar(keys %army_force) > 1) { # бой продолжается до тех пор, пока в нем осталось хотя бы 2 участника
        $self->_debug_message('battle ', \%army_force);

        my $the_most_dangerous;
        my $second_most_dangerous;
        my %total_power;
        # определяем общую силу участников сражения
        while (my ($user_id, $force) = each %army_force) {
            while (my ($unit_type, $troops) = each %$force) {
                $total_power{$user_id} += $_->{power} for @$troops;
            }
            if (!$the_most_dangerous || $total_power{$user_id} && $total_power{$user_id} > $total_power{$the_most_dangerous}) {
                $second_most_dangerous = $the_most_dangerous;
                $the_most_dangerous = $user_id;
            }
            elsif (!$second_most_dangerous || $total_power{$user_id} > $total_power{$second_most_dangerous}) {
                $second_most_dangerous = $user_id;
            }
        }
        $self->_debug_message('total_power', \%total_power);

        $self->_debug_message(sprintf("the_most_dangerous is %d, second_most_dangerous is %d", $the_most_dangerous, $second_most_dangerous));
        # Считаем урон
        my %army_damage;
        while (my ($user_id, $force) = each %army_force) {
            # я атакую самого сильного. но если я самый сильный, атакую второго по силе.
            my $target = ($the_most_dangerous == $user_id) ? $second_most_dangerous : $the_most_dangerous;
            while (my ($unit_type, $troops) = each %$force) {
                next unless scalar(@$troops);
                my $damage_left;
                $damage_left += $_->{power} for @$troops; # определяем общую силу всех юнитов данного типа данного игрока
                $self->_debug_message( sprintf("damage for %d of %d is %f", $unit_type, $user_id, $damage_left) );
                for my $target_unit(@{ UNIT_ATTACK_ORDER->{$unit_type} }) { # бьем все типы юнитов выбранного врага в предпочитаемом порядке
                    last if $damage_left <= 0; # пока не кончится урон

                    my $target_type = $target_unit->{type};
                    my $damage_multiplier = $target_unit->{damage};

                    next unless $army_force{$target}->{$target_type}; # некого бить

                    # итоговый урон определяется множителем для данного атакующего по данной цели, и ограничивается численностью юнитов цели
                    my $multiplied_damage = $damage_left * $damage_multiplier;
                    my $actual_damage = min $multiplied_damage, scalar(@{ $army_force{$target}->{$target_type} });

                    # наносим урон цели и вычитаем из своего урона
                    $army_damage{$target}->{$target_type} += $actual_damage;
                    $damage_left -= $actual_damage / $damage_multiplier;

                    $self->_debug_message(sprintf("with multiplyer %f cause %f damage (truncated to %f) to %d, %f left",
                        $damage_multiplier, $multiplied_damage, $actual_damage, $target_type, $damage_left));
                }
            }
        }

        $self->_debug_message('damage ', \%army_damage);

        undef $the_most_dangerous;
        undef $second_most_dangerous;

        my $damaged;
        # Применяем урон
        while (my ($user_id, $force) = each %army_force) {
            for my $unit_type(keys %$force) {
                while ($army_damage{$user_id}->{$unit_type} >= 1) {
                    # убираем по одному юниту данного типа, всего в количестве нанесенного урона
                    shift @{ $force->{$unit_type} };
                    $army_damage{$user_id}->{$unit_type} -= 1;
                    $damaged = 1;
                }
            }
        }

        # если ни одному типу юнитов не было нанесено урона (итоговый урон для всех был меньше 1),
        # выберем случайную жертву.
        unless ($damaged) {
            my $random_victim = $fighting_users[int( rand( scalar(@fighting_users) ) )];
            my ($first_unit_type) = keys %{ $army_force{$random_victim} };

            $self->_debug_message('nobody was damaged so randomly choose victim', $random_victim, $first_unit_type);

            shift @{ $army_force{$random_victim}->{$first_unit_type} };
        }

        # убираем из боя игроков, у которых не осталось ни одного юнита
        my @defeated;
        while (my ($user_id, $force) = each %army_force) {
            my $defeated = 1;
            for my $unit_type(keys %$force) {
                $defeated = 0 if scalar(@{ $force->{$unit_type} });
            }
            push @defeated, $user_id if $defeated;
        }

        delete $army_force{$_} for @defeated;
    }

    while (my ($user_id, $armies) = each %$user_armies) { # пройдемся по армиям, участвовавшим в бою, и посмотрим что от них осталось
        if ($army_force{$user_id}) { # это победивший игрок
            for my $army(@$armies) {
                # посмотрим, остались ли живые юниты в этой армии
                my $army_undefeated = 0;
                for my $unit(@{ $army->{units} }) {
                    unless ( grep { $_ == $unit->{unit_type} } (UNIT_TYPE_INFANTRY, UNIT_TYPE_ARCHER, UNIT_TYPE_CAVALRY) ) {
                        # небоевые юниты победившего игрока остаются нетронутыми
                        $army_undefeated = 1;
                    }
                    elsif (grep { $_->{unit_id} == $unit->{id} } @{ $army_force{$user_id}->{ $unit->{unit_type} } }) {
                        # этот юнит не был удален, значит он цел, как и его армия
                        $army_undefeated = 1
                    }
                    else { # этот юнит был удален в процессе применения урона
                        $unit->{defeated} = 1;
                    }
                    $unit->{action_points} = 0; # воевавший юнит больше не будет ничего делать
                }
                $army->{defeated} = 1 unless $army_undefeated; # в этой армии не нашлось ни одного целого юнита
                $army->{action_points} = 0; # воевавшая армия больше не будет ничего делать
            }
        }
        else {
            # этого игрока убрали из боя, так как у него не осталось активных юнитов (либо их и не было)
            # значит все его армии уничтожены
            for my $army(@$armies) {
                $army->{defeated} = 1;
            }
        }
    }
}
sub _update_armies_in_tiles {
    my ($self) = @_;

    $self->{_armies_in_tiles} = {};
    while (my ($army_id, $army) = each %{ $self->{_armies} }) {
        next if $army->{defeated};
        push @{ $self->{_armies_in_tiles}{ $army->{x} . '_' . $army->{y} }{ $army->{user_id} } }, $army;
    }
}

sub initialize_task {
    my ($self, $building, $task) = @_;

    if (
        $task->{task_type} == TASK_TYPE_TRANSPORT_RESOURCE
    ) {
        my $task_data = decode_json($task->{task_data});
        if (
               $self->{_map}{$task_data->{from}[0]}{$task_data->{from}[1]}{stored_wood} >= $task_data->{wood}
            && $self->{_map}{$task_data->{from}[0]}{$task_data->{from}[1]}{stored_gold} >= $task_data->{gold}
            && $self->{_map}{$task_data->{from}[0]}{$task_data->{from}[1]}{stored_food} >= $task_data->{food}
        ) {
            $self->{_map}{$task_data->{from}[0]}{$task_data->{from}[1]}{stored_wood} -= $task_data->{wood};
            $self->{_map}{$task_data->{from}[0]}{$task_data->{from}[1]}{stored_gold} -= $task_data->{gold};
            $self->{_map}{$task_data->{from}[0]}{$task_data->{from}[1]}{stored_food} -= $task_data->{food};

            return 1;
        }
        return 0;
    }

    return 1;
}
sub perform_task {
    my ($self, $performer, $task, %opts) = @_;

    my $performer_group = $opts{performer_group} // ($performer->{building_type} ? 'building' : 'unit');
    my $performer_type  = $performer->{$performer_group . '_type'};

    if (my $cost = TASK_RESOURCE_COST->{ $task->{task_type} }) {
        my %resources_available = (wood => 0, gold => 0, food => 0);
        $resources_available{$_} += $self->{_map}{ $performer->{x} }{ $performer->{y} }{'stored_' . $_} for qw(wood gold food);

        my $have_enough = (grep {!$cost->{$_} || $resources_available{$_} >= $cost->{$_}} qw(wood gold food)) == 3;
        unless ($have_enough) {
            $self->add_player_log(
                $performer->{user_id},
                LOG_TYPE_FAIL,
                $task->{task_type},
                'no_resource',
                $performer_group,
                $performer->{id},
                '',
                $task->{task_data},
            ) unless $opts{dont_log_fail};
            return;
        }

        for my $resource(keys %$cost) {
            $self->{_map}{ $performer->{x} }{ $performer->{y} }{"stored_$resource"} -= $cost->{$resource};
        }
    }

    if ($task->{task_type} == TASK_TYPE_HARVEST_WOOD) {
        return $self->harvest_resource(
            $performer, 'wood',
            performer_group => $performer_group,
            dont_log_fail =>  $opts{dont_log_fail}
        );
    }
    elsif ($task->{task_type} == TASK_TYPE_HARVEST_GOLD) {
        return $self->harvest_resource(
            $performer, 'gold',
            performer_group => $performer_group,
            dont_log_fail =>  $opts{dont_log_fail}
        );
    }
    elsif ($task->{task_type} == TASK_TYPE_HARVEST_FOOD) {
        return $self->harvest_resource(
            $performer, 'food',
            performer_group => $performer_group,
            dont_log_fail =>  $opts{dont_log_fail}
        );
    }
    elsif ($task->{task_type} == TASK_TYPE_HIRE_INFANTRY) {
        return $self->hire_unit($performer, UNIT_TYPE_INFANTRY, $task->{task_data});
    }
    elsif ($task->{task_type} == TASK_TYPE_HIRE_ARCHER) {
        return $self->hire_unit($performer, UNIT_TYPE_ARCHER, $task->{task_data});
    }
    elsif ($task->{task_type} == TASK_TYPE_HIRE_CAVALRY) {
        return $self->hire_unit($performer, UNIT_TYPE_CAVALRY, $task->{task_data});
    }
    elsif ($task->{task_type} == TASK_TYPE_DIRECT_RESOURCE) {
        $performer->{building_data} = $task->{task_data};
        return 1;
    }
    elsif ($task->{task_type} == TASK_TYPE_JOIN_ARMY) {
        if ($performer->{army_id} != $task->{task_data} && $self->{_armies}{ $task->{task_data} }) {
            @{ $self->{_armies}{ $performer->{army_id} }{units} } = grep { $_->{id} != $performer->{id} } @{ $self->{_armies}{ $performer->{army_id} }{units} };
            $performer->{army_id} = $task->{task_data};
            push @{ $self->{_armies}{ $performer->{army_id} }{units} }, $performer;
            return 1;
        }
        return 0;
    }

    return 0;
}
sub perform_global_task {
    my ($self, $player, $task, %opts) = @_;

    if (
           $task->{task_type} == TASK_TYPE_BUILD_HOUSE
        || $task->{task_type} == TASK_TYPE_BUILD_SAWMILL
        || $task->{task_type} == TASK_TYPE_BUILD_MINE
        || $task->{task_type} == TASK_TYPE_BUILD_FARM
        || $task->{task_type} == TASK_TYPE_BUILD_BARRACKS
        || $task->{task_type} == TASK_TYPE_BUILD_SHOOTING_RANGE
        || $task->{task_type} == TASK_TYPE_BUILD_STADIUM
    ) {
        $self->perform_global_building($player, $task, %opts);
    }
    elsif (
        $task->{task_type} == TASK_TYPE_TRANSPORT_RESOURCE
    ) {
        my $task_data = decode_json($task->{task_data});

        $self->{_map}{$task_data->{to}[0]}{$task_data->{to}[1]}{delivered_wood} += $task_data->{wood};
        $self->{_map}{$task_data->{to}[0]}{$task_data->{to}[1]}{delivered_gold} += $task_data->{gold};
        $self->{_map}{$task_data->{to}[0]}{$task_data->{to}[1]}{delivered_food} += $task_data->{food};
    }
}
sub perform_global_building {
    my ($self, $player, $task, %opts) = @_;

    my $task_data = decode_json($task->{task_data});
    my ($x, $y) = @{ $task_data->{position} };

    $self->_debug_message("task_data", $task_data);

    my @slots_taken = (0, 0, 0, 0);
    my $tile_buildings_sth = $self->_get_dbh()->prepare("SELECT * FROM buildings WHERE x=? AND y=?");
    $tile_buildings_sth->execute($x, $y);
    while ( my $building = $tile_buildings_sth->fetchrow_hashref() ) {
        $slots_taken[$building->{slot}] = 1;
        my ($hsize, $vsize) = split('x', BUILDING_SPACES->{ $building->{building_type} });
        $slots_taken[$building->{slot} + 1] = 1 if $hsize > 1;
        $slots_taken[$building->{slot} + 2] = 1 if $vsize > 1;
    }

    my $want_slot = int($task_data->{position}[2]);
    $want_slot = undef unless grep {$want_slot == $_} (0, 1, 2, 3);
    $want_slot = undef if $slots_taken[$want_slot];
    if (BUILDING_SPACES->{ BUILDING_TASK_RESULTS->{ $task->{task_type} } } eq '1x1') {
    }
    elsif (BUILDING_SPACES->{ BUILDING_TASK_RESULTS->{ $task->{task_type} } } eq '2x1') {
        $want_slot = undef unless $want_slot == 0 || $want_slot == 2;
        $want_slot = undef if $slots_taken[$want_slot + 1];
    }
    elsif (BUILDING_SPACES->{ BUILDING_TASK_RESULTS->{ $task->{task_type} } } eq '1x2') {
        $want_slot = undef unless $want_slot == 0 || $want_slot == 1;
        $want_slot = undef if $slots_taken[$want_slot + 2];
    }

    $self->_debug_message('want_slot ', $want_slot);

    unless (defined $want_slot) {
        # $self->_debug_message("no space");
        $self->add_player_log(
            $player->{user_id},
            LOG_TYPE_FAIL,
            $task->{task_type},
            'no_room_in_tile',
            'global',
            0,
            $task->{task_type},
        );

        return;
    }

    my %cost = %{ TASK_RESOURCE_COST->{ $task->{task_type} } };
    my %resources_available = (wood => 0, gold => 0, food => 0);

    # my %connected_settlements = $self->find_shortest_path([$x, $y], [keys %{ $task_data->{storages} }], max => 12);
    # $connected_settlements{$pos->[0] . ';' . $pos->[1]} = [];
    # $self->_debug_message('connected_settlements ', \%connected_settlements);
    # return unless %connected_settlements;

    my @build_road_tasks;
    my %planned_roads;

    road: for my $road (@{ $task_data->{roads} }) {
        my ($c_x, $c_y) = ($x, $y);

        step: for my $step(@$road) {
            my ($next_x, $next_y) = split(';', $step);
            my $xd = $next_x - $c_x;
            my $yd = $next_y - $c_y;

            my $direction_forth;
            for my $i( 0 .. $#{ DIRECTIONS->[$c_y % 2] }) {
                if ($xd == DIRECTIONS->[$c_y % 2][$i][0] && $yd == DIRECTIONS->[$c_y % 2][$i][1]) {
                    $direction_forth = $i;
                    last;
                }
            }
            $self->_debug_message('step', $step, $direction_forth);
            next road unless defined $direction_forth;
            my $direction_back = OPPOSITE_DIRECTIONS->[$direction_forth];

            unless ($self->{_map}{$c_x}{$c_y}{roads} & (1 << $direction_forth) || $planned_roads{$c_x . ';' . $c_y . ' ' . $direction_forth}) {
                push @build_road_tasks, {x => $c_x, y => $c_y, direction => $direction_forth};
                $planned_roads{$c_x . ';' . $c_y . ' ' . $direction_forth} = 1;
            }
            unless ($self->{_map}{$next_x}{$next_y}{roads} & (1 << $direction_back) || $planned_roads{$next_x . ';' . $next_y . ' ' . $direction_back}) {
                push @build_road_tasks, {x => $next_x, y => $next_y, direction => $direction_back};
                $planned_roads{$next_x . ';' . $next_y . ' ' . $direction_back} = 1;
            }

            if ($task_data->{storages}{$next_x . ';' . $next_y}) {
                $task_data->{storages}{$next_x . ';' . $next_y}{reached} = 1;
            }

            $c_x = $next_x;
            $c_y = $next_y;
        }
    }
    $task_data->{storages}{$task_data->{position}[0] . ';' . $task_data->{position}[1]}{reached} = 1;
    $self->_debug_message('storages reached ', $task_data->{storages});

    $cost{gold} += scalar(@build_road_tasks) * TASK_RESOURCE_COST->{TASK_TYPE_BUILD_ROAD()}{gold};

    for my $settlement(keys %{ $task_data->{storages} }) {
        next unless $task_data->{storages}{$settlement}{reached};
        my ($s_x, $s_y) = split(';', $settlement);
        $resources_available{$_} += min($self->{_map}{$s_x}{$s_y}{'stored_' . $_}, $task_data->{storages}{$settlement}{$_} // 0) for qw(wood gold food);
    }

    $self->_debug_message("cost", \%cost, \%resources_available);

    my $have_enough = (grep {!$cost{$_} || $resources_available{$_} >= $cost{$_}} qw(wood gold food)) == 3;
    unless ($have_enough) {
        $self->add_player_log(
            $player->{user_id},
            LOG_TYPE_FAIL,
            $task->{task_type},
            'no_resource',
            'global',
            join('/', @cost{qw(wood gold food)}) . ' > ' . join('/', @resources_available{qw(wood gold food)}),
        ) unless $opts{dont_log_fail};
        return;
    }

    for my $resource(keys %cost) {
        for my $settlement(keys %{ $task_data->{storages} }) {
            my ($s_x, $s_y) = split(';', $settlement);
            my $spend = min($self->{_map}{$s_x}{$s_y}{'stored_' . $resource}, $task_data->{storages}{$settlement}{$resource} // 0);
            $cost{$resource}                                 -= $spend;
            $self->{_map}{$s_x}{$s_y}{'stored_' . $resource} -= $spend;
            last if $cost{$resource} <= 0;
        }
    }

    for my $task(@build_road_tasks) {
        $self->{_map}{ $task->{x} }{ $task->{y} }{roads} |= (1 << $task->{direction});
    }

    $self->_get_dbh()->do(
        "INSERT INTO buildings(x, y, user_id, building_type, slot) VALUES(?, ?, ?, ?, ?)",
        undef,
        $x, $y, $player->{user_id}, BUILDING_TASK_RESULTS->{ $task->{task_type} }, $want_slot
    );
    my $new_id = $self->_get_dbh()->sqlite_last_insert_rowid();
    $self->{_buildings}{$new_id} = {
        id            => $new_id,
        x             => $x,
        y             => $y,
        user_id       => $player->{user_id},
        building_type => BUILDING_TASK_RESULTS->{ $task->{task_type} },
        action_points => 0,
    };

    $self->add_player_log(
        $player->{user_id},
        LOG_TYPE_BUILD_BUILDING,
        $x . ';' . $y,
        BUILDING_TASK_RESULTS->{ $task->{task_type} },
        $new_id,
    );
}
my %target_groups = (
    b => 'building',
    u => 'unit',
);
sub harvest_resource {
    my ($self, $building, $resource, %opts) = @_;

    my $target = [$building->{x}, $building->{y}];
    if ($building->{building_data}) {
        $target = [split(';', $building->{building_data}, 2)]
    }

    if (
        $self->{_map}{ $building->{x} }{ $building->{y} }{'raw_'    . $resource} > 0
    ) {
        # TODO: константы
        my $harversted_amount = min($self->{_map}{ $building->{x} }{ $building->{y} }{'raw_'    . $resource}, 15);
        $self->{_map}{ $building->{x} }{ $building->{y} }{'raw_'    . $resource} -= $harversted_amount;
        $self->{_map}{ $target->[0] }  { $target->[1] }  {'stored_' . $resource} += $harversted_amount;

        return 1;
        # $self->add_player_log(
        #     $building->{user_id},
        #     LOG_TYPE_HARVEST_RESOURCE,
        #     $resource,
        #     'building',
        #     $building->{id},
        #     $self->{_map}{ $target->[0] }  { $target->[1] }  {'stored_' . $resource},
        #     $self->{_map}{ $building->{x} }{ $building->{y} }{'raw_'    . $resource},
        # );
    }
    else {
        my $task_type = 'TASK_TYPE_HARVEST_' . uc($resource);
        $self->add_player_log(
            $building->{user_id},
            LOG_TYPE_FAIL,
            HexStrat->$task_type(),
            'no_resource',
            'building',
            $building->{id},
            $resource,
        ) unless $opts{dont_log_fail};

        return 0;
    }
}

sub hire_unit {
    my ($self, $building, $unit_type, $army_id) = @_;
    my ($army, $created);
    if ($army_id && $self->{_armies}{$army_id}{x} == $building->{x} && $self->{_armies}{$army_id}{y} == $building->{y}) {
        $army = $self->{_armies}{$army_id};
    }
    if (!$army) {
        $self->{_new_armies}{ $building->{user_id} }{ $building->{x} . '_' . $building->{y} } ||= $self->create_army($building->{x}, $building->{y}, $building->{user_id});
        $army = $self->{_new_armies}{ $building->{user_id} }{ $building->{x} . '_' . $building->{y} };
        $created = 1;
    }
    return unless $army;

    $self->_get_dbh()->do(
        "INSERT INTO units(user_id, army_id, unit_type) VALUES(?, ?, ?)",
        undef,
        $building->{user_id}, $army->{id}, $unit_type
    );
    my $new_id = $self->_get_dbh()->sqlite_last_insert_rowid();
    my $new_unit = {
        id              => $new_id,
        user_id         => $building->{user_id},
        army_id         => $army->{id},
        unit_type       => $unit_type,
        action_points   => 0,
    };
    $self->{_units}{$new_id} = $new_unit;
    push @{ $self->{_armies}{ $army->{id} }{units} }, $new_unit;

    $self->add_player_log(
        $building->{user_id},
        LOG_TYPE_HIRE_UNIT,
        $building->{id},
        $unit_type,
        $new_id,
        $created,
        $army->{id}
    );

    return $new_unit;
}
sub create_army {
    my ($self, $x, $y, $user_id, %opts) = @_;

    $self->_get_dbh()->do(
        "INSERT INTO armies(x, y, user_id) VALUES(?, ?, ?)",
        undef,
        $x, $y, $user_id
    );
    my $new_id = $self->_get_dbh()->sqlite_last_insert_rowid();
    $self->{_armies}{$new_id} = {
        id              => $new_id,
        x               => $x,
        y               => $y,
        user_id         => $user_id,
        action_points   => 0,
    };
    return $self->{_armies}{$new_id} = {
        id      => $new_id,
        x       => $x,
        y       => $y,
        user_id => $user_id,
    };
}
sub destroy_building {
    my ($self, $performer, $target_string, %opts) = @_;

    my $target_g = substr($target_string, 0, 1);
    my $target_group = $target_groups{$target_g};
    my $target_id = substr($target_string, 2);
    my $target = $self->{'_' . $target_group . 's'}{$target_id};
    my $target_type = $target->{$target_group . '_type'};

    if (
        $target_group eq 'building' && $target
        && $target->{x} == $performer->{x} && $target->{y} == $target->{y}
    ) {
        $target->{destroyed} = 1;
    }
}


sub validate_turn_cache {
    my ($self) = @_;

    my ($turn) = $self->_get_dbh()->selectrow_array("SELECT value FROM game_info WHERE key='turn'");

    if (!defined $self->{_cached_turn} || $self->{_cached_turn} != $turn) {
        undef $self->{_map_graph};
        undef $self->{_roads_graph};
    }

    $self->{_cached_turn} = $turn;
}
sub _load_actors {
    my ($self) = @_;

    my $sth_b = $self->_get_dbh()->prepare("SELECT * FROM buildings");
    $sth_b->execute();
    while ( my $building = $sth_b->fetchrow_hashref() ) {
        $building->{action_points} = 12;
        $self->{_buildings}{ $building->{id} } = $building;
    }

    my $sth_a = $self->_get_dbh()->prepare("SELECT * FROM armies");
    $sth_a->execute();
    while ( my $army = $sth_a->fetchrow_hashref() ) {
        $army->{action_points} = 12;
        $self->{_armies}{ $army->{id} } = $army;
    }

    my $sth_u = $self->_get_dbh()->prepare("SELECT * FROM units");
    $sth_u->execute();
    while ( my $unit = $sth_u->fetchrow_hashref() ) {
        $unit->{action_points} = 12;
        $self->{_units}{ $unit->{id} } = $unit;

        next unless $self->{_armies}{ $unit->{army_id} };

        $unit->{x} = $self->{_armies}{ $unit->{army_id} }{x};
        $unit->{y} = $self->{_armies}{ $unit->{army_id} }{y};
        push @{ $self->{_armies}{ $unit->{army_id} }{units} }, $unit;
    }

    my $sth_ut = $self->_get_dbh()->prepare("SELECT * FROM unit_tasks ORDER BY id");
    $sth_ut->execute();
    while ( my $task = $sth_ut->fetchrow_hashref() ) {
        next unless $self->{_armies}{ $task->{army_id} };
        push @{ $self->{_armies}{ $task->{army_id} }{tasks} }, $task;
    }

    $self->{_new_armies} = {};

    # $self->_debug_message('actors', $self->{_buildings}, $self->{_armies});
}
sub _unload_actors {
    my ($self) = @_;
    undef $self->{_buildings};
    undef $self->{_units};
    undef $self->{_armies};
    undef $self->{_new_armies};
}
sub _save_actors {
    my ($self) = @_;

    while (my ($building_id, $building) = each %{ $self->{_buildings} }) {
        if ($building->{destroyed}) {
            $self->_get_dbh()->do(
                "DELETE FROM buildings WHERE id=?",
                undef,
                $building->{id}
            );
        }
        else {
            $self->_get_dbh()->do(
                "
                    UPDATE buildings
                    SET user_id=?, building_data=?
                    WHERE id=?
                ",
                undef,
                $building->{user_id}, $building->{building_data},
                $building->{id}
            );
        }
    }

    while (my ($unit_id, $unit) = each %{ $self->{_units} }) {
        if ($unit->{defeated}) {
            $self->_get_dbh()->do(
                "DELETE FROM units WHERE id=?",
                undef,
                $unit->{id}
            );
            $self->_get_dbh()->do(
                "DELETE FROM unit_tasks WHERE unit_id=?",
                undef,
                $unit->{id}
            );
        }
        else {
            $self->_get_dbh()->do(
                "UPDATE units SET army_id=? WHERE id=?",
                undef,
                $unit->{army_id}, $unit->{id}
            );
        }
    }

    while (my ($army_id, $army) = each %{ $self->{_armies} }) {
        if ($army->{defeated} || scalar(@{ $army->{units} }) == 0) {
            $self->_get_dbh()->do(
                "DELETE FROM armies WHERE id=?",
                undef,
                $army->{id}
            );
            $self->_get_dbh()->do(
                "DELETE FROM units WHERE army_id=?",
                undef,
                $army->{id}
            );
            $self->_get_dbh()->do(
                "DELETE FROM unit_tasks WHERE army_id=?",
                undef,
                $army->{id}
            );
            $self->_get_dbh()->do(
                "DELETE FROM unit_tasks WHERE task_type=? AND task_data=?",
                undef,
                ARMY_TASK_TYPE_INTERCEPT, $army->{id}
            );
        }
        else {
            $self->_get_dbh()->do(
                "UPDATE armies SET x=?, y=? WHERE id=?",
                undef,
                $army->{x}, $army->{y}, $army->{id}
            );
        }
    }
}
sub _load_map_data {
    my ($self) = @_;

    # $self->_debug_message('prepare_map_graph');

    my $users = $self->get_users();
    my @user_fow_columns = map {'user_' . $_->{id} . '_fow'} @$users;

    $self->{_map} = {};
    $self->{_map_graph} = {};
    $self->{_roads_graph} = {};

    my ($max_x, $max_y) = (0, 0);
    my $sth = $self->_get_dbh()->prepare('
        SELECT tiles.x, tiles.y, tiles.tile_type, tiles.roads, COUNT(buildings.id) AS buildings_count,
            stored_wood, stored_gold, stored_food, raw_wood, raw_gold, raw_food
        FROM tiles
        LEFT JOIN buildings ON buildings.x = tiles.x AND buildings.y = tiles.y
        GROUP BY tiles.x, tiles.y;
    ');
    $sth->execute();

    while ( my $row = $sth->fetchrow_hashref() ) {
        @{$row}{@user_fow_columns} = map {1} @user_fow_columns;
        # $self->_debug_message($row);
        $self->{_map}{ $row->{x} }{ $row->{y} } = $row;
        $max_x = $row->{x} if $row->{x} > $max_x;
        $max_y = $row->{y} if $row->{y} > $max_y;
    }

    # $self->_debug_message('filled map');

    for my $y(0 .. $max_y) {
        for my $x(0 .. $max_x) {
            for my $direction(@{ DIRECTIONS->[$y % 2] }) {
                next if $direction->[1] == 1 || ($direction->[1] == 0 && $direction->[0] == 1);

                my $adjacent_x = $x + $direction->[0];
                my $adjacent_y = $y + $direction->[1];

                next if $adjacent_x < 0 || $adjacent_y < 0 || $adjacent_x > $max_x || $adjacent_y > $max_y;
                
                my $tile1 = $self->{_map}{$x}{$y};
                my $tile2 = $self->{_map}{$adjacent_x}{$adjacent_y};
                my ($cost, $is_road) = $self->calculate_move_cost(
                    $x, $y, $adjacent_x, $adjacent_y, tile1 => $tile1, tile2 => $tile2
                );
                next if $cost > 12;

                my $current_key = $x . ';' . $y;
                my $adjacent_key = $adjacent_x . ';' . $adjacent_y;

                $self->{_map_graph}{$current_key}{$adjacent_key} = $cost;
                $self->{_map_graph}{$adjacent_key}{$current_key} = $cost;
                if ($is_road) {
                    $self->{_roads_graph}{$current_key}{$adjacent_key} = 1;
                    $self->{_roads_graph}{$adjacent_key}{$current_key} = 1;
                }
            }
        }
    }

    # $self->_debug_message('prepare_map_graph done');
}
sub _save_map_data {
    my ($self) = @_;

    my $users = $self->get_users();
    my @updated_columns = (qw(stored_wood stored_gold stored_food raw_wood raw_gold raw_food roads), map {'user_' . $_->{id} . '_fow'} @$users);
    my $set_sql = join(', ', map {"$_ = ?"} @updated_columns);

    # $self->_debug_message('_save_map_data', $self->{_campaign}, $users, [@updated_columns]);

    for my $x(keys %{ $self->{_map} }) {
        for my $y(keys %{ $self->{_map}{$x} }) {
            my $tile = $self->{_map}{$x}{$y};
            $tile->{"stored_$_"} += ($tile->{"delivered_$_"} // 0) for qw(wood gold food);

            $self->_get_dbh()->do(
                "UPDATE tiles SET $set_sql WHERE x=? AND y=?",
                undef,
                @{$tile}{@updated_columns},
                $tile->{x}, $tile->{y},
            );
        }
    }
}


1;
