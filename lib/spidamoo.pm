package spidamoo;
use Dancer2;
#use Template;
use utf8;
use Encode;

use Data::Dumper;
use Math::Counting ':student';

our $VERSION = '0.2';

BEGIN { $SIG{'__WARN__'} = sub { warning $_[0] } }

my $lists;
sub LoadLists {
    return $lists if $lists;
    my $path = config->{appdir} . '/data/lists/';
    my $dir;
    opendir $dir, $path;
    while (my $v = readdir($dir)) {
        next if ($v eq '.' || $v eq '..');
        my $list = substr($v, 0, -4);
        my $fp = $path . '/' . $v;
        next if !-f $fp;
        open my ($file), $fp;
        while (my $item = <$file>) {
            chop($item);
            push @{$lists->{$list}}, decode("utf8", $item);
        }
    }
    return $lists;
}

my $books_info;
sub LoadBooks {
    return $books_info if $books_info;
    my $path = config->{appdir} . '/data/books/';
    my $indexPath = $path . 'index.txt';
    my $index;
    open $index, $indexPath;
    while (my $line = <$index>) {
        chop($line);
        my ($file, $format, $name, $description) = split ('\|', decode("utf8", $line));
        push @{$books_info}, {
            file        => $file,
            format      => $format,
            name        => $name,
            description => $description
        };
    }
    return $books_info;
}

my $books = {};
sub LoadBook {
    my $name = shift @_;
    return $books->{$name} if $books->{$name};
    info "loading book $name";
    my $path = config->{appdir} . '/data/books/';

    my $books_info = LoadBooks();
    for my $book(@{$books_info}) {
        if ($book->{name} eq $name) {
            my $fp = $path . $book->{file};
            if (-f $fp) {
                my $fh;
                open $fh, $fp;
                binmode $fh;
                my @contents = <$fh>;
                $book->{contents} = join '', @contents;
                $books->{$name} = $book;
                return $book;
            }
        }
    }
    return;
}

my $posts_info;
sub LoadPosts {
    return $posts_info if $posts_info;
    info 'loading posts';
    my $path = config->{appdir} . '/data/blog/';
    my $indexPath = $path . 'index.txt';
    my $index;
    open $index, $indexPath;
    while (my $line = <$index>) {
        chop($line);
        my ($name, $title, $preview, $time, $read_more) = split ('\|', decode("utf8", $line));
        $preview =~ s/__read_more__/href="\/blog\/$name"/;
        push @{$posts_info}, {name => $name, title => $title, preview => $preview, time => $time, read_more => $read_more};
    }
    return $posts_info;
}

my $posts = {};
sub LoadPost {
    my $name = shift @_;
    return $posts->{name} if $posts->{name};
    info "loading post $name";
    my $path = config->{appdir} . '/data/blog/';

    my $posts_info = LoadPosts();
    for my $post(@{$posts_info}) {
        if ($post->{name} eq $name) {
            my $fp = $path . $post->{name} . '.txt';
            if (-f $fp) {
                my $fh;
                open $fh, $fp;
                my @contents = <$fh>;
                $post->{contents} = decode("utf8", join '', @contents);
                $posts->{$name} = $post;
                return $post;
            }
        }
    }
    return;
}

my $portfolio;
sub LoadPortfolio {
    return $portfolio if $portfolio;
    info 'loading portfolio';
    my $fp = config->{appdir} . '/data/portfolio.txt';
    my $file;
    open $file, $fp;
    while (my $line = <$file>) {
        chop($line);
        my ($url, $name, $image) = split ('\|', decode("utf8", $line));
        push @{$portfolio}, {
            url         => $url,
            name        => $name,
            image       => $image,
        };
    }
    return $portfolio;
}

sub get_trgpg_menu {
    return [
        # {title => 'DM Helper', url => '/trpg/tools', name => 'tools'},
        {title => 'Dungeon Wanderer', url => '/trpg/dw', name => 'dw'},
        {title => 'WoD', url => '/trpg/wod', name => 'wod'},
        # {title => 'GURPS', url => '/trpg/books', name => 'books'},
        # {title => 'Статьи', url => '/blog', name => 'blog'},
    ]
}

get '/' => sub {
    #my $lists = LoadLists();
    # template 'index' => {background_color => '#E1DAE3'};
    send_file 'smth_dummy.html';
};

# get '/trpg/tools/?' => sub {
#     my $lists = LoadLists();
#     template 'tools' => {
#         lists => $lists,
#         page  => "tools",
#         title => "DM Helper",
#         menu  => get_trgpg_menu,
#     };
# };

get '/trpg/dw/?' => sub {
    template 'dw' => {
        title => "Dungeon Wanderer",
        menu  => get_trgpg_menu,
        page  => 'dw',
    };
};

# get '/trpg/books/?' => sub {
#     my $books = LoadBooks();
#     template 'books' => {
#         books => $books,
#         page  => "books",
#         title => "Книги",
#         menu  => get_trgpg_menu,
#     };
# };

get '/trpg/books/:book/?:action?' => sub {
    if (my $book = LoadBook(params->{book})) {
        header('Content-Type' => 'application/' . $book->{format});
        if (params->{action} && params->{action} eq "download") {
            header('Content-Disposition' => 'attachment; filename=' . $book->{name} . '.' . $book->{format});
        }
        binmode STDOUT;
        #print $book;
        return $book->{contents};
    } else {
        send_error("no book found", 404);
    }
};

get '/trpg/wod/?' => sub {
    template 'wod' => {
        page  => "wod",
        title => "WoD calculations",
        menu  => get_trgpg_menu,
    };
};

get '/trpg/wod/calculate/?' => sub {
    content_type 'text/plain';
    my $n = param 'n';        # количество бросков
    my $k = param 'k';        # целевое значение успехов
    my $d = param('d') || 8;  # трудность броска
    my $c = param('c') || 10; # критическая угроза
    
    my $p  = (11 - $d) / 10;
    my $cp = (11 - $c) / 10;
    
    my $result = 0;
    # Складываем вероятности успехов от требуемой до максимума
    for my $i($k .. $n) {
        # Формула Байеса http://www.matburo.ru/tv_spr_sub.php?p=1
        my $P = ( factorial($n) / ( factorial($i) * factorial($n - $i) ) ) * $p ** $i * (1 - $p) ** ($n - $i) ;
        $result += $P;
    }
    
    # for my $i( 1 .. int($k / 2) ) {
        # my $ck = $k - $i * 2; # Требуемое количество успехов сейчас
        # Шанс выпадения требуемого для этого случая количества успехов
        # my $P  = ( factorial($n) / ( factorial($ck) * factorial($n - $ck) ) ) * $p ** $ck * (1 - $p) ** ($n - $ck) ;
        # my $cn = $n - $ck; # Количество дайсов без обычных успехов
        # Шанс выпадения требуемого для этого случая количества критов из тех дайсов что не успехи
        # my $Pc = ( factorial($cn) / ( factorial($i) * factorial($cn - $i) ) ) * $cp ** $i * (1 - $cp) ** ($cn - $i);
        # my $crit_success = $Pc * $p; # Шанс дополнительного успеха после крита
        # $result += $crit_success * $P;
    # }
    
    return $result;
};

get '/trpg/wod/calculate2/?' => sub {
    content_type 'application/json';
    my $n = param 'n';        # количество бросков
    my $d = param('d') || 8;  # трудность броска
    #my $c = param('c') || 10; # критическая угроза
    
    my $p  = (11 - $d) / 10;
    # my $cp = (11 - $c) / 10;
    
    my $k1 = $n * $p - (1 - $p);
    my $k2 = $n * $p + $p;
    my $km = ($k1 + $k2) / 2;
    
    return to_json [$k1, $k2, $km] ;
};

1;
